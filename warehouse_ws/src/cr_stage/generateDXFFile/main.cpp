#include <iostream>
#include "dl_dxf.h"
using namespace std;

int main(int argc, char** argv) {
    DL_Dxf dxf;
    DL_WriterA* dw = dxf.out("world.dxf", DL_Codes::AC1015);

    // section header:
    dxf.writeHeader(*dw);
    dw->sectionEnd();

    // section tables:
    dw->sectionTables();

    // VPORT:
    dxf.writeVPort(*dw);

    // LTYPE:
    dw->tableLinetypes(3);
    dxf.writeLinetype(*dw, DL_LinetypeData("CONTINUOUS", "Continuous", 0, 0, 0.0));
    dxf.writeLinetype(*dw, DL_LinetypeData("BYLAYER", "", 0, 0, 0.0));
    dxf.writeLinetype(*dw, DL_LinetypeData("BYBLOCK", "", 0, 0, 0.0));
    dw->tableEnd();

    // LAYER:
    dw->tableLayers(1);
    dxf.writeLayer(
            *dw,
            DL_LayerData("0", 0),
            DL_Attributes("", 2, 0, 100, "CONTINUOUS")
    );
    dw->tableEnd();

    // STYLE:
    dw->tableStyle(1);
    DL_StyleData style("Standard", 0, 0.0, 1.0, 0.0, 0, 2.5, "txt", "");
    style.bold = false;
    style.italic = false;
    dxf.writeStyle(*dw, style);
    dw->tableEnd();

    // VIEW:
    dxf.writeView(*dw);

    // UCS:
    dxf.writeUcs(*dw);

    // APPID:
    dw->tableAppid(1);
    dxf.writeAppid(*dw, "ACAD");
    dw->tableEnd();

    // DIMSTYLE:
    dxf.writeDimStyle(*dw, 2.5, 0.625, 0.625, 0.625, 2.5);

    // BLOCK_RECORD:
    dxf.writeBlockRecord(*dw);
    dw->tableEnd();

    dw->sectionEnd();


    // ENTITIES:
    dw->sectionEntities();


    DL_Attributes attributes("0", 2, 0, -1, "BYLAYER");

    int shelfBlockNumX = 3, shelfBlockNumY = 5;
    double l=0.0, w=0.0;//l~x, w~y
    l+=3;
    for(int i=0; i<shelfBlockNumX; ++i) {
        if(i == 0) {
            l+=8;
        }
        else if(i%2 == 0) {
            l+=3;
            l+=8;
        }
        else {
            l+=2;
            l+=8;
        }
        if(i == 8 || i == 9) {
            l+=2;//8th & 9th shelfs' length is 10m, others 8m.
        }
    }
    l+=3;
    w+=3;
    for(int j=0; j<shelfBlockNumY; ++j) {
        if(j == 0) {
            w+=0.5;
        }
        else if(j == 1) {
            w+=1;
            w+=0.5;
        }
        else {
            w+=1.5;
            w+=0.5;
        }
    }
    w+=3;
    //boundary
    dxf.writePolyline(*dw, DL_PolylineData(4,0,0,1),attributes);
    dxf.writeVertex(*dw, DL_VertexData(0, 0));
    dxf.writeVertex(*dw, DL_VertexData(l*1000, 0));
    dxf.writeVertex(*dw, DL_VertexData(l*1000, w*1000));
    dxf.writeVertex(*dw, DL_VertexData(0, w*1000));

    double length, width, x, y;//x: centerX
    x = 0;
    y = 0;
    for(int i=0; i<shelfBlockNumX; ++i) {
        length = (i == 8 || i == 9) ? 10.0 : 8.0;
        if(i%2 == 0) {
            x += 3;
        }
        else {
            x += 2;
        }
        x += length/2;

        for(int j=0; j<shelfBlockNumY; ++j) {
            width = 0.5;
            if(j == 0) {
                y += 3;
                y += width/2;
            }
            else if(j == 1) {
                y += 1;
                y += width/2;
            }
            else {
                y += 1.5;
                y += width/2;
            }

            //shelfBlocks
            dxf.writePolyline(*dw, DL_PolylineData(4,0,0,1),attributes);
            dxf.writeVertex(*dw, DL_VertexData((x-length/2)*1000, (y-width/2)*1000));
            dxf.writeVertex(*dw, DL_VertexData((x+length/2)*1000, (y-width/2)*1000));
            dxf.writeVertex(*dw, DL_VertexData((x+length/2)*1000, (y+width/2)*1000));
            dxf.writeVertex(*dw, DL_VertexData((x-length/2)*1000, (y+width/2)*1000));

            //RFIDs
            dxf.writeCircle(*dw, DL_CircleData((x-length/2+0.1)*1000, (y-width/2-0.4)*1000, 0, 100), attributes);
            dxf.writeCircle(*dw, DL_CircleData((x+length/2-0.1)*1000, (y-width/2-0.4)*1000, 0, 100), attributes);
            dxf.writeCircle(*dw, DL_CircleData((x+length/2+0.4)*1000, (y-width/2+0.1)*1000, 0, 100), attributes);
            dxf.writeCircle(*dw, DL_CircleData((x+length/2+0.4)*1000, (y+width/2-0.1)*1000, 0, 100), attributes);
            dxf.writeCircle(*dw, DL_CircleData((x+length/2-0.1)*1000, (y+width/2+0.4)*1000, 0, 100), attributes);
            dxf.writeCircle(*dw, DL_CircleData((x-length/2+0.1)*1000, (y+width/2+0.4)*1000, 0, 100), attributes);
            dxf.writeCircle(*dw, DL_CircleData((x-length/2-0.4)*1000, (y+width/2-0.1)*1000, 0, 100), attributes);
            dxf.writeCircle(*dw, DL_CircleData((x-length/2-0.4)*1000, (y-width/2+0.1)*1000, 0, 100), attributes);



            //cout<<"X1 "<<(x-length/2)*1000<<","<<(y-width/2)*1000<<"  X2 "<<(x+length/2)*1000<<","<<(y+width/2)*1000<<endl;
//            cout<<"["<<(x-length/2+0.1)*1000<<","<<(y-width/2-0.4)*1000<<"]  ";
//            cout<<"["<<(x+length/2-0.1)*1000<<","<<(y-width/2-0.4)*1000<<"]  ";
//            cout<<"["<<(x+length/2+0.4)*1000<<","<<(y-width/2+0.1)*1000<<"]  ";
//            cout<<"["<<(x+length/2+0.4)*1000<<","<<(y+width/2-0.1)*1000<<"]  ";
//            cout<<"["<<(x+length/2-0.1)*1000<<","<<(y+width/2+0.4)*1000<<"]  ";
//            cout<<"["<<(x-length/2+0.1)*1000<<","<<(y+width/2+0.4)*1000<<"]  ";
//            cout<<"["<<(x-length/2-0.4)*1000<<","<<(y+width/2-0.1)*1000<<"]  ";
//            cout<<"["<<(x-length/2-0.4)*1000<<","<<(y-width/2+0.1)*1000<<"]  "<<endl;
            y += width/2;
        }
        x += length/2;
        y = 0;
    }

    // end section ENTITIES:
    dw->sectionEnd();
    dxf.writeObjects(*dw, "MY_OBJECTS");
    dxf.writeObjectsEnd(*dw);

    dw->dxfEOF();
    dw->close();
    delete dw;

    return 0;
}