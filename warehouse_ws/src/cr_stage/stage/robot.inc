define hokuyoLaser ranger
(
  #ctrl "lasernoise"
  sensor
  (
    range [0.06 10.0]
    #fov 270.0
    #samples 1081
    fov 180.0
    samples 721
    noise [ 0.001 0.0 0.0 ]
    #color "red"
  )

  model
  (
    # generic model properties
    color "black"
    size [ 0.05 0.05 0.07 ]
    origin [ 0.0 0.0 0.0 0.0 ]
  )
)

define robot position
(
  drive "diff"
  localization "gps"
  
  #localization "odom"
  #localization_origin [1 1 0 1.572963]
  localization_origin [0.0 0.0 0.0 0.0]
  #odom_error [0.03 0.03 0.00 0.05]
  #odom_error [x y z t]
  #odom_error [0.03 0.03 999999 999999 999999 0.02]

  size [ 0.5 0.5 0.4 ]
  #origin [ 0.25 0.25 0.0 0.0 ]
  origin [ 0.0 0.0 0.0 0.0 ]
  gui_nose 1
  gui_move 1
  color "gray"
  
  hokuyoLaser(pose [ 0.21 0.0 -0.2 0.0 ])
  #hokuyoLaser(pose [ 0.0 0.0 -0.2 0.0 ])
)