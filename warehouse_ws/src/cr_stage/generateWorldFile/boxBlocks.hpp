//
// Created by crti on 4/18/17.
//

#ifndef GENERATEWORLDFILE_BOXBLOCKS_HPP
#define GENERATEWORLDFILE_BOXBLOCKS_HPP

#include <iostream>
#include <fstream>
using namespace std;

class boxBlocks
{
    struct BLOCK
    {
        double length;
        double width;
        double x;
        double y;
        double theta;
    };
private:
    fstream incFile;
public:
    boxBlocks();
    ~boxBlocks();
    int calBlocks();
    int generateFile();
};




#endif //GENERATEWORLDFILE_BOXBLOCKS_HPP
