//
// Created by crti on 4/18/17.
//

#ifndef GENERATEWORLDFILE_SHELFBLOCKS_HPP
#define GENERATEWORLDFILE_SHELFBLOCKS_HPP

#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

class shelfBlocks
{
    struct BLOCK
    {
        double length;
        double width;
        double x;
        double y;
        double theta;
    };
private:
    fstream incFile;
    string incFileName;
    vector<BLOCK> shelfBlockVector;
    int blockNumX, blockNumY;
public:
    shelfBlocks();
    ~shelfBlocks();
    int calBlocks();
    int generateFile();
};

shelfBlocks::shelfBlocks()
{
    incFileName = "../../stage/shelfBlocks.inc";
    blockNumX = 16;
    blockNumY = 11;
}

shelfBlocks::~shelfBlocks()
{
    vector<BLOCK>().swap(shelfBlockVector);
}

int shelfBlocks::calBlocks() {
    shelfBlockVector.reserve(blockNumX*blockNumY+1);
    for(int i = 0; i < blockNumX; ++i) {
        for(int j = 0; j < blockNumY; ++j) {
            BLOCK tmpBlock;
            tmpBlock.length = (i == 8 || i == 9) ? 10.0 : 8.0;
            tmpBlock.width = 0.5;

            if(i == 0) {
                tmpBlock.x = 3.0;
            }
            else if(i%2 == 1) {
                BLOCK leftBlock = shelfBlockVector[i*blockNumY+j-blockNumY];
                tmpBlock.x = leftBlock.x + leftBlock.length + 2.0;
            }
            else {
                BLOCK leftBlock = shelfBlockVector[i*blockNumY+j-blockNumY];
                tmpBlock.x = leftBlock.x + leftBlock.length + 3.0;
            }

            if(j == 0) {
                tmpBlock.y = 9.0;
            }
            else if(j == 1) {
                BLOCK bottomBlock = shelfBlockVector[i*blockNumY+j-1];
                tmpBlock.y = bottomBlock.y + bottomBlock.width + 1.0;
            }
            else {
                BLOCK bottomBlock = shelfBlockVector[i*blockNumY+j-1];
                tmpBlock.y = bottomBlock.y + bottomBlock.width + 1.5;
            }

            tmpBlock.theta = 0.0;
            shelfBlockVector.push_back(tmpBlock);
        }
    }

    BLOCK conveyorBeltBlock;
    BLOCK rightTopBlock = shelfBlockVector[blockNumX*blockNumY-1];
    conveyorBeltBlock.length = rightTopBlock.x + rightTopBlock.length + 3.0;
    conveyorBeltBlock.width = 6.0;
    conveyorBeltBlock.x = 0.0;
    conveyorBeltBlock.y = 0.0;
    conveyorBeltBlock.theta = 0.0;
    shelfBlockVector.push_back(conveyorBeltBlock);

    shelfBlockVector.shrink_to_fit();
    return 0;
}

int shelfBlocks::generateFile() {
    incFile.open(incFileName, ios::out);
    incFile << "include \"shelfBlock.inc\"" << endl << endl;
    incFile << "define shelfBlocks model" << endl << "(" << endl;

    int shelfBlockNum = shelfBlockVector.size();
    for(int i = 0; i < shelfBlockNum; ++i) {
        incFile << "  shelfBlock( ";

        incFile << "pose [" << shelfBlockVector[i].x;
        incFile << " " << shelfBlockVector[i].y;
        incFile << " 0.0 " << shelfBlockVector[i].theta << "] ";

        incFile << "size [" << shelfBlockVector[i].length;
        incFile << " " << shelfBlockVector[i].width;
        incFile << " 2.0]";

        incFile << " origin [" << shelfBlockVector[i].length/2;
        incFile << " " << shelfBlockVector[i].width/2;
        incFile << " 0.0 0.0 ]";

        incFile << " )" << endl;
    }

    incFile << ")" << endl;
    return 0;
}


#endif //GENERATEWORLDFILE_SHELFBLOCKS_HPP
