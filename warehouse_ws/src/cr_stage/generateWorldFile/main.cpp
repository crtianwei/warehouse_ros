#include <iostream>
#include <fstream>
#include "shelfBlocks.hpp"
#include "boxBlocks.hpp"
using namespace std;


int main() {
    shelfBlocks SBs;
    SBs.calBlocks();
    SBs.generateFile();
    return 0;
}