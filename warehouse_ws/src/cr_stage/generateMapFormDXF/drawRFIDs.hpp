//
// Created by crti on 5/11/17.
//

#ifndef GENERATEMAPFORMDXF_DRAWRFIDS_HPP
#define GENERATEMAPFORMDXF_DRAWRFIDS_HPP

#include "blockStruct.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <opencv2/opencv.hpp>
using namespace cv;
using namespace std;

int drawRFIDs(Mat& img, const vector<CR_RFID>& RFIDS, double resolution = 0.025) {
    int RFIDSize = RFIDS.size();
    for(int i=0; i<RFIDSize; ++i) {
        Point center;
        center.x = (int)(RFIDS[i].x/resolution);
        center.y = (int)(RFIDS[i].y/resolution);
        int r = (int)(RFIDS[i].r/resolution);

        circle(img, center, r, Scalar(i*100+160), -1);
    }
}

int saveRFIDs(const vector<CR_RFID>& RFIDS) {
    fstream RFIDLogFile;
    string RFIDLogFileName = "RFID.txt";
    RFIDLogFile.open(RFIDLogFileName, ios::out);

    int RFIDSize = RFIDS.size();
    for(int i=0; i<RFIDSize; ++i) {
        //ID,X,Y,RFID[]
        RFIDLogFile << i << "," << RFIDS[i].x << "," << RFIDS[i].y << "," << i+1000 << endl;
    }

    RFIDLogFile.close();
}

int savePathTMP() {
    fstream pathLogFile;
    string pathLogFileName = "PATH.txt";
    pathLogFile.open(pathLogFileName, ios::out);

    //circles around shefls
    for(int i=0; i<15; ++i) {
        pathLogFile << 0 + i*8 << "," << 1 + i*8 << endl;
        pathLogFile << 1 + i*8 << "," << 2 + i*8 << endl;
        pathLogFile << 2 + i*8 << "," << 3 + i*8 << endl;
        pathLogFile << 3 + i*8 << "," << 4 + i*8 << endl;
        pathLogFile << 4 + i*8 << "," << 5 + i*8 << endl;
        pathLogFile << 5 + i*8 << "," << 6 + i*8 << endl;
        pathLogFile << 6 + i*8 << "," << 7 + i*8 << endl;
        pathLogFile << 7 + i*8 << "," << 0 + i*8 << endl;
    }

    //straight of cross
    for(int i=0; i<3; ++i) {
        for(int j=0; j<5-1; ++j) {
            pathLogFile << 15 + i*40 + j*8 << "," <<  6 + i*40 + j*8 << endl;//up
            pathLogFile <<  3 + i*40 + j*8 << "," << 10 + i*40 + j*8 << endl;//down
        }
    }
    for(int i=0; i<3-1; ++i) {
        for(int j=0; j<5; ++j) {
            pathLogFile << 45 + i*40 + j*8 << "," <<  4 + i*40 + j*8 << endl;//left
            pathLogFile <<  1 + i*40 + j*8 << "," << 40 + i*40 + j*8 << endl;//right
        }
    }

    //turn left of cross
    for(int i=0; i<3-1; ++i) {
        for(int j=0; j<5-1; ++j) {
            pathLogFile <<  3 + i*40 + j*8 << "," << 48 + i*40 + j*8 << endl;
            pathLogFile <<  9 + i*40 + j*8 << "," << 46 + i*40 + j*8 << endl;
            pathLogFile << 55 + i*40 + j*8 << "," <<  4 + i*40 + j*8 << endl;
            pathLogFile << 45 + i*40 + j*8 << "," << 10 + i*40 + j*8 << endl;
        }
    }

    pathLogFile.close();
}

#endif //GENERATEMAPFORMDXF_DRAWRFIDS_HPP
