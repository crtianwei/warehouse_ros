//
// Created by crti on 5/9/17.
//

#include "DXLReader.h"

DXLReader::DXLReader() {
    stage = 0;
    zeroCheck = 0.01;
    boundaryBlockIndex = 0;
    boundaryBlockLength = 0;
    boundaryBlockWidth = 0;
}

void DXLReader::updateBlock(bool reset, double vertexX, double vertexY) {
    if(reset) stage = 0;

    if(stage == 0) { //Polyline
        vector<CR_POINT>().swap(blockVertexS);
        blockVertexS.reserve(4);

        addBlock.width = 0;
        addBlock.length = 0;
        addBlock.theta = 0;
        addBlock.x = 0;
        addBlock.y = 0;

        ++stage;
        return;
    }
    else if(stage == 1 || stage == 2 || stage == 3 || stage == 4) { //Vertex
        blockVertexS.push_back(CR_POINT{vertexX,vertexY});
        ++stage;
    }
    else {
        cout << "ERROR DXLReader updateBlock : stage = " << stage << endl;
        stage = 0;
        return;
    }

    if(stage == 5) {
        double localZeroCheck = zeroCheck;
        sort(blockVertexS.begin(), blockVertexS.end(), [localZeroCheck](const CR_POINT& p1, const CR_POINT& p2){
            if(abs(p1.x-p2.x) < localZeroCheck) {
                return p1.y < p2.y;
            }
            else {
                return p1.x < p2.x;
            }
        });

        //check square
        if( abs(blockVertexS[0].x - blockVertexS[1].x) < zeroCheck
            || abs(blockVertexS[2].x - blockVertexS[3].x) < zeroCheck
            || abs(blockVertexS[0].y - blockVertexS[1].y) < zeroCheck
            || abs(blockVertexS[2].y - blockVertexS[3].y) < zeroCheck) {
            //is unrotated square
            addBlock.x = (blockVertexS[0].x + blockVertexS[3].x)*0.5/1000.0;
            addBlock.y = (blockVertexS[0].y + blockVertexS[3].y)*0.5/1000.0;
            addBlock.theta = 0;
            addBlock.length = abs(blockVertexS[0].x - blockVertexS[3].x)/1000.0;
            addBlock.width = abs(blockVertexS[0].y - blockVertexS[3].y)/1000.0;

            if(boundaryBlockLength <= addBlock.length && boundaryBlockWidth <= addBlock.width) {
                boundaryBlockLength = addBlock.length;
                boundaryBlockWidth = addBlock.width;
                boundaryBlockIndex = blockS.size();
            }
            blockS.push_back(addBlock);
        }
        if( abs(blockVertexS[0].x - blockVertexS[1].x - blockVertexS[2].x + blockVertexS[3].x) < zeroCheck
            || abs(blockVertexS[0].y - blockVertexS[1].y - blockVertexS[2].y + blockVertexS[3].y) < zeroCheck) {
            //is rotated square
            //addBlock.x = (blockVertexS[0].x + blockVertexS[2].x)*0.5;
            //addBlock.y = (blockVertexS[0].y + blockVertexS[2].y)*0.5;
        }
        else {
            //not square
        }
    }
}

void DXLReader::updateRFID(double x, double y, double r) {
    addRFID.x = x/1000.0;
    addRFID.y = y/1000.0;
    addRFID.r = r/1000.0;
    RFIDS.push_back(addRFID);
}

void DXLReader::addLayer(const DL_LayerData& data) {
    printf("LAYER: %s flags: %d\n", data.name.c_str(), data.flags);
}

void DXLReader::addPoint(const DL_PointData& data) {
    printf("POINT    (%6.3f, %6.3f, %6.3f)\n",
           data.x, data.y, data.z);
}

void DXLReader::addLine(const DL_LineData& data) {
    printf("LINE     (%6.3f, %6.3f, %6.3f) (%6.3f, %6.3f, %6.3f)\n",
           data.x1, data.y1, data.z1, data.x2, data.y2, data.z2);
}

void DXLReader::addArc(const DL_ArcData& data) {
    printf("ARC      (%6.3f, %6.3f, %6.3f) %6.3f, %6.3f, %6.3f\n",
           data.cx, data.cy, data.cz,
           data.radius, data.angle1, data.angle2);
}

void DXLReader::addCircle(const DL_CircleData& data) {
    printf("CIRCLE   (%6.3f, %6.3f, %6.3f) %6.3f\n",
           data.cx, data.cy, data.cz, data.radius);
    updateRFID(data.cx, data.cy, data.radius);
}

void DXLReader::addPolyline(const DL_PolylineData& data) {
    printf("POLYLINE %d\n", (int)data.flags);
    updateBlock(true);
}

void DXLReader::addVertex(const DL_VertexData& data) {
    printf("VERTEX   (%6.3f, %6.3f, %6.3f) %6.3f\n",
           data.x, data.y, data.z, data.bulge);
    updateBlock(false, data.x, data.y);
}

void DXLReader::add3dFace(const DL_3dFaceData& data) {
    printf("3DFACE\n");
    for (int i=0; i<4; i++) {
        printf("   corner %d: %6.3f %6.3f %6.3f\n",
               i, data.x[i], data.y[i], data.z[i]);
    }
}

