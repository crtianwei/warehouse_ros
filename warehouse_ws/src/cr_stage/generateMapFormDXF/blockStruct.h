//
// Created by crti on 5/9/17.
//

#ifndef GENERATEMAPFORMDXF_BLOCKSTRUCT_H
#define GENERATEMAPFORMDXF_BLOCKSTRUCT_H

struct CR_BLOCK
{
    double length;
    double width;
    double x;
    double y;
    double theta;
};

struct CR_RFID
{
    double x;
    double y;
    double r;
};

#endif //GENERATEMAPFORMDXF_BLOCKSTRUCT_H
