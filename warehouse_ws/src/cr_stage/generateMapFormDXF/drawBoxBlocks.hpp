//
// Created by crti on 17-4-21.
//

#ifndef GENERATEMAPFILE_DRAWBOXBLOCKS_HPP
#define GENERATEMAPFILE_DRAWBOXBLOCKS_HPP

#include "blockStruct.h"
#include "drawBlock.hpp"
#include <iostream>
#include <opencv2/opencv.hpp>
using namespace cv;
using namespace std;

double generateGaussianNoise(double var) {
    var = var/3;
    static bool haveSpare = false;
    static double rand1, rand2;

    if(haveSpare) {
        haveSpare = false;
        return var * sqrt(rand1) * sin(rand2);
    }

    haveSpare = true;

    rand1 = rand() / (double)RAND_MAX;
    if(rand1 < 1e-100) rand1 = 1e-100;
    rand1 = -2 * log(rand1);
    rand2 = (rand() / (double)RAND_MAX) * M_PI * 2;

    return var * sqrt(rand1) * cos(rand2);
}

int drawBoxBlocks(Mat& img, const vector<CR_BLOCK>& shelfBlockS, int boundaryBlockIndex, double resolution = 0.025) {
    int L, W, X, Y, T;
    int shelfBlockSize = shelfBlockS.size();
    for(int i=0; i<shelfBlockSize; ++i) {
        if(i != boundaryBlockIndex) {
            bool lengthLongerThanWidth = (shelfBlockS[i].length > shelfBlockS[i].width);
            int boxNumPerShelf = lengthLongerThanWidth ?
                                 (int)(shelfBlockS[i].length/shelfBlockS[i].width) :
                                 (int)(shelfBlockS[i].width/shelfBlockS[i].length);
            double boxLength = lengthLongerThanWidth ? shelfBlockS[i].width : shelfBlockS[i].length;
            for(int j=0; j<boxNumPerShelf; ++j) {
                L = (int)(boxLength / resolution);
                W = (int)(boxLength / resolution);
                if(lengthLongerThanWidth) {
                    X = (int)((shelfBlockS[i].x - shelfBlockS[i].length/2 + j*boxLength + generateGaussianNoise(0.02)) / resolution);
                    Y = (int)((shelfBlockS[i].y + generateGaussianNoise(0.05)) / resolution);
                }
                else {
                    X = (int)((shelfBlockS[i].x + generateGaussianNoise(0.05)) / resolution);
                    Y = (int)((shelfBlockS[i].y - shelfBlockS[i].width/2 + j*boxLength + generateGaussianNoise(0.02)) / resolution);
                }

                T = (int)(shelfBlockS[i].theta + generateGaussianNoise(10));
                drawBlock(img,Point2f(X,Y),Size2f(L,W),T);
            }
        }
    }
}



#endif //GENERATEMAPFILE_DRAWBOXBLOCKS_HPP
