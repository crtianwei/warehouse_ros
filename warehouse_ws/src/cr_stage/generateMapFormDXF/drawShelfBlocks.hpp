//
// Created by crti on 17-4-21.
//

//resolution: 0.05, 1 pixel ~ 0.05m,  20 pixel ~ 1m
//            0.025, 1 pixel ~ 0.025m,  40 pixel ~ 1m

#ifndef GENERATEMAPFILE_DRAWSHELFBLOCKS_HPP
#define GENERATEMAPFILE_DRAWSHELFBLOCKS_HPP

#include "blockStruct.h"
#include "drawBlock.hpp"
#include <iostream>
#include <opencv2/opencv.hpp>
using namespace cv;
using namespace std;

int drawShelfBlocks(Mat& img, const vector<CR_BLOCK>& shelfBlockS, int boundaryBlockIndex, double resolution = 0.025) {
    int L, W, X, Y, T;
    int shelfBlockSize = shelfBlockS.size();
    for(int i=0; i<shelfBlockSize; ++i) {
        if(i != boundaryBlockIndex) {
            L = (int)(shelfBlockS[i].length/resolution);
            W = (int)(shelfBlockS[i].width/resolution);
            X = (int)(shelfBlockS[i].x/resolution);
            Y = (int)(shelfBlockS[i].y/resolution);
            T = (int)(shelfBlockS[i].theta);
            //cout<<"X1 "<<(x-length/2)*1000<<","<<(y-width/2)*1000<<"  X2 "<<(x+length/2)*1000<<","<<(y+width/2)*1000<<endl;
            drawBlock(img,Point2f(X,Y),Size2f(L,W),T);
        }
    }
}

#endif //GENERATEMAPFILE_DRAWSHELFBLOCKS_HPP
