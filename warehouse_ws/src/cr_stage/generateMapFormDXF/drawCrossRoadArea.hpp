//
// Created by crti on 7/6/17.
//

#ifndef GENERATEMAPFORMDXF_DRAWCROSSROADAREA_HPP
#define GENERATEMAPFORMDXF_DRAWCROSSROADAREA_HPP
#include "blockStruct.h"
#include "drawBlock.hpp"
#include <iostream>
#include <algorithm>
#include <opencv2/opencv.hpp>
using namespace cv;
using namespace std;

int drawCrossRoadArea(Mat& img, const vector<CR_BLOCK>& shelfBlockS, int boundaryBlockIndex, double resolution = 0.025) {
    int L, W, X, Y, T;
    vector<CR_BLOCK> shelfBlockSTMP;
    shelfBlockSTMP.insert(shelfBlockSTMP.end(), shelfBlockS.begin(), shelfBlockS.end());

    int shelfBlockSize = shelfBlockSTMP.size();
    shelfBlockSTMP[boundaryBlockIndex].x = 99999999;
    shelfBlockSTMP[boundaryBlockIndex].y = 99999999;

    sort(shelfBlockSTMP.begin(), shelfBlockSTMP.end(), [](CR_BLOCK B1, CR_BLOCK B2){
        if(abs(B1.x - B2.x)<1e-3){
            return (B1.y < B2.y);
        }
        else{
            return (B1.x < B2.x);
        }
    });

    int row = 0, col = 0;
    for(int i=0; i<shelfBlockSize; ++i){
        if((shelfBlockSTMP[i].x - shelfBlockSTMP[0].x)>1e-3){
            row = i;
            break;
        }
    }
    col = shelfBlockSize/row;

    for(int i=0; i<col-1; ++i) {
        for (int j = 0; j < row - 1; ++j) {
            L = (int) (((shelfBlockSTMP[i * row + j + row].x - 0.5 * shelfBlockSTMP[i * row + j + row].length) -
                        (shelfBlockSTMP[i * row + j].x + 0.5 * shelfBlockSTMP[i * row + j].length)) / resolution);
            W = (int) (((shelfBlockSTMP[i * row + j + 1].y - 0.5 * shelfBlockSTMP[i * row + j + 1].width) -
                        (shelfBlockSTMP[i * row + j].y + 0.5 * shelfBlockSTMP[i * row + j].width)) / resolution);
            X = (int) (((shelfBlockSTMP[i * row + j + row].x - 0.5 * shelfBlockSTMP[i * row + j + row].length) +
                        (shelfBlockSTMP[i * row + j].x + 0.5 * shelfBlockSTMP[i * row + j].length)) * 0.5 / resolution);
            Y = (int) (((shelfBlockSTMP[i * row + j + 1].y - 0.5 * shelfBlockSTMP[i * row + j + 1].width) +
                        (shelfBlockSTMP[i * row + j].y + 0.5 * shelfBlockSTMP[i * row + j].width)) * 0.5 / resolution);
            T = (int) (shelfBlockSTMP[i * row + j].theta);
            drawBlock (img, Point2f (X, Y), Size2f (L, W), T, 100);
        }
    }
}
#endif //GENERATEMAPFORMDXF_DRAWCROSSROADAREA_HPP
