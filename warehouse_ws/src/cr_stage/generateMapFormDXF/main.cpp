#include <iostream>
#include "dl_dxf.h"
#include "DXLReader.h"

#include <opencv2/opencv.hpp>
#include "drawBlock.hpp"
#include "drawShelfBlocks.hpp"
#include "drawBoxBlocks.hpp"
#include "drawRFIDs.hpp"
#include "drawCrossRoadArea.hpp"
using namespace cv;
using namespace std;


int main(int argc, char** argv) {

    const char* file = (argc < 2) ? "world.dxf" : argv[1];
    DXLReader* myDXLReader = new DXLReader();
    DL_Dxf* dxf = new DL_Dxf();
    if (!dxf->in(file, myDXLReader)) { // if file open failed
        std::cerr << file << " could not be opened.\n";
        return -1;
    }
    cout <<"boundary: "<< myDXLReader->boundaryBlockLength <<"*"<< myDXLReader->boundaryBlockWidth
         << " " << myDXLReader->boundaryBlockIndex << "th" <<endl;

    Size imgSize = calMapSize(myDXLReader->boundaryBlockLength, myDXLReader->boundaryBlockWidth);

    Mat shelfImg(imgSize,CV_8UC1,Scalar(255));
    drawBoundary(shelfImg, imgSize);
    drawShelfBlocks(shelfImg,myDXLReader->blockS,myDXLReader->boundaryBlockIndex,0.025);
    drawCrossRoadArea(shelfImg,myDXLReader->blockS,myDXLReader->boundaryBlockIndex,0.025);
    imwrite("shelfMap.png", shelfImg);

    Mat boxImg(imgSize,CV_8UC1,Scalar(255));
    drawBoundary(boxImg, imgSize);
    drawBoxBlocks(boxImg,myDXLReader->blockS,myDXLReader->boundaryBlockIndex,0.025);
    imwrite("boxMap.png", boxImg);

    Mat RFIDImg(imgSize,CV_8UC1,Scalar(255));
    drawRFIDs(RFIDImg,myDXLReader->RFIDS,0.025);
    imwrite("RFIDMap.png", RFIDImg);

    saveRFIDs(myDXLReader->RFIDS);

    savePathTMP();

    return 0;
}