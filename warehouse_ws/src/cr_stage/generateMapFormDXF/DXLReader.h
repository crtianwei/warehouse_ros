//
// Created by crti on 5/9/17.
//

#ifndef GENERATEMAPFORMDXF_DXLREADER_H
#define GENERATEMAPFORMDXF_DXLREADER_H

#include "dl_creationadapter.h"
#include "blockStruct.h"
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class DXLReader : public DL_CreationAdapter {
public:
    DXLReader();
    virtual void addLayer(const DL_LayerData& data);
    virtual void addPoint(const DL_PointData& data);
    virtual void addLine(const DL_LineData& data);
    virtual void addArc(const DL_ArcData& data);
    virtual void addCircle(const DL_CircleData& data);
    virtual void addPolyline(const DL_PolylineData& data);
    virtual void addVertex(const DL_VertexData& data);
    virtual void add3dFace(const DL_3dFaceData& data);

    vector<CR_BLOCK> blockS;
    vector<CR_RFID> RFIDS;
    int boundaryBlockIndex;
    double boundaryBlockLength, boundaryBlockWidth;

private:
    struct CR_POINT {
        double x;
        double y;
    };

    double zeroCheck;

    vector<CR_POINT> blockVertexS;
    CR_BLOCK addBlock;
    int stage;

    CR_RFID addRFID;

    void updateBlock(bool reset, double vertexX = 0, double vertexY = 0);
    void updateRFID(double x, double y, double r);
};


#endif //GENERATEMAPFORMDXF_DXLREADER_H
