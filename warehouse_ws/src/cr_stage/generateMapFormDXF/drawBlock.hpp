//
// Created by crti on 17-4-20.
//

#ifndef GENERATEMAPFILE_DRAWBLOCK_HPP
#define GENERATEMAPFILE_DRAWBLOCK_HPP

#include <iostream>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

//drawBlock(image,Point2f(100,100),Size2f(100,50),90);
int drawBlock(Mat& img, const Point2f& center, const Size2f& size, float angle, int color=0) {

    RotatedRect rRect(center, size, angle);
    Point2f point2f[4];
    Point point2i[4];
    rRect.points(point2f);
    for(int i=0; i<4; ++i) {
        point2i[i] = point2f[i];
    }
    fillConvexPoly(img,point2i,4,Scalar(color));
}

int drawBoundary(Mat& img, Size size, int thickness=1) {
    rectangle(img, Point(0,0), Point(size.width-1,size.height-1), Scalar(0), thickness);
}

Size calMapSize(double boundaryBlockLength, double boundaryBlockWidth, double resolution = 0.025) {
    //boundaryBlockLength & boundaryBlockWidth is in metre
    int sizeX, sizeY;

    sizeX = (int)(boundaryBlockLength/resolution);
    sizeY = (int)(boundaryBlockWidth/resolution);

    cout<<"[x,y]: ["<<boundaryBlockLength<<"m,"<<boundaryBlockWidth<<"m]"<<endl;
    cout<<"size: ["<<sizeX<<","<<sizeY<<"]"<<endl;

    return Size(sizeX,sizeY);
}

#endif //GENERATEMAPFILE_DRAWBLOCK_HPP
