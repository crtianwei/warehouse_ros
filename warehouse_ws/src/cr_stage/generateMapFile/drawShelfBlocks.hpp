//
// Created by crti on 17-4-21.
//

//resolution: 0.05, 1 pixel ~ 0.05m,  20 pixel ~ 1m
//            0.025, 1 pixel ~ 0.025m,  40 pixel ~ 1m

#ifndef GENERATEMAPFILE_DRAWSHELFBLOCKS_HPP
#define GENERATEMAPFILE_DRAWSHELFBLOCKS_HPP

#include "drawBlock.hpp"
#include <iostream>
using namespace std;

int drawShelfBlocks(Mat& img, int shelfBlockNumX = 16, int shelfBlockNumY = 11, double resolution = 0.025) {
    double length, width, x, y, theta;//x: centerX
    int L, W, X, Y, T;
    x = 0;
    y = 0;
    theta = 0;
    for(int i=0; i<shelfBlockNumX; ++i) {
        length = (i == 8 || i == 9) ? 10.0 : 8.0;
        if(i%2 == 0) {
            x += 3;
        }
        else {
            x += 2;
        }
        x += length/2;

        for(int j=0; j<shelfBlockNumY; ++j) {
            width = 0.5;
            if(j == 0) {
                y += 3;
                y += width/2;
            }
            else if(j == 1) {
                y += 1;
                y += width/2;
            }
            else {
                y += 1.5;
                y += width/2;
            }

            L = (int)(length/resolution);
            W = (int)(width/resolution);
            X = (int)(x/resolution);
            Y = (int)(y/resolution);
            T = (int)(theta);
            //cout<<"X1 "<<(x-length/2)*1000<<","<<(y-width/2)*1000<<"  X2 "<<(x+length/2)*1000<<","<<(y+width/2)*1000<<endl;
            cout<<"["<<(x-length/2+0.1)*1000<<","<<(y-width/2-0.1)*1000<<"]  ";
            cout<<"["<<(x+length/2-0.1)*1000<<","<<(y-width/2-0.1)*1000<<"]  ";
            cout<<"["<<(x+length/2+0.1)*1000<<","<<(y-width/2+0.1)*1000<<"]  ";
            cout<<"["<<(x+length/2+0.1)*1000<<","<<(y+width/2-0.1)*1000<<"]  ";
            cout<<"["<<(x+length/2-0.1)*1000<<","<<(y+width/2+0.1)*1000<<"]  ";
            cout<<"["<<(x-length/2+0.1)*1000<<","<<(y+width/2+0.1)*1000<<"]  ";
            cout<<"["<<(x-length/2-0.1)*1000<<","<<(y+width/2-0.1)*1000<<"]  ";
            cout<<"["<<(x-length/2-0.1)*1000<<","<<(y-width/2+0.1)*1000<<"]  "<<endl;
            drawBlock(img,Point2f(X,Y),Size2f(L,W),T);
            y += width/2;
        }

        x += length/2;
        y = 0;
    }

}

Size calMapSize(int shelfBlockNumX = 16, int shelfBlockNumY = 11, double resolution = 0.025) {
    double x=0.0, y=0.0;
    int sizeX, sizeY;

    x+=3;
    for(int i=0; i<shelfBlockNumX; ++i) {
        if(i == 0) {
            x+=8;
        }
        else if(i%2 == 0) {
            x+=3;
            x+=8;
        }
        else {
            x+=2;
            x+=8;
        }
        if(i == 8 || i == 9) {
            x+=2;//8th & 9th shelfs' length is 10m, others 8m.
        }
    }
    x+=3;

    y+=3;
    for(int j=0; j<shelfBlockNumY; ++j) {
        if(j == 0) {
            y+=0.5;
        }
        else if(j == 1) {
            y+=1;
            y+=0.5;
        }
        else {
            y+=1.5;
            y+=0.5;
        }
    }
    y+=3;

    sizeX = (int)(x/resolution);
    sizeY = (int)(y/resolution);

    cout<<"[x,y]: ["<<x<<"m,"<<y<<"m]"<<endl;
    cout<<"size: ["<<sizeX<<","<<sizeY<<"]"<<endl;

    return Size(sizeX,sizeY);
}

#endif //GENERATEMAPFILE_DRAWSHELFBLOCKS_HPP
