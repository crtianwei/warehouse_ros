#include <iostream>
#include <opencv2/opencv.hpp>
#include "drawShelfBlocks.hpp"
#include "drawBoxBlocks.hpp"
#include "drawSimMap.hpp"

using namespace cv;
using namespace std;

int main() {
    Size imgSize = calMapSize(3,5,0.025);

    Mat shelfImg(imgSize,CV_8UC1,Scalar(255));
    drawBoundary(shelfImg, imgSize);
    drawShelfBlocks(shelfImg,3,5,0.025);
    imshow("ShelfBlocks",shelfImg);

    Mat boxImg(imgSize,CV_8UC1,Scalar(255));
    drawBoundary(boxImg, imgSize);
    drawBoxBlocks(boxImg,3,5,0.025);
    imshow("BoxBlocks",boxImg);

    imgSize = calSimMapSize();
    Mat SimImg(imgSize,CV_8UC1,Scalar(255));
    drawBoundary(SimImg, imgSize);
    drawSimMap(SimImg, imgSize);
    imshow("SimMap",SimImg);

    //imwrite("../../stage/shelfMap.png", shelfImg);
    //imwrite("../../stage/boxMap.png", boxImg);
    imwrite("../../stage/SimMap.png", SimImg);

//    double min_,max_;
//    for(int i=0; i<10000; ++i) {
//        double tmp = generateGaussianNoise(0.01);
//        min_ = min(min_,tmp);
//        max_ = max(max_,tmp);
//        cout<<tmp<<" ";
//    }
//    cout<<endl<<min_<<" "<<max_<<endl;

    waitKey(0);
    return 0;
}