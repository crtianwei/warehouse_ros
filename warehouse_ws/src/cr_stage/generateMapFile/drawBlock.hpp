//
// Created by crti on 17-4-20.
//

#ifndef GENERATEMAPFILE_DRAWBLOCK_HPP
#define GENERATEMAPFILE_DRAWBLOCK_HPP

#include <iostream>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

//drawBlock(image,Point2f(100,100),Size2f(100,50),90);
int drawBlock(Mat& img, const Point2f& center, const Size2f& size, float angle) {

    RotatedRect rRect(center, size, angle);
    Point2f point2f[4];
    Point point2i[4];
    rRect.points(point2f);
    for(int i=0; i<4; ++i) {
        point2i[i] = point2f[i];
    }
    fillConvexPoly(img,point2i,4,Scalar(0));
}

int drawTriangle(Mat& img, const Point2f& p1, const Point2f& p2, const Point2f& p3){
    Point point2i[3];
    point2i[0] = p1;
    point2i[1] = p2;
    point2i[2] = p3;
    fillConvexPoly(img,point2i,3,Scalar(0));
}

int drawBoundary(Mat& img, Size size, int thickness=1) {
    rectangle(img, Point(0,0), Point(size.width-1,size.height-1), Scalar(0), thickness);
}

#endif //GENERATEMAPFILE_DRAWBLOCK_HPP
