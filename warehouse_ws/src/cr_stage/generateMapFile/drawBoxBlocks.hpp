//
// Created by crti on 17-4-21.
//

#ifndef GENERATEMAPFILE_DRAWBOXBLOCKS_HPP
#define GENERATEMAPFILE_DRAWBOXBLOCKS_HPP

#include "drawBlock.hpp"
#include <iostream>
using namespace std;

double generateGaussianNoise(double var) {
    var = var/3;
    static bool haveSpare = false;
    static double rand1, rand2;

    if(haveSpare) {
        haveSpare = false;
        return var * sqrt(rand1) * sin(rand2);
    }

    haveSpare = true;

    rand1 = rand() / (double)RAND_MAX;
    if(rand1 < 1e-100) rand1 = 1e-100;
    rand1 = -2 * log(rand1);
    rand2 = (rand() / (double)RAND_MAX) * M_PI * 2;

    return var * sqrt(rand1) * cos(rand2);
}

int drawBoxBlocks(Mat& img, int shelfBlockNumX = 16, int shelfBlockNumY = 11, double resolution = 0.025) {
    double length, width, x, y, theta;//x: centerX
    int L, W, X, Y, T;
    x = 0;
    y = 0;
    theta = 0;
    for(int i=0; i<shelfBlockNumX; ++i) {
        length = (i == 8 || i == 9) ? 10.0 : 8.0;
        if(i%2 == 0) {
            x += 3;
        }
        else {
            x += 2;
        }
        x += length/2;

        for(int j=0; j<shelfBlockNumY; ++j) {
            width = 0.5;
            if(j == 0) {
                y += 3;
                y += width/2;
            }
            else if(j == 1) {
                y += 1;
                y += width/2;
            }
            else {
                y += 1.5;
                y += width/2;
            }

            int boxNumPerShelf = (int)(length/width);
            for(int k=0; k<boxNumPerShelf; ++k) {
                L = (int)(width / resolution);
                W = (int)(width / resolution);
                X = (int)((x - length/2 + k*width + generateGaussianNoise(0.02)) / resolution);
                Y = (int)((y + generateGaussianNoise(0.05)) / resolution);
                T = (int)(theta + generateGaussianNoise(10));
                drawBlock(img,Point2f(X,Y),Size2f(L,W),T);
            }

            y += width/2;
        }

        x += length/2;
        y = 0;
    }
}



#endif //GENERATEMAPFILE_DRAWBOXBLOCKS_HPP
