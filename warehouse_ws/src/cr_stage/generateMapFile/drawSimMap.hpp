//
// Created by crti on 12/13/17.
//

#ifndef GENERATEMAPFILE_DRAWSIMMAP_HPP
#define GENERATEMAPFILE_DRAWSIMMAP_HPP


#include "drawBlock.hpp"
#include <iostream>
using namespace std;

Size calSimMapSize(float meterX = 26, float meterY = 16, float resolution = 0.005){
    int sizeX=0, sizeY=0;
    sizeX = (int)(meterX/resolution);
    sizeY = (int)(meterY/resolution);
    cout<<"[x,y]: ["<<meterX<<"m,"<<meterY<<"m]"<<endl;
    cout<<"size: ["<<sizeX<<","<<sizeY<<"]"<<endl;
    return Size(sizeX,sizeY);
}

int drawSimMap(Mat& img, Size& size, float resolution = 0.005) {
    float r = resolution;
    float h = size.height;
    drawBlock(img, Point2f(5.0/r,h-5.0/r), Size2f(6.0/r,6.0/r), 0);

    drawBlock(img, Point2f(13.5/r,h-5.0/r), Size2f(7.0/r,6.0/r), 0);
    drawBlock(img, Point2f(21.5/r,h-5.0/r), Size2f(5.0/r,6.0/r), 0);
    //drawBlock(img, Point2f(13.25/r,h-5.0/r), Size2f(6.5/r,6.0/r), 0);
    //drawBlock(img, Point2f(21.75/r,h-5.0/r), Size2f(4.5/r,6.0/r), 0);

    drawBlock(img, Point2f(5.0/r,h-13.0/r), Size2f(6.0/r,6.0/r), 0);
    drawBlock(img, Point2f(12.0/r,h-12.0/r), Size2f(4.0/r,4.0/r), 0);
    drawBlock(img, Point2f(23.0/r,h-12.0/r), Size2f(2.0/r,4.0/r), 0);

    drawTriangle(img, Point2f(14.0/r,h-10.0/r), Point2f(16.0/r,h-10.0/r), Point2f(14.0/r,h-14.0/r));

    drawTriangle(img, Point2f(16.0/r,h-14.0/r), Point2f(20.0/r,h-14.0/r), Point2f(18.0/r,h-10.0/r));
    //drawTriangle(img, Point2f(16.5/r,h-14.0/r), Point2f(19.5/r,h-14.0/r), Point2f(18.0/r,h-11.0/r));

    drawTriangle(img, Point2f(20.0/r,h-10.0/r), Point2f(22.0/r,h-10.0/r), Point2f(22.0/r,h-14.0/r));
}






#endif //GENERATEMAPFILE_DRAWSIMMAP_HPP
