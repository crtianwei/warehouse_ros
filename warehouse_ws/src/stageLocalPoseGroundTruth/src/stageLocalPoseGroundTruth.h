//
// Created by crti on 5/10/17.
//

//resolution: 0.05, 1 pixel ~ 0.05m,  20 pixel ~ 1m
//            0.025, 1 pixel ~ 0.025m,  40 pixel ~ 1m

#ifndef STAGELOCALPOSEGROUNDTRUTH_STAGELOCALPOSEGROUNDTRUTH_H
#define STAGELOCALPOSEGROUNDTRUTH_STAGELOCALPOSEGROUNDTRUTH_H

#include <iostream>
#include <opencv2/opencv.hpp>

#include "ros/ros.h"
#include "geometry_msgs/Point.h"
#include "nav_msgs/Odometry.h"

#include <tf/tf.h>

#include "Lane.h"

using namespace cv;
using namespace std;


class stageLocalPoseGroundTruth {
public:
    stageLocalPoseGroundTruth();
    ~stageLocalPoseGroundTruth();

    float resolution;



private:
    Mat shelfMap;
    //ros
    ros::NodeHandle nh_;
    ros::NodeHandle private_nh_;

    //Subscriber & Publisher
    ros::Subscriber odometrySub;
    void odometryCallback(const nav_msgs::OdometryConstPtr& odometry);

    ros::Subscriber localPoseSub;
    void localPoseCallback(const featureDetection::LaneConstPtr &localPose);

    ros::Publisher stageLocalPoseGroundTruthPub;

    double error2D, error2T, errorNum;
    featureDetection::Lane laneGroudTruth;

    double angleTo90(double inputAngle);

};


#endif //STAGELOCALPOSEGROUNDTRUTH_STAGELOCALPOSEGROUNDTRUTH_H
