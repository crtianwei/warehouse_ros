//
// Created by crti on 5/10/17.
//

#include "ros/ros.h"
#include "stageLocalPoseGroundTruth.h"

int main(int argc, char** argv) {
    ros::init(argc, argv, "stageLocalPoseGroundTruth");

    stageLocalPoseGroundTruth slpgt;

    ros::spin();
    return 0;
}