//
// Created by crti on 5/10/17.
//

#include "stageLocalPoseGroundTruth.h"

//todo: groundTruth exit jump, and cannot judge updown or leftright to output

stageLocalPoseGroundTruth::stageLocalPoseGroundTruth() :
        private_nh_("~")
{
    resolution = 0.025;
    error2D = 0;
    error2T = 0;
    errorNum = 0;

    shelfMap = imread("shelfMap.png",CV_LOAD_IMAGE_UNCHANGED);

    odometrySub = nh_.subscribe("odom", 1, &stageLocalPoseGroundTruth::odometryCallback, this);
    localPoseSub = nh_.subscribe("localPose", 1, &stageLocalPoseGroundTruth::localPoseCallback, this);

    stageLocalPoseGroundTruthPub = nh_.advertise<featureDetection::Lane>("localPoseGroudTruth",0);

}

stageLocalPoseGroundTruth::~stageLocalPoseGroundTruth() {}

void stageLocalPoseGroundTruth::odometryCallback(const nav_msgs::OdometryConstPtr &odometry)
{
    laneGroudTruth.header.stamp = ros::Time::now();

    int mapWidth, mapHeight;
    mapWidth = shelfMap.size().width;
    mapHeight = shelfMap.size().height;
    int pixelX, pixelY;
    pixelX = (int)(odometry->pose.pose.position.x / resolution);
    pixelY = mapHeight - (int)(odometry->pose.pose.position.y / resolution);
    double angle = tf::getYaw(odometry->pose.pose.orientation) * 180 / M_PI;
    //cout<<mapWidth<<","<<mapHeight<<","<<angle<<","<<(int)shelfMap.ptr<uchar>(pixelY)[pixelX]<<endl;
    int d_left=0, d_right=0, d_up=0, d_down=0;

    if(pixelX<0 || pixelX>mapWidth) return;
    if(pixelY<0 || pixelY>mapHeight) return;

    if((int)shelfMap.ptr<uchar>(pixelY)[pixelX] == 0) {//in block
        //cout << "stageLocalPoseGroundTruth:: point in shelf blocks" << endl;
    }
    else if((int)shelfMap.ptr<uchar>(pixelY)[pixelX] == 100){//in cross
        //cal d*4
        for(d_left = 0; pixelX-d_left > 0; ++d_left) {
            if((int)shelfMap.ptr<uchar>(pixelY)[pixelX-d_left] == 0 || (int)shelfMap.ptr<uchar>(pixelY)[pixelX-d_left] == 255) {
                break;
            }
        }
        for(d_right = 0; pixelX+d_right < mapWidth; ++d_right) {
            if((int)shelfMap.ptr<uchar>(pixelY)[pixelX+d_right] == 0 || (int)shelfMap.ptr<uchar>(pixelY)[pixelX+d_right] == 255) {
                break;
            }
        }
        for(d_up = 0; pixelY-d_up > 0; ++d_up) {
            if((int)shelfMap.ptr<uchar>(pixelY-d_up)[pixelX] == 0 || (int)shelfMap.ptr<uchar>(pixelY-d_up)[pixelX] == 255) {
                break;
            }
        }
        for(d_down = 0; pixelY+d_down < mapHeight; ++d_down) {
            if((int)shelfMap.ptr<uchar>(pixelY+d_down)[pixelX] == 0 || (int)shelfMap.ptr<uchar>(pixelY+d_down)[pixelX] == 255) {
                break;
            }
        }
        //cal lane by angle
        if(abs(angle)<45){//-45~45
            laneGroudTruth.leftDistance = d_up*resolution;
            laneGroudTruth.rightDistance = d_down*resolution;
            laneGroudTruth.frontDistance = d_right*resolution;
            laneGroudTruth.leftTheta = laneGroudTruth.rightTheta = angleTo90(angle);
            laneGroudTruth.frontTheta = angleTo90(angleTo90(angle)+90);
            stageLocalPoseGroundTruthPub.publish(laneGroudTruth);
        }
        else if(abs(angle)>135){//135~-135
            laneGroudTruth.leftDistance = d_down*resolution;
            laneGroudTruth.rightDistance = d_up*resolution;
            laneGroudTruth.frontDistance = d_left*resolution;
            laneGroudTruth.leftTheta = laneGroudTruth.rightTheta = angleTo90(angle);//?
            laneGroudTruth.frontTheta = angleTo90(angleTo90(angle)+90);
            stageLocalPoseGroundTruthPub.publish(laneGroudTruth);
        }
        else if(angle>0){//45~135
            laneGroudTruth.leftDistance = d_left*resolution;
            laneGroudTruth.rightDistance = d_right*resolution;
            laneGroudTruth.frontDistance = d_up*resolution;
            laneGroudTruth.leftTheta = laneGroudTruth.rightTheta = angleTo90(angle-90);
            laneGroudTruth.frontTheta = angleTo90(angleTo90(angle-90)+90);
            stageLocalPoseGroundTruthPub.publish(laneGroudTruth);
        }
        else{//-45~-135
            laneGroudTruth.leftDistance = d_right*resolution;
            laneGroudTruth.rightDistance = d_left*resolution;
            laneGroudTruth.frontDistance = d_down*resolution;
            laneGroudTruth.leftTheta = laneGroudTruth.rightTheta = angleTo90(angle+90);
            laneGroudTruth.frontTheta = angleTo90(angleTo90(angle+90)+90);
            stageLocalPoseGroundTruthPub.publish(laneGroudTruth);
        }
    }
    else if((int)shelfMap.ptr<uchar>(pixelY)[pixelX] == 255){//in lane
        //cal lane by angle
        if(abs(angle)<45){//-45~45
            for(d_up = 0; pixelY-d_up > 0; ++d_up) {
                if((int)shelfMap.ptr<uchar>(pixelY-d_up)[pixelX] == 0 || (int)shelfMap.ptr<uchar>(pixelY-d_up)[pixelX] == 100) {
                    break;
                }
            }
            for(d_down = 0; pixelY+d_down < mapHeight; ++d_down) {
                if((int)shelfMap.ptr<uchar>(pixelY+d_down)[pixelX] == 0 || (int)shelfMap.ptr<uchar>(pixelY+d_down)[pixelX] == 100) {
                    break;
                }
            }
            bool enterCross = false;
            for(d_right = 0; pixelX+d_right < mapWidth; ++d_right) {
                if((int)shelfMap.ptr<uchar>(pixelY)[pixelX+d_right] == 0){
                    break;
                }
                else if((int)shelfMap.ptr<uchar>(pixelY)[pixelX+d_right] == 100){
                    enterCross = true;
                }
                else if((int)shelfMap.ptr<uchar>(pixelY)[pixelX+d_right] == 255){
                    if(enterCross){
                        break;
                    }
                }
            }
            laneGroudTruth.leftDistance = d_up*resolution;
            laneGroudTruth.rightDistance = d_down*resolution;
            laneGroudTruth.frontDistance = d_right*resolution;
            laneGroudTruth.leftTheta = laneGroudTruth.rightTheta = angleTo90(angle);
            laneGroudTruth.frontTheta = angleTo90(angleTo90(angle)+90);
            stageLocalPoseGroundTruthPub.publish(laneGroudTruth);
        }
        else if(abs(angle)>135){//135~-135
            for(d_up = 0; pixelY-d_up > 0; ++d_up) {
                if((int)shelfMap.ptr<uchar>(pixelY-d_up)[pixelX] == 0 || (int)shelfMap.ptr<uchar>(pixelY-d_up)[pixelX] == 100) {
                    break;
                }
            }
            for(d_down = 0; pixelY+d_down < mapHeight; ++d_down) {
                if((int)shelfMap.ptr<uchar>(pixelY+d_down)[pixelX] == 0 || (int)shelfMap.ptr<uchar>(pixelY+d_down)[pixelX] == 100) {
                    break;
                }
            }
            bool enterCross = false;
            for(d_left = 0; pixelX-d_left > 0; ++d_left) {
                if((int)shelfMap.ptr<uchar>(pixelY)[pixelX-d_left] == 0){
                    break;
                }
                else if((int)shelfMap.ptr<uchar>(pixelY)[pixelX-d_left] == 100){
                    enterCross = true;
                }
                else if((int)shelfMap.ptr<uchar>(pixelY)[pixelX-d_left] == 255){
                    if(enterCross){
                        break;
                    }
                }
            }
            laneGroudTruth.leftDistance = d_down*resolution;
            laneGroudTruth.rightDistance = d_up*resolution;
            laneGroudTruth.frontDistance = d_left*resolution;
            laneGroudTruth.leftTheta = laneGroudTruth.rightTheta = angleTo90(angle);//?
            laneGroudTruth.frontTheta = angleTo90(angleTo90(angle)+90);
            stageLocalPoseGroundTruthPub.publish(laneGroudTruth);
        }
        else if(angle>0){//45~135
            for(d_left = 0; pixelX-d_left > 0; ++d_left) {
                if((int)shelfMap.ptr<uchar>(pixelY)[pixelX-d_left] == 0 || (int)shelfMap.ptr<uchar>(pixelY)[pixelX-d_left] == 100) {
                    break;
                }
            }
            for(d_right = 0; pixelX+d_right < mapWidth; ++d_right) {
                if((int)shelfMap.ptr<uchar>(pixelY)[pixelX+d_right] == 0 || (int)shelfMap.ptr<uchar>(pixelY)[pixelX+d_right] == 100) {
                    break;
                }
            }
            bool enterCross = false;
            for(d_up = 0; pixelY-d_up > 0; ++d_up) {
                if((int)shelfMap.ptr<uchar>(pixelY-d_up)[pixelX] == 0){
                    break;
                }
                else if((int)shelfMap.ptr<uchar>(pixelY-d_up)[pixelX] == 100){
                    enterCross = true;
                }
                else if((int)shelfMap.ptr<uchar>(pixelY-d_up)[pixelX] == 255){
                    if(enterCross){
                        break;
                    }
                }
            }
            laneGroudTruth.leftDistance = d_left*resolution;
            laneGroudTruth.rightDistance = d_right*resolution;
            laneGroudTruth.frontDistance = d_up*resolution;
            laneGroudTruth.leftTheta = laneGroudTruth.rightTheta = angleTo90(angle-90);
            laneGroudTruth.frontTheta = angleTo90(angleTo90(angle-90)+90);
            stageLocalPoseGroundTruthPub.publish(laneGroudTruth);
        }
        else{//-45~-135
            for(d_left = 0; pixelX-d_left > 0; ++d_left) {
                if((int)shelfMap.ptr<uchar>(pixelY)[pixelX-d_left] == 0 || (int)shelfMap.ptr<uchar>(pixelY)[pixelX-d_left] == 100) {
                    break;
                }
            }
            for(d_right = 0; pixelX+d_right < mapWidth; ++d_right) {
                if((int)shelfMap.ptr<uchar>(pixelY)[pixelX+d_right] == 0 || (int)shelfMap.ptr<uchar>(pixelY)[pixelX+d_right] == 100) {
                    break;
                }
            }
            bool enterCross = false;
            for(d_down = 0; pixelY+d_down < mapHeight; ++d_down) {
                if((int)shelfMap.ptr<uchar>(pixelY+d_down)[pixelX] == 0){
                    break;
                }
                else if((int)shelfMap.ptr<uchar>(pixelY+d_down)[pixelX] == 100){
                    enterCross = true;
                }
                else if((int)shelfMap.ptr<uchar>(pixelY+d_down)[pixelX] == 255){
                    if(enterCross){
                        break;
                    }
                }
            }
            laneGroudTruth.leftDistance = d_right*resolution;
            laneGroudTruth.rightDistance = d_left*resolution;
            laneGroudTruth.frontDistance = d_down*resolution;
            laneGroudTruth.leftTheta = laneGroudTruth.rightTheta = angleTo90(angle+90);
            laneGroudTruth.frontTheta = angleTo90(angleTo90(angle+90)+90);
            stageLocalPoseGroundTruthPub.publish(laneGroudTruth);
        }
    }
}

void stageLocalPoseGroundTruth::localPoseCallback(const featureDetection::LaneConstPtr &localPose){
//    localPoseTmp
//    else {
//
//
//        errorNum++;
//        error2D += abs(localPoseTmp.x - d_y_down*resolution);// * (localPoseTmp.x - groundTruth.y);
//        error2T += abs(localPoseTmp.y - angle);// * (localPoseTmp.y - groundTruth.z);
//        double eD = error2D/errorNum;
//        double eT = error2T/errorNum;
//        cout<<eD<<" "<<eT<<" "<<localPoseTmp.x <<" "<< d_y_down*resolution<<endl;
//    }
//
//    stageLocalPoseGroundTruthPub.publish(groundTruth);
    cout << laneGroudTruth.rightDistance << "," <<  localPose->rightDistance<<endl;//
    //cout <<laneGroudTruth.rightTheta << "," << localPose->rightTheta<< endl;

}

double stageLocalPoseGroundTruth::angleTo90(double inputAngle){
    if(abs(inputAngle)<=90){//-90~90
        return inputAngle;
    }
    else if(inputAngle>90){//90~180
        return inputAngle-180;
    }
    else if(inputAngle<-90){//-90~-180
        return inputAngle+180;
    }
    else{//error
        return -1;
    }
}

