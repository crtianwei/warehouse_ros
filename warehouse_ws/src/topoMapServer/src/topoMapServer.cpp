//
// Created by crti on 5/15/17.
//

#include "topoMapServer.h"

topoMapServe::topoMapServe() :
        private_nh_("~")
{
    RFIDRadius = 0.1;
    //RFIDLogFileName = "RFID.txt";
    //PATHLogFileName = "PATH.txt";
    RFIDLogFileName = "RFIDSIM.txt";
    PATHLogFileName = "PATHSIM.txt";

    currentGoalIndex = 0;
    currentTask = {-1,-1,-1,-1};

    loadRFIDsFromFile(RFIDLogFileName);
    loadPATHsFromFile(PATHLogFileName);
    //printPointMap();
    //printId2seqMap();
    //printPathMap();

    RFIDSub = nh_.subscribe("RFIDInfo", 1, &topoMapServe::RFIDCallback, this);
    topoTaskSub = nh_.subscribe("topoTask", 3, &topoMapServe::topoTaskCallback, this);

    topoAllRFIDPub = nh_.advertise<visualization_msgs::Marker>("topoAllRFID", 0);
    topoAllPathPub = nh_.advertise<visualization_msgs::Marker>("topoAllPath", 0);

    topoCurrentRFIDPub = nh_.advertise<visualization_msgs::Marker>("topoCurrentRFID", 0);
    topoCurrentPathPub = nh_.advertise<visualization_msgs::Marker>("topoCurrentPath", 0);

    //topoTaskRespondPub = nh_.advertise<topoMapServe::TopoRespond>("topoRespond", 3);

    while(ros::ok()) {

        publishAllPath();
        publishAllPoint();
        //publishDemo();
        sleep(1);
    }
}

void topoMapServe::RFIDCallback(const std_msgs::Int32ConstPtr& RFIDRecv)
{
    currentRFID = RFIDRecv->data;
    if(currentRFID == -1){
        //
    }
    else if(id2seqMap.count(currentRFID)>0){
        int seq = id2seqMap[currentRFID];
        //check goal reach
        if(seq == currentTask[currentGoalIndex]){//reach current goal
            topoMapServer::TopoRespond resp;
            resp.header.stamp = ros::Time::now();
            resp.goalReached = 1;
            resp.reachedGoalSeq = currentGoalIndex;
            resp.errorNum = 0;

            topoTaskRespondPub.publish(resp);
        }
        else{//reach next goal (jump some point
            for(int i = currentGoalIndex; i<currentTask.size(); ++i){
                if(seq == currentTask[i]){
                    //do sth
                    break;
                }
            }
        }
    }
    else{
        cout<<"RFID["<<currentRFID<<"] not in the map"<<endl;
    }

}

void topoMapServe::topoTaskCallback(const topoMapServer::TopoTaskConstPtr& taskRecv)
{
    //todo: add init

    int tmpIndex = currentGoalIndex;
    vector<int> tmpCurrentTask = currentTask;
    vector<int> tmpTask;
    tmpTask.insert(tmpTask.begin(), tmpCurrentTask.begin(), tmpCurrentTask.begin()+tmpIndex);

    int currentGoal = currentTask[currentGoalIndex];
    //todo: check current next goal
    if(currentGoal == taskRecv->lastGoal){
        //do nothing
    }
    else if(currentGoal == taskRecv->currentGoal){
        tmpTask.push_back(taskRecv->nextGoal);
        tmpTask.push_back(taskRecv->nextNextGoal);
    }
    else if(currentGoal == taskRecv->nextGoal){
        tmpTask.push_back(taskRecv->nextNextGoal);
    }
    else if(currentGoal == taskRecv->nextNextGoal){
        //do nothing
    }

    if(tmpTask.size()>=4){
        vector<int>().swap(currentTask);
        currentTask.insert(currentTask.begin(), tmpTask.end()-3, tmpTask.end());
        currentGoalIndex = 4 - (tmpTask.size() - (tmpIndex + 1)) - 1;
        //todo: check >0 or other
    }
    else{
        //todo: expection jump points
    }

//    if(tmpIndex == 0){
//        //init or error
//    }
//    else if(tmpIndex == 1){
//        //commen state(might wrong
//    }
//    else if(tmpIndex == 2){
//        //jump a point or error(might wrong
//    }
//    else if(tmpIndex == 3){
//        //jump 2 points or error(might wrong
//    }
//    else{
//        //unexpected error
//    }



}

void topoMapServe::loadRFIDsFromFile(string _RFIDLogFileName)
{
    fstream RFIDLogFile;
    RFIDLogFile.open(_RFIDLogFileName, ios::in);
    if(!RFIDLogFile.good()) {
        cout << "topoMapServer: read RFID log file (" << _RFIDLogFileName << " ) failed." <<endl;
        return;
    }

    string line;
    while(getline(RFIDLogFile, line)) {
        TOPO_RFID addRFID;
        istringstream lineStream(line);
        string RFIDInfo;
        int i = 0;
        while(getline(lineStream, RFIDInfo, ',')) {
            switch(i) {
                case 0:
                    //ID
                    addRFID.seq = stoi(RFIDInfo);
                    cout<<"seq:"<<addRFID.seq<<" ";
                    break;
                case 1:
                    //x
                    addRFID.x = stod(RFIDInfo);
                    cout<<"X:"<<addRFID.x<<" ";
                    break;
                case 2:
                    //y
                    addRFID.y = stod(RFIDInfo);
                    cout<<"Y:"<<addRFID.y<<" ";
                    break;
                default:
                    //cout<< "topoMapServer: read RFID info failed. Line: " << line << endl;
                    addRFID.RFIDs.push_back(stoi(RFIDInfo));
                    cout<<"RIFD["<<addRFID.RFIDs.size()<<"]:"<<addRFID.RFIDs.back()<<" ";
                    break;
            }
            ++i;
        }
        cout<<endl;
        //todo: add txt input exception handling

        //add to pointMap
        pointMap[addRFID.seq] = addRFID;
        //cout<<"_____________"<<endl<<pointMap.size()<<endl;

        //add to id2seqMap
        for(auto &rfid:addRFID.RFIDs){
            id2seqMap[rfid] = addRFID.seq;
        }
    }
}

void topoMapServe::loadPATHsFromFile(string _PATHLogFileName)
{
    fstream PATHLogFile;
    PATHLogFile.open(_PATHLogFileName, ios::in);
    if(!PATHLogFile.good()) {
        cout << "topoMapServer: read PATH log file (" << _PATHLogFileName << " ) failed." <<endl;
        return;
    }

    string line;
    while(getline(PATHLogFile, line)) {
        TOPO_PATH addPATH;
        istringstream lineStream(line);
        string PATHInfo;
        int i = 0;
        while(getline(lineStream, PATHInfo, ',')) {
            switch(i) {
                case 0:
                    //beginPointSeq
                    addPATH.beginPointSeq = stoi(PATHInfo);
                    cout<<"beginPointSeq:"<<addPATH.beginPointSeq<<" ";
                    break;
                case 1:
                    //endPointSeq
                    addPATH.endPointSeq = stoi(PATHInfo);
                    cout<<"endPointSeq:"<<addPATH.endPointSeq<<" ";
                    break;
                case 2:
                    //laneLength
                    addPATH.laneLength = stod(PATHInfo);
                    cout<<"laneLength:"<<addPATH.laneLength<<" ";
                    break;
                case 3:
                    //laneWidth
                    addPATH.laneWidth = stod(PATHInfo);
                    cout<<"laneWidth:"<<addPATH.laneWidth<<" ";
                default:
                    cout<< "topoMapServer: read PATH info failed. Line: " << line << endl;
                    break;
            }
            ++i;
        }
        cout<<endl;
        //todo: add txt input exception handling

        //add to pathMap
        pathMap[addPATH.beginPointSeq].push_back(addPATH);
    }
}

void topoMapServe::printPointMap()
{
    cout<<"topoMapServer: print pointMap"<<endl;
    for(auto &pointTmp:pointMap){
        cout<<"seq:"<<pointTmp.first<<" seq:"<<pointTmp.second.seq<<" x:"<< pointTmp.second.x<<" y:"<<pointTmp.second.y<<" RFID";
        for(auto &rfid:pointTmp.second.RFIDs){
            cout<<rfid<<" ";
        }
        cout<<endl;
    }
    cout<<"topoMapServer: print pointMap finished"<<endl;
}
void topoMapServe::printId2seqMap()
{
    cout<<"topoMapServer: print id2seqMap"<<endl;
    for(auto &pointTmp:id2seqMap){
        cout<<"rfid:"<<pointTmp.first<<" seq:"<<pointTmp.second<<endl;
    }
    cout<<"topoMapServer: print id2seqMap finished"<<endl;
}
void topoMapServe::printPathMap()
{
    cout<<"topoMapServer: print pathMap"<<endl;
    for(auto &pointTmp:pathMap){
        cout<<"seq:"<<pointTmp.first;
        for(auto &pathTmp:pointTmp.second){
            cout<<" point->point:"<<pathTmp.beginPointSeq<<"->"<<pathTmp.endPointSeq;
        }
        cout<<endl;
    }
    cout<<"topoMapServer: print pathMap finshed"<<endl;
}

void topoMapServe::publishAllPoint()
{
    visualization_msgs::Marker topoRFID;
    topoRFID.header.frame_id = "odom";
    topoRFID.header.stamp = ros::Time::now();
    topoRFID.ns = "topoRFID";
    topoRFID.id = 0;
    topoRFID.type = visualization_msgs::Marker::POINTS;
    topoRFID.action = visualization_msgs::Marker::MODIFY;
    topoRFID.scale.x = 0.5;
    topoRFID.scale.y = 0.5;
    topoRFID.color.b = 1.0;
    topoRFID.color.a = 1.0;

    geometry_msgs::Point p;
    p.z = 0;
    topoRFID.points.reserve(pointMap.size());
    for(auto &val:pointMap){
        //todo: check map seq == map struct seq
        p.x = val.second.x;
        //p.y = 14 - val.second.y;
        p.y = val.second.y;
        topoRFID.points.push_back(p);
    }


    topoAllRFIDPub.publish(topoRFID);
}
void topoMapServe::publishAllPath()
{
    visualization_msgs::Marker topoPATH;
    topoPATH.header.frame_id = "odom";
    topoPATH.header.stamp = ros::Time::now();
    topoPATH.ns = "topoPath";
    topoPATH.id = 0;
    topoPATH.type = visualization_msgs::Marker::LINE_LIST;
    topoPATH.action = visualization_msgs::Marker::MODIFY;
    topoPATH.scale.x = 0.05;
    topoPATH.color.b = 1.0;
    topoPATH.color.a = 1.0;

    geometry_msgs::Point p;
    p.z = 0;
    topoPATH.points.reserve(pathMap.size()*2);
    //int i=0;
    for(auto &val:pathMap){
        //i++;
        int beginPointSeq = val.first;
        for(auto &pathTMP:val.second){
            //todo: check map seq == map struct seq
            auto beginPointTMP = pointMap[pathTMP.beginPointSeq];//todo: if the value not in the map
            auto endPointTMP = pointMap[pathTMP.endPointSeq];//todo: if the value not in the map
            //cout<<i<<" "<<pathTMP.beginPointSeq<<" "<<pathTMP.endPointSeq<<" "<<pointMap.size()<<endl;
            p.x = beginPointTMP.x;
            //p.y = 14 - beginPointTMP.y;
            p.y = beginPointTMP.y;
            topoPATH.points.push_back(p);
            p.x = endPointTMP.x;
            //p.y = 14 - endPointTMP.y;
            p.y = endPointTMP.y;
            topoPATH.points.push_back(p);
        }
    }

    topoAllPathPub.publish(topoPATH);
}

void topoMapServe::publishDemo()
{
    visualization_msgs::Marker topoPath;
    topoPath.header.frame_id = "odom";
    topoPath.header.stamp = ros::Time::now();
    topoPath.ns = "topoPath";
    topoPath.id = 0;
    topoPath.type = visualization_msgs::Marker::LINE_LIST;
    topoPath.action = visualization_msgs::Marker::MODIFY;
    topoPath.scale.x = 0.1;
    topoPath.color.b = 1.0;
    topoPath.color.a = 1.0;

    geometry_msgs::Point p;
    p.x = pointMap[8].x;
    p.y = 14 - pointMap[8].y;
    topoPath.points.push_back(p);

    p.x = pointMap[9].x;
    p.y = 14 - pointMap[9].y;
    topoPath.points.push_back(p);

    p.x = pointMap[9].x;
    p.y = 14 - pointMap[9].y;
    topoPath.points.push_back(p);

    p.x = pointMap[10].x;
    p.y = 14 - pointMap[10].y;
    topoPath.points.push_back(p);

    p.z = 0;
    p.x = pointMap[10].x;
    p.y = 14 - pointMap[10].y;
    topoPath.points.push_back(p);

    p.x = pointMap[11].x;
    p.y = 14 - pointMap[11].y;
    topoPath.points.push_back(p);

//    p.x = pointMap[10].x;
//    p.y = 14 - pointMap[10].y;
//    topoPath.points.push_back(p);
//
//    p.x = pointMap[11].x;
//    p.y = 14 - pointMap[11].y;
//    topoPath.points.push_back(p);
//
//    p.x = pointMap[11].x;
//    p.y = 14 - pointMap[11].y;
//    topoPath.points.push_back(p);
//
//    p.x = pointMap[18].x;
//    p.y = 14 - pointMap[18].y;
//    topoPath.points.push_back(p);
//
//    p.z = 0;
//    p.x = pointMap[18].x;
//    p.y = 14 - pointMap[18].y;
//    topoPath.points.push_back(p);
//
//    p.x = pointMap[19].x;
//    p.y = 14 - pointMap[19].y;
//    topoPath.points.push_back(p);

    topoCurrentPathPub.publish(topoPath);
}