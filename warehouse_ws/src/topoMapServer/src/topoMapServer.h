//
// Created by crti on 5/15/17.
//

#ifndef TOPOMAPSERVER_TOPOMAPSERVER_H
#define TOPOMAPSERVER_TOPOMAPSERVER_H

#include <iostream>
#include <vector>
#include <map>
#include <fstream>
#include <algorithm>
#include "ros/ros.h"
#include "visualization_msgs/Marker.h"
#include "std_msgs/builtin_int32.h"
#include <tf/tf.h>
#include "../msg/TopoTask.h"
#include "../msg/TopoRespond.h"
using namespace std;

struct TOPO_RFID{
    int seq;
    double x;
    double y;
    vector<int> RFIDs;
};
struct TOPO_PATH{
    int beginPointSeq;
    int endPointSeq;
    double laneLength;
    double laneWidth;
};

class topoMapServe {
public:
    topoMapServe();
    ~topoMapServe(){};

    double RFIDRadius;
    string RFIDLogFileName;
    string PATHLogFileName;

    //RFID points
    //  seq  x,y,RFID[]
    map<int, TOPO_RFID> pointMap;

    //RFID to seq
    //  RFID seq
    map<int, int> id2seqMap;

    //edges, paths
    //  point1 seq  point2seq,length,width
    map<int, vector<TOPO_PATH>> pathMap;

    //task about
    int currentRFID;
    int currentPointSeq;

    vector<int> currentTask;
    int currentGoalIndex;//index in currentTask


private:
    //ros
    ros::NodeHandle nh_;
    ros::NodeHandle private_nh_;

    //Subscriber & Publisher
    ros::Subscriber RFIDSub;
    void RFIDCallback(const std_msgs::Int32ConstPtr& RFIDRecv);
    ros::Subscriber topoTaskSub;
    void topoTaskCallback(const topoMapServer::TopoTaskConstPtr& taskRecv);

    ros::Publisher topoAllRFIDPub;
    ros::Publisher topoAllPathPub;
    ros::Publisher topoCurrentRFIDPub;
    ros::Publisher topoCurrentPathPub;
    ros::Publisher topoTaskRespondPub;

    void publishAllPoint();
    void publishAllPath();

    void loadRFIDsFromFile(string RFIDLogFileName);
    void loadPATHsFromFile(string RFIDLogFileName);

    void printPointMap();//print pointMap
    void printId2seqMap();//print id2seqMap
    void printPathMap();//print pathMap

    void publishDemo();



};


#endif //TOPOMAPSERVER_TOPOMAPSERVER_H
