// Generated by gencpp from file featureDetection/LineSegment.msg
// DO NOT EDIT!


#ifndef FEATUREDETECTION_MESSAGE_LINESEGMENT_H
#define FEATUREDETECTION_MESSAGE_LINESEGMENT_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace featureDetection
{
template <class ContainerAllocator>
struct LineSegment_
{
  typedef LineSegment_<ContainerAllocator> Type;

  LineSegment_()
    : A(0.0)
    , B(0.0)
    , C(0.0)
    , point1_x(0.0)
    , point1_y(0.0)
    , point2_x(0.0)
    , point2_y(0.0)
    , pointIndex()  {
    }
  LineSegment_(const ContainerAllocator& _alloc)
    : A(0.0)
    , B(0.0)
    , C(0.0)
    , point1_x(0.0)
    , point1_y(0.0)
    , point2_x(0.0)
    , point2_y(0.0)
    , pointIndex(_alloc)  {
  (void)_alloc;
    }



   typedef float _A_type;
  _A_type A;

   typedef float _B_type;
  _B_type B;

   typedef float _C_type;
  _C_type C;

   typedef float _point1_x_type;
  _point1_x_type point1_x;

   typedef float _point1_y_type;
  _point1_y_type point1_y;

   typedef float _point2_x_type;
  _point2_x_type point2_x;

   typedef float _point2_y_type;
  _point2_y_type point2_y;

   typedef std::vector<uint8_t, typename ContainerAllocator::template rebind<uint8_t>::other >  _pointIndex_type;
  _pointIndex_type pointIndex;




  typedef boost::shared_ptr< ::featureDetection::LineSegment_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::featureDetection::LineSegment_<ContainerAllocator> const> ConstPtr;

}; // struct LineSegment_

typedef ::featureDetection::LineSegment_<std::allocator<void> > LineSegment;

typedef boost::shared_ptr< ::featureDetection::LineSegment > LineSegmentPtr;
typedef boost::shared_ptr< ::featureDetection::LineSegment const> LineSegmentConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::featureDetection::LineSegment_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::featureDetection::LineSegment_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace featureDetection

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': False}
// {'featureDetection': ['/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg'], 'std_msgs': ['/opt/ros/lunar/share/std_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::featureDetection::LineSegment_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::featureDetection::LineSegment_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::featureDetection::LineSegment_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::featureDetection::LineSegment_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::featureDetection::LineSegment_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::featureDetection::LineSegment_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::featureDetection::LineSegment_<ContainerAllocator> >
{
  static const char* value()
  {
    return "05a85d86d81e7a33148958a96f822730";
  }

  static const char* value(const ::featureDetection::LineSegment_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x05a85d86d81e7a33ULL;
  static const uint64_t static_value2 = 0x148958a96f822730ULL;
};

template<class ContainerAllocator>
struct DataType< ::featureDetection::LineSegment_<ContainerAllocator> >
{
  static const char* value()
  {
    return "featureDetection/LineSegment";
  }

  static const char* value(const ::featureDetection::LineSegment_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::featureDetection::LineSegment_<ContainerAllocator> >
{
  static const char* value()
  {
    return "float32 A\n\
float32 B\n\
float32 C\n\
float32 point1_x\n\
float32 point1_y\n\
float32 point2_x\n\
float32 point2_y\n\
uint8[] pointIndex\n\
";
  }

  static const char* value(const ::featureDetection::LineSegment_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::featureDetection::LineSegment_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.A);
      stream.next(m.B);
      stream.next(m.C);
      stream.next(m.point1_x);
      stream.next(m.point1_y);
      stream.next(m.point2_x);
      stream.next(m.point2_y);
      stream.next(m.pointIndex);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct LineSegment_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::featureDetection::LineSegment_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::featureDetection::LineSegment_<ContainerAllocator>& v)
  {
    s << indent << "A: ";
    Printer<float>::stream(s, indent + "  ", v.A);
    s << indent << "B: ";
    Printer<float>::stream(s, indent + "  ", v.B);
    s << indent << "C: ";
    Printer<float>::stream(s, indent + "  ", v.C);
    s << indent << "point1_x: ";
    Printer<float>::stream(s, indent + "  ", v.point1_x);
    s << indent << "point1_y: ";
    Printer<float>::stream(s, indent + "  ", v.point1_y);
    s << indent << "point2_x: ";
    Printer<float>::stream(s, indent + "  ", v.point2_x);
    s << indent << "point2_y: ";
    Printer<float>::stream(s, indent + "  ", v.point2_y);
    s << indent << "pointIndex[]" << std::endl;
    for (size_t i = 0; i < v.pointIndex.size(); ++i)
    {
      s << indent << "  pointIndex[" << i << "]: ";
      Printer<uint8_t>::stream(s, indent + "  ", v.pointIndex[i]);
    }
  }
};

} // namespace message_operations
} // namespace ros

#endif // FEATUREDETECTION_MESSAGE_LINESEGMENT_H
