//
// Created by crti on 12/12/17.
//

#ifndef TOPONODEDELAUNAY_TOPONODEDELAUNAY_H
#define TOPONODEDELAUNAY_TOPONODEDELAUNAY_H

#include "ros/ros.h"
#include "sensor_msgs/LaserScan.h"
#include "nav_msgs/Odometry.h"
#include "visualization_msgs/Marker.h"
#include "geometry_msgs/Point.h"

#include "../msg/LineSegment.h"
#include "../msg/LineSegmentList.h"
#include "../msg/FeaturePolygon.h"

#include <opencv2/opencv.hpp>

#include <math.h>
#include <chrono>

#include <tf/tf.h>

using namespace cv;
using namespace std;

class TopoNodeDelaunay {
public:
    TopoNodeDelaunay();
    ~TopoNodeDelaunay(){};

private:
    //ros
    ros::NodeHandle nh_;
    ros::NodeHandle private_nh_;

    ros::Subscriber laserScanSub;
    void laserScanCallback(const sensor_msgs::LaserScanConstPtr& laserScan);

    ros::Subscriber odometrySub;
    void odometryCallback(const nav_msgs::OdometryConstPtr& odometry);

    ros::Subscriber lineSegmentSub;
    void lineSegmentCallback(const featureDetection::LineSegmentListConstPtr& lineSegmentList);

    ros::Publisher nodeTrianglePub;
    ros::Publisher nodePolygonPub;
    ros::Publisher nodeCenterPub;
    ros::Publisher nodePub;

    //opencv
    int imgScale, imgHeight, imgWidth;
    float laserOffset, robotWidthMin, robotWidthMax, mergePolygonD, featureXD, featureYD;
    float nearV;//find linesegment point near polygon point
    float nearTheta;
    float near90Theta;
    float odomX, odomY, odomT;


};




#endif //TOPONODEDELAUNAY_TOPONODEDELAUNAY_H
