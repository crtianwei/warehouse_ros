//
// Created by crti on 12/12/17.
//

#include "topoNodeDelaunay.h"


TopoNodeDelaunay::TopoNodeDelaunay() :
        private_nh_("~")
{
    imgScale = 100;
    imgWidth = 12*imgScale;
    imgHeight = 22*imgScale;
    imgWidth = 600;
    imgHeight = 600;
    laserOffset = 0.2;
    robotWidthMin = 1;
    robotWidthMax = 8;
    mergePolygonD = 0.2;
    featureXD = 6;
    featureYD = 4;
    nearV = 0.4;
    nearTheta = 70;
    near90Theta = 120;//min QAQ

    //laserScanSub = nh_.subscribe("scan", 1, &TopoNodeDelaunay::laserScanCallback, this);
    odometrySub = nh_.subscribe("odom", 1, &TopoNodeDelaunay::odometryCallback, this);
    lineSegmentSub = nh_.subscribe("lineSegments", 1, &TopoNodeDelaunay::lineSegmentCallback, this);

    nodeTrianglePub = nh_.advertise<visualization_msgs::Marker>("nodeTriangle", 0);
    nodePolygonPub = nh_.advertise<visualization_msgs::Marker>("nodePolygon", 0);
    nodeCenterPub = nh_.advertise<visualization_msgs::Marker>("nodeCenter", 0);
    nodePub = nh_.advertise<topoNodeDelaunay::FeaturePolygon>("currentNode",0);
    //localPosePub = nh_.advertise<featureDetection::Lane>("localPose",0);



}


void TopoNodeDelaunay::laserScanCallback(const sensor_msgs::LaserScanConstPtr& laserScan)
{
    auto startTime = chrono::steady_clock::now();
    //cout << "," << double(chrono::duration_cast<chrono::microseconds>(chrono::steady_clock::now() - startTime).count()) * chrono::microseconds::period::num / chrono::microseconds::period::den;

    Rect rect(-200, -200, 400, 400);
    Subdiv2D subdiv(rect);
    Point2f p;
    float theta;

    float laserIncrement = laserScan->angle_increment;
    float laserStartAngle = laserScan->angle_min;
    float laserMaxRange = laserScan->range_max;
    int laserSize =laserScan->ranges.size();
    int ccccnt=0;
    for (int i = 0; i < laserSize; ++i) {
        //ccccnt++;

        if(laserScan->ranges[i]<laserMaxRange){
            theta = (i * laserIncrement + laserStartAngle);// - 3.5/ 180 * M_PI);// / 180 * M_PI;
            p.x = sin(theta) * laserScan->ranges[i] + laserOffset;
            p.y = cos(theta) * laserScan->ranges[i];
            p.y = -p.y;
            //point from robot to img
            //p.x = imgWidth/2 + p.x*imgScale;
            //p.y = imgHeight/2 + p.y*imgScale;
            if(rect.contains(p)){
                subdiv.insert(p);
                //cout<< p.x << "," << p.y << endl;
            }
            else{
                //cout<< p.x << "," << p.y << endl;
            }
        }
    }
    //subdiv.deletePoint(-3);


    //get delaunay triangles
    vector<Vec6f> triangles;
    subdiv.getTriangleList(triangles);

    //find feature triangles
    vector<Vec6f> featureTriangles;
    for(int i=0; i<triangles.size(); ++i){
        Vec6f t = triangles[i];
        float l0 = (t[0]-t[2])*(t[0]-t[2]) + (t[1]-t[3])*(t[1]-t[3]);
        float l1 = (t[2]-t[4])*(t[2]-t[4]) + (t[3]-t[5])*(t[3]-t[5]);
        float l2 = (t[4]-t[0])*(t[4]-t[0]) + (t[5]-t[1])*(t[5]-t[1]);
        float lmax = max(l0,max(l1,l2));
        float lmin = min(l0,min(l1,l2));
        float lmid = l0+l1+l2-lmax-lmin;
        float dmax = robotWidthMax*robotWidthMax;
        float dmin = robotWidthMin*robotWidthMin;

        if(abs(t[0])+abs(t[1])<50 && abs(t[2])+abs(t[3])<50 && abs(t[4])+abs(t[5])<50 //delete fake point
           && l0 > dmin && l1 > dmin && l2 > dmin
           && l0 < dmax && l1 < dmax && l2 < dmax
           //&& lmax < 6*lmin
           && (lmin+lmid-lmax)/(2*sqrt(lmin*lmid)) > cos(100.0/180.0*M_PI) //max angle > 100deg
           && abs(t[1])<featureXD && abs(t[3])<featureXD && abs(t[5])<featureXD
                ){
            featureTriangles.push_back(t);
            //cout<<"("<<t[0]<<","<<t[1]<<") ("<<t[2]<<","<<t[3]<<") ("<<t[4]<<","<<t[5]<<")"<<endl;
            //ccccnt++;
        }
    }
    //cout<<featureTriangles.size()<<"***********************************************************************************"<<endl;
    //find feature polygon or triangle
    vector<Vec3f> featurePolygon;//(x,y,degree)
    if(featureTriangles.size()==1){//one triangle
        Vec3f p;
        p[2] = 0;
        p[0] = featureTriangles[0][0];
        p[1] = featureTriangles[0][1];
        featurePolygon.push_back(p);
        p[0] = featureTriangles[0][2];
        p[1] = featureTriangles[0][3];
        featurePolygon.push_back(p);
        p[0] = featureTriangles[0][4];
        p[1] = featureTriangles[0][5];
        featurePolygon.push_back(p);
    }
    else if(featureTriangles.size()>1){//multi triangles
        Vec3f p;
        p[2] = 1;
        //find main triangle by distance to origin point
        float minDis = 100;
        int minIndex = -1;
        for(int i=0;i<featureTriangles.size();++i){
            Vec6f t = featureTriangles[i];
            float dis = abs(t[0]+t[2]+t[4])*2 + abs(t[1]+t[3]+t[5]);
            if(dis<minDis){
                minDis = dis;
                minIndex = i;
            }
        }

        //find triangle connect to main triangle or polygon
        vector<int> mask;
        mask.reserve(featureTriangles.size());
        for(int i=0;i<mask.size();++i){mask[i]=0;}
        mask[minIndex] = 2;

        p[0] = featureTriangles[minIndex][0];
        p[1] = featureTriangles[minIndex][1];
        featurePolygon.push_back(p);
        p[0] = featureTriangles[minIndex][2];
        p[1] = featureTriangles[minIndex][3];
        featurePolygon.push_back(p);
        p[0] = featureTriangles[minIndex][4];
        p[1] = featureTriangles[minIndex][5];
        featurePolygon.push_back(p);

        bool inserted = true;
        while(inserted){
            inserted = false;
            int it0[3],it1=-10,it2=-20;
            Vec3f insertPoint;
            for(int i=0; i<featureTriangles.size(); ++i){
                //if(mask[i]>0) { continue; }

                int cnt=0;//2,3,4 | 5,6,7 | 9
                Vec6f t = featureTriangles[i];
                for(int j=0; j<3; ++j){
                    //if(mask[i]>0) { break; }
                    for(int k=0; k<featurePolygon.size(); ++k){
                        if(//featurePolygon[k][2]>0
                           1 && (featurePolygon[k][0]-t[2*j])*(featurePolygon[k][0]-t[2*j]) + (featurePolygon[k][1]-t[1+2*j])*(featurePolygon[k][1]-t[1+2*j]) < mergePolygonD*mergePolygonD){
                            cnt += j+2;
                            it0[j] = k;
                            break;
                        }
                    }
                }
                //cout<<cnt<<endl;

                if(cnt==9){//same
                    mask[i] = 1;
                }
                else if(cnt==5){//insert
                    it1 = it0[0];
                    it2 = it0[1];
                    insertPoint[0] = t[4];
                    insertPoint[1] = t[5];
                    mask[i] = 2;
                }
                else if(cnt==6){//insert
                    it1 = it0[0];
                    it2 = it0[2];
                    insertPoint[0] = t[2];
                    insertPoint[1] = t[3];
                    mask[i] = 2;
                }
                else if(cnt==7){//insert
                    it1 = it0[1];
                    it2 = it0[2];
                    insertPoint[0] = t[0];
                    insertPoint[1] = t[1];
                    mask[i] = 2;
                }

                if(it1==0 && it2==(featurePolygon.size()-1)){
                    featurePolygon.push_back(insertPoint);
                    inserted = true;
                }
                else if(it2==0 && it1==(featurePolygon.size()-1)){
                    featurePolygon.push_back(insertPoint);
                    inserted = true;
                }
                else if(abs(it1-it2)==1){
                    featurePolygon.insert(featurePolygon.begin() + max(it1,it2),insertPoint);
                    inserted = true;
                }
            }
        }
    }

    //cout << endl << double(chrono::duration_cast<chrono::microseconds>(chrono::steady_clock::now() - startTime).count()) * chrono::microseconds::period::num / chrono::microseconds::period::den;
    cout<< triangles.size() << endl;


    int pointNumOfPolygon=0;
    float centerX=0, centerY=0;

    //send feature polygon or triangle to rivz, for display
    visualization_msgs::Marker FP;//delaunay triangles
    FP.header.frame_id = "base_link";
    FP.header.stamp = ros::Time::now();
    FP.ns = "nodePolygonNameSpace";
    FP.id = 0;
    FP.type = visualization_msgs::Marker::LINE_STRIP;
    FP.action = visualization_msgs::Marker::MODIFY;
    FP.scale.x = 0.1;
    FP.color.b = 1.0;
    FP.color.a = 1.0;
    geometry_msgs::Point pp;
    pp.z = 0;
    if(featurePolygon.size()>0) {
        for (int i = 0; i < featurePolygon.size (); ++i) {
            pp.x = featurePolygon[i][0];
            pp.y = featurePolygon[i][1];
            centerX += pp.x;
            centerY += pp.y;
            pointNumOfPolygon++;
            FP.points.push_back (pp);
        }
        pp.x = featurePolygon[0][0];
        pp.y = featurePolygon[0][1];
        FP.points.push_back (pp);
    }
    nodePolygonPub.publish (FP);



    //send triangles to rivz, for display
    vector<Vec4f> edgeList;
    subdiv.getEdgeList(edgeList);
    //Point2f fakePoint[4];
    //for(int i=0;i<4;++i){
    //    fakePoint[i] = subdiv.getVertex(-i);
    //}

    visualization_msgs::Marker DT;//delaunay triangles
    DT.header.frame_id = "base_link";
    DT.header.stamp = ros::Time::now();
    DT.ns = "nodeTiangleNameSpace";
    DT.id = 0;
    DT.type = visualization_msgs::Marker::LINE_LIST;
    DT.action = visualization_msgs::Marker::MODIFY;
    DT.scale.x = 0.005;
    DT.color.g = 1.0;
    DT.color.a = 1.0;
    geometry_msgs::Point p1,p2,p3;
    p1.z = 0;
    p2.z = 0;
    p3.z = 0;
    for(int i=0; i<edgeList.size(); ++i){
        p1.x = edgeList[i][0];
        p1.y = edgeList[i][1];
        p2.x = edgeList[i][2];
        p2.y = edgeList[i][3];
        bool isFake = false;
        if(abs(p1.x)+abs(p1.y) > 50) {isFake = true;}
        if(abs(p2.x)+abs(p2.y) > 50) {isFake = true;}

        //for(int j=0; j<4; ++j){
        //    if(abs(fakePoint[j].x) + abs(fakePoint[j].y) > 50){
        //        isFake = true;
        //        break;
        //    }
        //    if(abs(p1.x-fakePoint[j].x)<robotWidth && abs(p1.y-fakePoint[j].y)<robotWidth){
        //        isFake = true;
        //        break;
        //    }
        //    if(abs(p2.x-fakePoint[j].x)<robotWidth && abs(p2.y-fakePoint[j].y)<robotWidth){
        //        isFake = true;
        //        break;
        //    }
        //}
        if(isFake) continue;
        DT.points.push_back(p1);
        DT.points.push_back(p2);
    }
    /*for(int i=0; i<triangles.size(); ++i){
        Vec6f t = triangles[i];
        //p1.x = (t[0] - imgWidth/2)/imgScale;
        //p1.y = -(imgHeight/2 - t[1])/imgScale;
        //p2.x = (t[2] - imgWidth/2)/imgScale;
        //p2.y = -(imgHeight/2 - t[3])/imgScale;
        //p3.x = (t[4] - imgWidth/2)/imgScale;
        //p3.y = -(imgHeight/2 - t[4])/imgScale;

        p1.x = t[0];
        p1.y = t[1];
        p2.x = t[2];
        p2.y = t[3];
        p3.x = t[4];
        p3.y = t[5];

        bool isFake = false;
        if(abs(p1.x)+abs(p1.y) > 50) {isFake = true;}
        if(abs(p2.x)+abs(p2.y) > 50) {isFake = true;}
        if(abs(p3.x)+abs(p3.y) > 50) {isFake = true;}
        if(isFake) continue;

        DT.points.push_back(p1);DT.points.push_back(p2);
        DT.points.push_back(p2);DT.points.push_back(p3);
        DT.points.push_back(p3);DT.points.push_back(p1);
    }*/
    nodeTrianglePub.publish(DT);
}

void TopoNodeDelaunay::odometryCallback(const nav_msgs::OdometryConstPtr& odometry)
{
    odomX = odometry->pose.pose.position.x;
    odomY = odometry->pose.pose.position.y;
    odomT = tf::getYaw(odometry->pose.pose.orientation);

}

void TopoNodeDelaunay::lineSegmentCallback(const featureDetection::LineSegmentListConstPtr& lineSegmentList)
{
    auto startTime = chrono::steady_clock::now();
    //cout << "," << double(chrono::duration_cast<chrono::microseconds>(chrono::steady_clock::now() - startTime).count()) * chrono::microseconds::period::num / chrono::microseconds::period::den;


    Rect rect(-200, -200, 400, 400);
    Subdiv2D subdiv(rect);
    Point2f p;


    int lineSegmentSize = lineSegmentList->lineSegments.size();
    for(int i=0; i<lineSegmentSize; ++i){
        featureDetection::LineSegment ls = lineSegmentList->lineSegments[i];
        float dx = ls.point1_x-ls.point2_x;
        float dy = ls.point1_y-ls.point2_y;
        float length = sqrt(dx*dx+dy*dy);

        int div = (int)(length*2/robotWidthMin) + 1;
        float divX = dx/div;
        float divY = dy/div;
        for(int j=0; j<=div; ++j){
            p.x = ls.point2_x + j*divX;
            p.y = ls.point2_y + j*divY;
            if(rect.contains(p)){
                subdiv.insert(p);
                //cout<< p.x << "," << p.y << endl;
            }
        }
    }


    //get delaunay triangles
    vector<Vec6f> triangles;
    subdiv.getTriangleList(triangles);

    //find feature triangles
    vector<Vec6f> featureTriangles;
    for(int i=0; i<triangles.size(); ++i){
        Vec6f t = triangles[i];
        float l0 = (t[0]-t[2])*(t[0]-t[2]) + (t[1]-t[3])*(t[1]-t[3]);
        float l1 = (t[2]-t[4])*(t[2]-t[4]) + (t[3]-t[5])*(t[3]-t[5]);
        float l2 = (t[4]-t[0])*(t[4]-t[0]) + (t[5]-t[1])*(t[5]-t[1]);
        float lmax = max(l0,max(l1,l2));
        float lmin = min(l0,min(l1,l2));
        float lmid = l0+l1+l2-lmax-lmin;
        float dmax = robotWidthMax*robotWidthMax;
        float dmin = robotWidthMin*robotWidthMin;

        if(abs(t[0])+abs(t[1])<50 && abs(t[2])+abs(t[3])<50 && abs(t[4])+abs(t[5])<50 //delete fake point
           && l0 > dmin && l1 > dmin && l2 > dmin
           && l0 < dmax && l1 < dmax && l2 < dmax
           //&& lmax < 6*lmin
           && (lmin+lmid-lmax)/(2*sqrt(lmin*lmid)) > cos(120.0/180.0*M_PI) //max angle > 100deg
           //&& acos((lmax+lmid-lmin)/(2*sqrt(lmax*lmid))) > 30 //min angle > 100deg
           && ((abs(t[0])<featureXD && abs(t[2])<featureXD) || (abs(t[2])<featureXD && abs(t[4])<featureXD) || (abs(t[4])<featureXD && abs(t[0])<featureXD))
           && ((abs(t[1])<featureYD && abs(t[3])<featureYD) || (abs(t[3])<featureYD && abs(t[5])<featureYD) || (abs(t[5])<featureYD && abs(t[1])<featureYD))
                ){
            bool isOut = false;
            for(int k=0; k<3; ++k){
                float cx = (t[0]+t[2]+t[4]-t[k*2])/2;
                float cy = (t[1]+t[3]+t[5]-t[k*2+1])/2;
                float A1 = -cy;
                float B1 = cx;
                float C1 = 0;
                for(int j=0; j<lineSegmentSize; ++j){
                    featureDetection::LineSegment ls = lineSegmentList->lineSegments[j];
                    float A2 = ls.A;
                    float B2 = ls.B;
                    float C2 = ls.C;
                    float jx = (B1*C2-B2*C1)/(A1*B2-A2*B1);
                    float jy = (A1*C2-A2*C1)/(A2*B1-A1*B2);
                    float f0 = 0;
                    if(      jx >= max(min(cx,f0),min(ls.point1_x,ls.point2_x))
                          && jx <= min(max(cx,f0),max(ls.point1_x,ls.point2_x))
                          && jy >= max(min(cy,f0),         min(ls.point1_y,ls.point2_y))
                          && jy <= min(max(cy,f0),         max(ls.point1_y,ls.point2_y))){
                        isOut = true;
                    }
                }
            }

            if(!isOut){
                featureTriangles.push_back(t);
            }

            //cout<<"("<<t[0]<<","<<t[1]<<") ("<<t[2]<<","<<t[3]<<") ("<<t[4]<<","<<t[5]<<")"<<endl;
            //ccccnt++;
        }
    }
    //cout<<featureTriangles.size()<<"***********************************************************************************"<<endl;
    //find feature polygon or triangle
    vector<Vec3f> featurePolygon;//(x,y,degree)
    if(featureTriangles.size()==1){//one triangle
        Vec3f p;
        p[2] = 0;
        p[0] = featureTriangles[0][0];
        p[1] = featureTriangles[0][1];
        featurePolygon.push_back(p);
        p[0] = featureTriangles[0][2];
        p[1] = featureTriangles[0][3];
        featurePolygon.push_back(p);
        p[0] = featureTriangles[0][4];
        p[1] = featureTriangles[0][5];
        featurePolygon.push_back(p);
    }
    else if(featureTriangles.size()>1){//multi triangles
        Vec3f p;
        p[2] = 1;
        //find main triangle by distance to origin point
        float minDis = 100;
        int minIndex = -1;
        for(int i=0;i<featureTriangles.size();++i){
            Vec6f t = featureTriangles[i];
            float dis = abs(t[0]+t[2]+t[4]) + abs(t[1]+t[3]+t[5])*2;
            if(dis<minDis){
                minDis = dis;
                minIndex = i;
            }
        }

        //find triangle connect to main triangle or polygon
        vector<int> mask;
        mask.reserve(featureTriangles.size());
        for(int i=0;i<mask.size();++i){mask[i]=0;}
        mask[minIndex] = 2;

        p[0] = featureTriangles[minIndex][0];
        p[1] = featureTriangles[minIndex][1];
        featurePolygon.push_back(p);
        p[0] = featureTriangles[minIndex][2];
        p[1] = featureTriangles[minIndex][3];
        featurePolygon.push_back(p);
        p[0] = featureTriangles[minIndex][4];
        p[1] = featureTriangles[minIndex][5];
        featurePolygon.push_back(p);

        bool inserted = true;
        while(inserted){
            inserted = false;
            int it0[3],it1=-10,it2=-20;
            Vec3f insertPoint;
            for(int i=0; i<featureTriangles.size(); ++i){
                //if(mask[i]>0) { continue; }

                int cnt=0;//2,3,4 | 5,6,7 | 9
                Vec6f t = featureTriangles[i];
                for(int j=0; j<3; ++j){
                    //if(mask[i]>0) { break; }
                    for(int k=0; k<featurePolygon.size(); ++k){
                        if(//featurePolygon[k][2]>0
                                1 && (featurePolygon[k][0]-t[2*j])*(featurePolygon[k][0]-t[2*j]) + (featurePolygon[k][1]-t[1+2*j])*(featurePolygon[k][1]-t[1+2*j]) < mergePolygonD*mergePolygonD){
                            cnt += j+2;
                            it0[j] = k;
                            break;
                        }
                    }
                }
                //cout<<cnt<<endl;

                if(cnt==9){//same
                    mask[i] = 1;
                }
                else if(cnt==5){//insert
                    it1 = it0[0];
                    it2 = it0[1];
                    insertPoint[0] = t[4];
                    insertPoint[1] = t[5];
                    mask[i] = 2;
                }
                else if(cnt==6){//insert
                    it1 = it0[0];
                    it2 = it0[2];
                    insertPoint[0] = t[2];
                    insertPoint[1] = t[3];
                    mask[i] = 2;
                }
                else if(cnt==7){//insert
                    it1 = it0[1];
                    it2 = it0[2];
                    insertPoint[0] = t[0];
                    insertPoint[1] = t[1];
                    mask[i] = 2;
                }

                if(it1==0 && it2==(featurePolygon.size()-1)){
                    featurePolygon.push_back(insertPoint);
                    inserted = true;
                }
                else if(it2==0 && it1==(featurePolygon.size()-1)){
                    featurePolygon.push_back(insertPoint);
                    inserted = true;
                }
                else if(abs(it1-it2)==1){
                    featurePolygon.insert(featurePolygon.begin() + max(it1,it2),insertPoint);
                    inserted = true;
                }
            }
        }
    }
    vector<Vec3f> featurePolygonTmp;
    for(int i=0; i<featurePolygon.size(); ++i){
        if(i==0){
            featurePolygonTmp.push_back(featurePolygon[i]);
        }
        else{
            if(abs(featurePolygon[i][0]-featurePolygon[i-1][0])<0.001
               && abs(featurePolygon[i][1]-featurePolygon[i-1][1])<0.001){

            }
            else{
                featurePolygonTmp.push_back(featurePolygon[i]);
            }
        }
    }
    featurePolygon.swap(featurePolygonTmp);

    //cout << endl << double(chrono::duration_cast<chrono::microseconds>(chrono::steady_clock::now() - startTime).count()) * chrono::microseconds::period::num / chrono::microseconds::period::den;
    //cout<< triangles.size() << endl;




    //node position
    int pointNumOfPolygon=featurePolygon.size();
    float centerX=0, centerY=0;// center pose in laser frame
    /*for(int i=0;i<pointNumOfPolygon;++i){
        centerX += featurePolygon[i][0];
        centerY += featurePolygon[i][1];
    }
    centerX /= featurePolygon.size();
    centerY /= featurePolygon.size();
    float ox,oy,ot;// odom
    ox = odomX;
    oy = odomY;
    ot = odomT;
    float sinT = sin(odomT);
    float cosT = cos(odomT);
    float mx,my;//center pose in map frame
    mx = ox + centerX*cosT - centerY*sinT;
    my = oy + centerX*sinT + centerY*cosT;
    //cout<<mx<<","<<my<<endl;*/

    //find the line segments which endpoint near each polygon point
    vector<vector<int>> lsofp;//line segments of polygon points
    for(int i=0; i<pointNumOfPolygon; ++i){
        vector<int> lsp;
        float pvx = featurePolygon[i][0];
        float pvy = featurePolygon[i][1];
        for(int j=0; j<lineSegmentSize; ++j){
            featureDetection::LineSegment ls = lineSegmentList->lineSegments[j];
            //cout<<"XC: "<<abs(ls.A*pvy + ls.B*pvx + ls.C)<<endl;

            float currentNearV;
            if((-ls.B*pvx+ls.A*pvy+ls.B*ls.point1_x-ls.A*ls.point1_y)*(-ls.B*pvx+ls.A*pvy+ls.B*ls.point2_x-ls.A*ls.point2_y)<0){
                currentNearV = ls.A*pvx + ls.B*pvy + ls.C;
                //cout<<"!"<<currentNearV<<endl;
            }
            else{
                currentNearV = min(sqrt((pvx-ls.point1_x)*(pvx-ls.point1_x)+(pvy-ls.point1_y)*(pvy-ls.point1_y)),
                                   sqrt((pvx-ls.point2_x)*(pvx-ls.point2_x)+(pvy-ls.point2_y)*(pvy-ls.point2_y)));
                //cout<<"@"<<currentNearV<<endl;
            }
            if(abs(currentNearV)<nearV){
               lsp.push_back(j);
            }
        }
        //cout<<"FDF: "<<lsp.size()<<endl;
        lsofp.push_back(lsp);
    }

    //find the line segments to polygon edge
    vector<vector<int>> lsofe;
    for(int i=0; i<pointNumOfPolygon; ++i){
        int i1=i;
        int i2= (i==(pointNumOfPolygon-1)) ? 0 : (i+1);
        float p1x = featurePolygon[i1][0];
        float p1y = featurePolygon[i1][1];
        float p2x = featurePolygon[i2][0];
        float p2y = featurePolygon[i2][1];

        vector<int> lse;
        float maxcos_t = near90Theta;
        int maxLSindex = -1;
        if(lsofp[i1].size()){
            for(int j=0; j<lsofp[i1].size(); ++j){
                float ls1x = lineSegmentList->lineSegments[lsofp[i1][j]].point1_x;
                float ls1y = lineSegmentList->lineSegments[lsofp[i1][j]].point1_y;
                float ls2x = lineSegmentList->lineSegments[lsofp[i1][j]].point2_x;
                float ls2y = lineSegmentList->lineSegments[lsofp[i1][j]].point2_y;
                float d1x, d1y, d2x, d2y;//xiangliang
                d1x = p2x - p1x;
                d1y = p2y - p1y;
                float cos_t;
                if((p1x-ls1x)*(p1x-ls1x)+(p1y-ls1y)*(p1y-ls1y)< nearV*nearV){
                    d2x = ls2x - ls1x;
                    d2y = ls2y - ls1y;
                    cos_t = (d1x*d2x + d1y*d2y) / (sqrt(d1x*d1x + d1y*d1y)*sqrt(d2x*d2x + d2y*d2y));
                    //cout<<"!1";
                }
                else if((p1x-ls2x)*(p1x-ls2x)+(p1y-ls2y)*(p1y-ls2y)< nearV*nearV){
                    d2x = ls1x - ls2x;
                    d2y = ls1y - ls2y;
                    cos_t = (d1x*d2x + d1y*d2y) / (sqrt(d1x*d1x + d1y*d1y)*sqrt(d2x*d2x + d2y*d2y));
                    //cout<<"!2";
                }
                else{
                    d2x = ls2x - ls1x;
                    d2y = ls2y - ls1y;
                    cos_t = abs(d1x*d2x + d1y*d2y) / (sqrt(d1x*d1x + d1y*d1y)*sqrt(d2x*d2x + d2y*d2y));
                    //cout<<"!3";
                }
                cos_t = acos(cos_t)/M_PI*180;
                //cout<<"!"<<cos_t<<endl;
                if(cos_t<maxcos_t){
                    maxcos_t = cos_t;
                    maxLSindex = lsofp[i1][j];
                }
            }
            if(maxLSindex>=0){
                lse.push_back(maxLSindex);
            }
        }
        maxcos_t = near90Theta;
        maxLSindex = -1;
        if(lsofp[i2].size()){
            for(int j=0; j<lsofp[i2].size(); ++j){
                float ls1x = lineSegmentList->lineSegments[lsofp[i2][j]].point1_x;
                float ls1y = lineSegmentList->lineSegments[lsofp[i2][j]].point1_y;
                float ls2x = lineSegmentList->lineSegments[lsofp[i2][j]].point2_x;
                float ls2y = lineSegmentList->lineSegments[lsofp[i2][j]].point2_y;
                float d1x, d1y, d2x, d2y;//xiangliang
                d1x = p1x - p2x;
                d1y = p1y - p2y;
                float cos_t;
                if((p2x-ls1x)*(p2x-ls1x)+(p2y-ls1y)*(p2y-ls1y)< nearV*nearV){
                    d2x = ls2x - ls1x;
                    d2y = ls2y - ls1y;
                    cos_t = (d1x*d2x + d1y*d2y) / (sqrt(d1x*d1x + d1y*d1y)*sqrt(d2x*d2x + d2y*d2y));
                    //cout<<"@1";
                }
                else if((p2x-ls2x)*(p2x-ls2x)+(p2y-ls2y)*(p2y-ls2y)< nearV*nearV){
                    d2x = ls1x - ls2x;
                    d2y = ls1y - ls2y;
                    cos_t = (d1x*d2x + d1y*d2y) / (sqrt(d1x*d1x + d1y*d1y)*sqrt(d2x*d2x + d2y*d2y));
                    //cout<<"@2";
                }
                else{
                    d2x = ls2x - ls1x;
                    d2y = ls2y - ls1y;
                    cos_t = abs(d1x*d2x + d1y*d2y) / (sqrt(d1x*d1x + d1y*d1y)*sqrt(d2x*d2x + d2y*d2y));
                    //cout<<"@3";
                }

                cos_t = acos(cos_t)/M_PI*180;
                //cout<<"@"<<cos_t<<endl;
                if(cos_t<maxcos_t){
                    maxcos_t = cos_t;
                    maxLSindex = lsofp[i2][j];
                }
            }
            if(maxLSindex>=0){
                lse.push_back(maxLSindex);
            }
        }
        //cout<<"QQ: "<<lse.size()<<endl;
        lsofe.push_back(lse);
    }

    //cal node type and find mid lines
    vector<Vec3f> midLine;//A,B,C
    int fakeCnt=0;
    for(int i=0; i<lsofe.size(); ++i){
        float A1=0, B1=0, A2=0, B2=0;
        if(lsofe[i].size()>0){
            A1 = lineSegmentList->lineSegments[lsofe[i][0]].A;
            B1 = lineSegmentList->lineSegments[lsofe[i][0]].B;
        }
        if(lsofe[i].size()>1){
            A2 = lineSegmentList->lineSegments[lsofe[i][1]].A;
            B2 = lineSegmentList->lineSegments[lsofe[i][1]].B;
        }
        float cos_A = acos(abs(A1*A2 + B1*B2))/M_PI*180;
        //cout<<"EW: "<<cos_A<<endl;

        float midX,midY;
        if(i==(featurePolygon.size()-1)){
            midX = (featurePolygon[i][0]+featurePolygon[0][0])/2;
            midY = (featurePolygon[i][1]+featurePolygon[0][1])/2;
        }
        else{
            midX = (featurePolygon[i][0]+featurePolygon[i+1][0])/2;
            midY = (featurePolygon[i][1]+featurePolygon[i+1][1])/2;
        }

        if(lsofe[i].size()==2){
            if(cos_A > nearTheta){
                fakeCnt++;
            }
            else{
                Vec3f tmp;
                if(A1*A2<0 || B1*B2<0){
                    A1 = -A1;
                    B1 = -B1;
                }
                tmp[0] = (A1+A2)/2;
                tmp[1] = (B1+B2)/2;
                tmp[2] = -(tmp[0]*midX + tmp[1]*midY);
                midLine.push_back(tmp);
                //cout<<"!"<<A1<<" "<<A2<<" "<<B1<<" "<<B2<<"#";
            }
        }
        else if(lsofe[i].size()==1){
            Vec3f tmp;
            tmp[0] = A1;
            tmp[1] = B1;
            tmp[2] = -(tmp[0]*midX + tmp[1]*midY);
            midLine.push_back(tmp);
            //cout<<"@"<<A1<<" "<<B1<<"#";
        }
    }
    int nodeType = featurePolygon.size() - fakeCnt;


    //cout<<"AA:"<<featurePolygon.size()<<","<<fakeCnt<<endl;
    //for(int i=0;i<featurePolygon.size();++i){
    //    cout<<featurePolygon[i][0]<<","<<featurePolygon[i][1]<<endl;
    //}

    //cal center
    vector<Vec2f> midCrossP;
    centerX = 0;
    centerY = 0;
    if(midLine.size()<2){
        for(int i=0; i<featurePolygon.size(); ++i){
            Vec2f tmp;
            tmp[0] = featurePolygon[i][0];
            tmp[1] = featurePolygon[i][1];
            midCrossP.push_back(tmp);
            //cout<<"!";//<<tmp[0]<<","<<tmp[1]<<endl;
        }
    }
    else{
        for(int i=0; i<midLine.size(); ++i){
            Vec2f tmp;
            int j = (i==(midLine.size()-1)) ? 0 : (i+1);
            float A1,B1,C1,A2,B2,C2;
            A1 = midLine[i][0];
            B1 = midLine[i][1];
            C1 = midLine[i][2];
            A2 = midLine[j][0];
            B2 = midLine[j][1];
            C2 = midLine[j][2];
            float sin_ = A1*B2 - A2*B1;
            if(abs(sin_)>sin(30*M_PI/180)){
                tmp[0] = (B1*C2-B2*C1)/sin_;
                tmp[1] = (A1*C2-A2*C1)/(-sin_);
                midCrossP.push_back(tmp);
                //cout<<"@";//<<tmp[0]<<","<<tmp[1]<<","<<A1<<","<<B1<<endl;
            }
            //cout<<"&&"<<A1<<" "<<B1<<" "<<C1<<" "<<A2<<" "<<B2<<" "<<C2<<"&&"<<endl;
        }
    }

    for(int i=0; i<midCrossP.size(); ++i){
        centerX += midCrossP[i][0];
        centerY += midCrossP[i][1];
    }
    centerX /= midCrossP.size();
    centerY /= midCrossP.size();
    //cout<<"CENTER: "<<centerX<<","<<centerY<<endl;


    //cal center in map
    float ox,oy,ot;// odom
    ox = odomX;
    oy = odomY;
    ot = odomT;
    float sinT = sin(odomT);
    float cosT = cos(odomT);
    float mx,my;//center pose in map frame
    mx = ox + centerX*cosT - centerY*sinT;
    my = oy + centerX*sinT + centerY*cosT;
    //cout<<ox<<" "<<oy<<" "<<" "<<mx<<" "<<my<<" "<<centerX<<" "<<centerY<<endl;

    topoNodeDelaunay::FeaturePolygon FPN;
    FPN.edgeNum = featurePolygon.size();
    FPN.goodEdgeNum = nodeType;
    FPN.robotx = centerX;
    FPN.roboty = centerY;
    FPN.mapx = mx;
    FPN.mapy = my;
    FPN.odomx = ox;
    FPN.odomy = oy;
    FPN.odomt = ot;
    for(int i=0; i<featurePolygon.size(); ++i){
        FPN.pointx.push_back(featurePolygon[i][0]);
        FPN.pointy.push_back(featurePolygon[i][1]);

        int j = (i==(featurePolygon.size()-1)) ? 0 : (i+1);//next
        int k = (i==0) ? (featurePolygon.size()-1) : (i-1);//before
        float xi = featurePolygon[i][0];
        float yi = featurePolygon[i][1];
        float xk = featurePolygon[k][0];
        float yk = featurePolygon[k][1];
        float xj = featurePolygon[j][0];
        float yj = featurePolygon[j][1];
        float xik = xk-xi;
        float yik = yk-yi;
        float xij = xj-xi;
        float yij = yj-yi;

        float distance = sqrt((xi-xk)*(xi-xk) + (yi-yk)*(yi-yk));
        float angle = acos((xik*xij+yik*yij)/(sqrt(xik*xik+yik*yik)*sqrt(xij*xij+yij*yij))) /M_PI *180;
        //cout<<distance<<" "<<angle<<" ";
        FPN.feature.push_back(distance);
        FPN.feature.push_back(angle);
    }
    //cout<<endl;
    if(//featurePolygon.size() &&
            abs(mx)<1000 && abs(my)<1000){
        nodePub.publish(FPN);
    }






    //send feature polygon or triangle to rivz, for display
    visualization_msgs::Marker FP;//delaunay triangles
    FP.header.frame_id = "base_link";
    FP.header.stamp = lineSegmentList->header.stamp;
    FP.ns = "nodePolygonNameSpace";
    FP.id = 0;
    FP.type = visualization_msgs::Marker::LINE_STRIP;
    FP.action = visualization_msgs::Marker::MODIFY;
    FP.scale.x = 0.1;
    FP.color.b = 1.0;
    FP.color.a = 1.0;
    geometry_msgs::Point pp;
    pp.z = 0;
    if(featurePolygon.size()>0) {
        for (int i = 0; i < featurePolygon.size (); ++i) {
            pp.x = featurePolygon[i][0];
            pp.y = featurePolygon[i][1];
            FP.points.push_back (pp);
        }
        pp.x = featurePolygon[0][0];
        pp.y = featurePolygon[0][1];
        FP.points.push_back (pp);
    }
    nodePolygonPub.publish (FP);



    //send triangles to rivz, for display
    vector<Vec4f> edgeList;
    subdiv.getEdgeList(edgeList);
    //Point2f fakePoint[4];
    //for(int i=0;i<4;++i){
    //    fakePoint[i] = subdiv.getVertex(-i);
    //}

    visualization_msgs::Marker DT;//delaunay triangles
    DT.header.frame_id = "base_link";
    DT.header.stamp = lineSegmentList->header.stamp;
    DT.ns = "nodeTiangleNameSpace";
    DT.id = 0;
    DT.type = visualization_msgs::Marker::LINE_LIST;
    DT.action = visualization_msgs::Marker::MODIFY;
    DT.scale.x = 0.01;
    DT.color.g = 1.0;
    DT.color.a = 1.0;
    geometry_msgs::Point p1,p2,p3;
    p1.z = 0;
    p2.z = 0;
    p3.z = 0;
    for(int i=0; i<edgeList.size(); ++i){
        p1.x = edgeList[i][0];
        p1.y = edgeList[i][1];
        p2.x = edgeList[i][2];
        p2.y = edgeList[i][3];
        bool isFake = false;
        if(abs(p1.x)+abs(p1.y) > 50) {isFake = true;}
        if(abs(p2.x)+abs(p2.y) > 50) {isFake = true;}

        //for(int j=0; j<4; ++j){
        //    if(abs(fakePoint[j].x) + abs(fakePoint[j].y) > 50){
        //        isFake = true;
        //        break;
        //    }
        //    if(abs(p1.x-fakePoint[j].x)<robotWidth && abs(p1.y-fakePoint[j].y)<robotWidth){
        //        isFake = true;
        //        break;
        //    }
        //    if(abs(p2.x-fakePoint[j].x)<robotWidth && abs(p2.y-fakePoint[j].y)<robotWidth){
        //        isFake = true;
        //        break;
        //    }
        //}
        if(isFake) continue;
        DT.points.push_back(p1);
        DT.points.push_back(p2);
    }
    /*for(int i=0; i<triangles.size(); ++i){
        Vec6f t = triangles[i];
        //p1.x = (t[0] - imgWidth/2)/imgScale;
        //p1.y = -(imgHeight/2 - t[1])/imgScale;
        //p2.x = (t[2] - imgWidth/2)/imgScale;
        //p2.y = -(imgHeight/2 - t[3])/imgScale;
        //p3.x = (t[4] - imgWidth/2)/imgScale;
        //p3.y = -(imgHeight/2 - t[4])/imgScale;

        p1.x = t[0];
        p1.y = t[1];
        p2.x = t[2];
        p2.y = t[3];
        p3.x = t[4];
        p3.y = t[5];

        bool isFake = false;
        if(abs(p1.x)+abs(p1.y) > 50) {isFake = true;}
        if(abs(p2.x)+abs(p2.y) > 50) {isFake = true;}
        if(abs(p3.x)+abs(p3.y) > 50) {isFake = true;}
        if(isFake) continue;

        DT.points.push_back(p1);DT.points.push_back(p2);
        DT.points.push_back(p2);DT.points.push_back(p3);
        DT.points.push_back(p3);DT.points.push_back(p1);
    }*/
    nodeTrianglePub.publish(DT);

    //send center to rivz, for display
    visualization_msgs::Marker NC;//center
    NC.header.frame_id = "odom";
    NC.header.stamp = lineSegmentList->header.stamp;
    NC.ns = "nodeCenterNameSpace";
    NC.id = 0;
    NC.type = visualization_msgs::Marker::POINTS;
    NC.action = visualization_msgs::Marker::MODIFY;
    NC.scale.x = 0.1;
    NC.scale.y = NC.scale.x;
    NC.color.r = 1.0;
    NC.color.a = 1.0;
    geometry_msgs::Point pC;
    pC.z = 0;
    //pC.x = oy + (centerX-laserOffset)*sinT + (centerY+laserOffset)*cosT;
    //pC.y = ox + (centerX-laserOffset)*cosT - (centerY+laserOffset)*sinT;
    pC.x = mx;
    pC.y = my;
    NC.points.push_back(pC);
    nodeCenterPub.publish(NC);

    //cout << endl << double(chrono::duration_cast<chrono::microseconds>(chrono::steady_clock::now() - startTime).count()) * chrono::microseconds::period::num / chrono::microseconds::period::den;


}
