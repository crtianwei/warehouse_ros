#ifndef DATA_TYPE_H_
#define DATA_TYPE_H_

#define ROBOT_NO 12

struct Translation{
	double position[3];
	void CopyFrom(double pos[3])
	{
		position[0] = pos[0];
		position[1] = pos[1];
		position[2] = pos[2];
	}
	Translation()
	{
		position[0] = 0.0;
		position[1] = 0.0;
		position[2] = 0.0;
	}
};

struct RotationHelical{
	double helical[3];
	void CopyFrom(double hel[3])
	{
		helical[0] = hel[0];
		helical[1] = hel[1];
		helical[2] = hel[2];
	}
	RotationHelical()
	{
		helical[0] = 0.0;
		helical[1] = 0.0;
		helical[2] = 0.0;
	}
};

struct RotationMatrix{
	double matrix[9];
	void CopyFrom(double mat[9])
	{
		for (int i = 0; i < 9; i++)
		{
			matrix[i] = mat[i];
		}
	}
	RotationMatrix()
	{
		for (int i = 0; i < 9; i++)
		{
			matrix[i] = 0.0;
		}
	}
};

struct RotationQuaternion{
	double Quaternion[4];
	void CopyFrom(double qua[4])
	{
		for (int i = 0; i < 4; i++)
		{
			Quaternion[i] = qua[i];
		}
	}

	RotationQuaternion()
	{
		Quaternion[0] = 0.0;
		Quaternion[1] = 0.0;
		Quaternion[2] = 0.0;
		Quaternion[3] = 0.0;
	}
};

struct RotationEuler{
	double EulerAngel[3];
	void CopyFrom(double eul[3])
	{
		EulerAngel[0] = eul[0];
		EulerAngel[1] = eul[1];
		EulerAngel[2] = eul[2];
	}
	RotationEuler()
	{
		EulerAngel[0] = 0.0;
		EulerAngel[1] = 0.0;
		EulerAngel[2] = 0.0;
	}
};


struct RobotData{
	Translation translation;
	RotationHelical rotationHelical;
	RotationMatrix  rotationMatrix;
	RotationQuaternion rotationQuaternion;
	RotationEuler rotationEuler;
	bool Valid = false;
	RobotData() : Valid(false){}

};

struct DataPacket{
	RobotData robotData[ROBOT_NO];
};

#endif