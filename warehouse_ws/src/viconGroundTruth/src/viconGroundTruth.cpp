//
// Created by crti on 17-12-31.
//

#include "viconGroundTruth.h"


ViconGroundTruth::ViconGroundTruth() :
        private_nh_("~")
{
    viconGroundTruthPub = nh_.advertise<nav_msgs::Odometry>("viconGroundTruth",0);



}

void ViconGroundTruth::run()
{
    sock = socket(AF_INET, SOCK_STREAM, 0);
    memset(&servAddr, 0, sizeof(servAddr));
    servAddr.sin_family = AF_INET;
    servAddr.sin_port = htons(6000);
    servAddr.sin_addr.s_addr = inet_addr("192.168.10.1");

    if(connect(sock, (struct sockaddr *)&servAddr, sizeof(servAddr)) <0){
        cout<<"failed connect to vicon"<<endl;
        return;
    }
    cout<<"connect to vicon"<<endl;

    DataPacket recvData;

    while(recv(sock, (char *)&recvData, sizeof(DataPacket), 0)) {
        double x = recvData.robotData[0].translation.position[0]/1000.0;
        double y = recvData.robotData[0].translation.position[1]/1000.0;
        double t = recvData.robotData[0].rotationEuler.EulerAngel[2]*180/M_PI;
        cout<<x<<" "<<y<<" "<<t<<endl;
    }

    close(sock);
}