//
// Created by crti on 17-12-31.
//

#include "ros/ros.h"
#include "viconGroundTruth.h"


int main(int argc, char **argv)
{
    ros::init(argc, argv, "viconGroundTruth");

    ViconGroundTruth vgt;
    while(true){
        vgt.run();
    }
    ros::spin();

    return(0);
}
