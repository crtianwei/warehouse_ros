//
// Created by crti on 17-12-31.
//

#ifndef VICONGROUNDTRUTH_VICONGROUNDTRUTH_H
#define VICONGROUNDTRUTH_VICONGROUNDTRUTH_H

#include "ros/ros.h"
#include "nav_msgs/Odometry.h"
#include "DataType.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

using namespace std;

class ViconGroundTruth
{
public:
    ViconGroundTruth();
    ~ViconGroundTruth(){};
    void run();

private:
    //ros
    ros::NodeHandle nh_;
    ros::NodeHandle private_nh_;

    //ros::Subscriber

    ros::Publisher viconGroundTruthPub;

    //socket
    int sock;
    sockaddr_in servAddr;

};


#endif //VICONGROUNDTRUTH_VICONGROUNDTRUTH_H
