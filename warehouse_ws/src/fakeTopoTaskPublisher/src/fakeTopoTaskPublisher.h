//
// Created by crti on 7/5/17.
//

#ifndef FAKETOPOTASKPUBLISHER_FAKETOPOTASKPUBLISHER_H
#define FAKETOPOTASKPUBLISHER_FAKETOPOTASKPUBLISHER_H


#include <iostream>
#include <vector>
#include <map>
#include <fstream>
#include <algorithm>
#include "ros/ros.h"
#include "visualization_msgs/Marker.h"
#include "std_msgs/builtin_int32.h"
#include <tf/tf.h>
#include "../msg/TopoTask.h"
#include "../msg/TopoRespond.h"
using namespace std;

//struct TOPO_RFID{
//    int seq;
//    double x;
//    double y;
//    vector<int> RFIDs;
//};
struct TOPO_PATH{
    int beginPointSeq;
    int endPointSeq;
    double laneLength;
    double laneWidth;
};

class fakeTopoTaskPublisher {
public:
    fakeTopoTaskPublisher();
    ~fakeTopoTaskPublisher(){};

    string PATHLogFileName;

    //edges, paths
    //  point1 seq  point2seq,length,width
    map<int, vector<TOPO_PATH>> pathMap;

    //task about
    int currentRFID;
    int currentPointSeq;

    vector<int> currentTask;
    int currentGoalIndex;//index in currentTask


private:
    //ros
    ros::NodeHandle nh_;
    ros::NodeHandle private_nh_;

    //Subscriber & Publisher
    ros::Subscriber topoTaskRespondSub;
    void topoTaskRespondCallback(const topoMapServer::TopoRespondConstPtr& RFIDRecv);

    ros::Publisher topoTaskPub;

    void loadPATHsFromFile(string RFIDLogFileName);

    void printPathMap();//print pathMap



};


#endif //FAKETOPOTASKPUBLISHER_FAKETOPOTASKPUBLISHER_H
