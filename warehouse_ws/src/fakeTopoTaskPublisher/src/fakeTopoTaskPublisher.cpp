//
// Created by crti on 7/5/17.
//

#include "fakeTopoTaskPublisher.h"

fakeTopoTaskPublisher::fakeTopoTaskPublisher() :
        private_nh_("~")
{
    PATHLogFileName = "PATH.txt";
    loadPATHsFromFile(PATHLogFileName);

    topoTaskRespondSub = nh_.subscribe("topoRespond", 3, &fakeTopoTaskPublisher::topoTaskRespondCallback, this);

    topoTaskPub = nh_.advertise<topoMapServer::TopoTask>("topoTask",3);
}

void fakeTopoTaskPublisher::loadPATHsFromFile(string _PATHLogFileName)
{
    fstream PATHLogFile;
    PATHLogFile.open(_PATHLogFileName, ios::in);
    if(!PATHLogFile.good()) {
        cout << "topoMapServer: read PATH log file (" << _PATHLogFileName << " ) failed." <<endl;
        return;
    }

    string line;
    while(getline(PATHLogFile, line)) {
        TOPO_PATH addPATH;
        istringstream lineStream(line);
        string PATHInfo;
        int i = 0;
        while(getline(lineStream, PATHInfo, ',')) {
            switch(i) {
                case 0:
                    //beginPointSeq
                    addPATH.beginPointSeq = stoi(PATHInfo);
                    cout<<"beginPointSeq:"<<addPATH.beginPointSeq<<" ";
                    break;
                case 1:
                    //endPointSeq
                    addPATH.endPointSeq = stoi(PATHInfo);
                    cout<<"endPointSeq:"<<addPATH.endPointSeq<<" ";
                    break;
                case 2:
                    //laneLength
                    addPATH.laneLength = stod(PATHInfo);
                    cout<<"laneLength:"<<addPATH.laneLength<<" ";
                    break;
                case 3:
                    //laneWidth
                    addPATH.laneWidth = stod(PATHInfo);
                    cout<<"laneWidth:"<<addPATH.laneWidth<<" ";
                default:
                    cout<< "topoMapServer: read PATH info failed. Line: " << line << endl;
                    break;
            }
            ++i;
        }
        cout<<endl;
        //todo: add txt input exception handling

        //add to pathMap
        pathMap[addPATH.beginPointSeq].push_back(addPATH);
    }
}

void fakeTopoTaskPublisher::printPathMap()
{
    cout<<"topoMapServer: print pathMap"<<endl;
    for(auto &pointTmp:pathMap){
        cout<<"seq:"<<pointTmp.first;
        for(auto &pathTmp:pointTmp.second){
            cout<<" point->point:"<<pathTmp.beginPointSeq<<"->"<<pathTmp.endPointSeq;
        }
        cout<<endl;
    }
    cout<<"topoMapServer: print pathMap finshed"<<endl;
}