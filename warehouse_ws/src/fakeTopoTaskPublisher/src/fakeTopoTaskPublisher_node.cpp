//
// Created by crti on 7/5/17.
//

#include "ros/ros.h"
#include "fakeTopoTaskPublisher.h"

int main(int argc, char** argv) {
    ros::init(argc, argv, "fakeTopoTaskPublisher");

    fakeTopoTaskPublisher fttp;

    ros::spin();
    return 0;
}