//
// Created by crti on 17-12-31.
//

#include "ros/ros.h"
#include "localization.h"

int main(int argc, char **argv)
{
    ros::init(argc, argv, "localization");

    Localization L;

    ros::spin();

    return(0);
}