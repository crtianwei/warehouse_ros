//
// Created by crti on 17-12-31.
//

#ifndef LOCALIZATION_LOCALIZATION_H
#define LOCALIZATION_LOCALIZATION_H

#include "ros/ros.h"

#include "visualization_msgs/Marker.h"
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Twist.h"
#include "../msg/FeaturePolygon.h"
#include "../msg/Lane.h"

#include <math.h>
#include <chrono>
#include <iostream>
#include <fstream>

#include <tf/tf.h>

#include "g2o/core/sparse_optimizer.h"
#include "g2o/core/block_solver.h"
#include "g2o/core/factory.h"
#include "g2o/core/optimization_algorithm_factory.h"
#include "g2o/core/optimization_algorithm_gauss_newton.h"
#include "g2o/core/optimization_algorithm_levenberg.h"

#include "g2o/solvers/csparse/linear_solver_csparse.h"

#include "g2o/types/slam2d/vertex_se2.h"
#include "g2o/types/slam2d/vertex_point_xy.h"
#include "g2o/types/slam2d/edge_se2.h"
#include "g2o/types/slam2d/edge_se2_pointxy.h"

G2O_USE_TYPE_GROUP(slam2d);
using namespace std;
using namespace g2o;

struct MAPEDGE{
    int no1;//firts node index
    int no2;//second node index, must no1<no2
    float A;
    float B;
    float C;
    friend bool operator <(MAPEDGE const& a, MAPEDGE const& b){
        return (a.no1==b.no1) ? (a.no2<b.no2) : (a.no1<b.no1);
    }
};

class Localization {
public:
    Localization();
    ~Localization(){};

    topoNodeDelaunay::FeaturePolygon lastNode;
    bool isFirstNode;
    float lx, ly, lt;//localization resualt pose
    float last_lx, last_ly, last_lt;//last pose
private:
    //ros
    ros::NodeHandle nh_;
    ros::NodeHandle private_nh_;

    ros::Subscriber nodeSub;
    void nodeCallback(const topoNodeDelaunay::FeaturePolygonConstPtr& node);

    ros::Subscriber odometrySub;
    void odometryCallback(const nav_msgs::OdometryConstPtr& odometry);

    ros::Subscriber laneSub;
    void laneCallback(const featureDetection::LaneConstPtr& lane);

    ros::Publisher topoAllNodeShowPub;
    ros::Publisher topoAllEdgeShowPub;

    ros::Publisher locPosePub;
    ros::Publisher cmdVelPub;

    vector<topoNodeDelaunay::FeaturePolygon> mapNodes;
    set<MAPEDGE> mapEdges;

    int loadMap();
    void drawMap();

    void g2oUpdate(float& lx, float& ly, float& lt,
                   float last_lx, float last_ly, float last_lt,
                   float landmarkx, float landmarky,
                   float rx, float ry,
                   float last_rx, float last_ry);
};


#endif //LOCALIZATION_LOCALIZATION_H
