//
// Created by crti on 17-12-31.
//

#include "localization.h"


Localization::Localization() :
        private_nh_("~")
{

    nodeSub = nh_.subscribe("currentNode", 1, &Localization::nodeCallback, this);
    //odometrySub = nh_.subscribe("odom", 1, &Localization::odometryCallback, this);
    laneSub = nh_.subscribe("localPose", 1, &Localization::laneCallback, this);

    locPosePub = nh_.advertise<nav_msgs::Odometry>("localizationPose",0);
    cmdVelPub = nh_.advertise<geometry_msgs::Twist>("cmd_vel",0);
    topoAllNodeShowPub = nh_.advertise<visualization_msgs::Marker>("topoAllNode", 0);
    topoAllEdgeShowPub = nh_.advertise<visualization_msgs::Marker>("topoAllEdge", 0);

    if(loadMap()==3){
        drawMap();
    }

    isFirstNode = true;
    lx = 0;
    ly = 0;
    lt = 0;
}

void Localization::nodeCallback(const topoNodeDelaunay::FeaturePolygonConstPtr& node)
{
    /*//zugoujin cai jiance
    if(node->edgeNum<=0 || node->goodEdgeNum<=0){
        return;
    }
    float minDis = node->robotx*node->robotx+node->roboty*node->roboty;
    for(int i=0; i<node->pointx.size(); ++i){
        float tmpDis = node->pointx[i]*node->pointx[i] + node->pointy[i]*node->pointy[i];
        if(tmpDis<minDis){
            minDis = tmpDis;
        }
    }
    if(minDis > startFindDis*startFindDis){
        return;
    }*/

    topoNodeDelaunay::FeaturePolygon N;
    N.edgeNum = node->edgeNum;
    N.goodEdgeNum = node->goodEdgeNum;
    N.robotx = node->robotx;//x in robot coor
    N.roboty = node->roboty;
    N.mapx = node->mapx;//x in map coor
    N.mapy = node->mapy;
    N.odomx = node->odomx;
    N.odomy = node->odomy;
    N.odomt = node->odomt;
    for(int i=0; i<node->feature.size(); ++i){
        N.feature.push_back(node->feature[i]);
    }

    if(isFirstNode){
        isFirstNode = false;
        lastNode.edgeNum = N.edgeNum;
        lastNode.goodEdgeNum = N.goodEdgeNum;
        lastNode.robotx = N.robotx;//x in robot coor
        lastNode.roboty = N.roboty;
        lastNode.mapx = N.mapx;//x in map coor
        lastNode.mapy = N.mapy;
        lastNode.odomx = N.odomx;
        lastNode.odomy = N.odomy;
        lastNode.odomt = N.odomt;
        for(int i=0; i<N.feature.size(); ++i){
            lastNode.feature.push_back(N.feature[i]);
        }
        last_lx = lx;
        last_ly = ly;
        last_lt = lt;
    }

    //update pose with odom
    float x1 = lastNode.odomx;
    float y1 = lastNode.odomy;
    float t1 = lastNode.odomt;
    float x2 = N.odomx;
    float y2 = N.odomy;
    float t2 = N.odomt;
    float ct1 = cos(t1);
    float st1 = sin(t1);
    float dx = ct1*(x2-x1) - st1*(y2-y1);
    float dy = st1*(x2-x1) + ct1*(y2-y1);
    float ct = cos(lt);
    float st = sin(lt);
    lx = ct*dx - st*dy + lx;
    ly = st*dx + ct*dy + ly;
    lt = lt + t2 -t1;

    if(N.edgeNum>0 && N.goodEdgeNum>0){//node polygon exist
        //g2o
        g2oUpdate(lx, ly, lt, last_lx, last_ly, last_lt,
                  0.0, 0.0, //landmark x,y
                  N.robotx, N.roboty, lastNode.robotx, lastNode.roboty);
    }


    lastNode.edgeNum = N.edgeNum;
    lastNode.goodEdgeNum = N.goodEdgeNum;
    lastNode.robotx = N.robotx;//x in robot coor
    lastNode.roboty = N.roboty;
    lastNode.mapx = N.mapx;//x in map coor
    lastNode.mapy = N.mapy;
    lastNode.odomx = N.odomx;
    lastNode.odomy = N.odomy;
    lastNode.odomt = N.odomt;
    for(int i=0; i<N.feature.size(); ++i){
        lastNode.feature.push_back(N.feature[i]);
    }
    last_lx = lx;
    last_ly = ly;
    last_lt = lt;
}

void Localization::odometryCallback(const nav_msgs::OdometryConstPtr& odometry)
{
    //drawMap();
}

void Localization::laneCallback(const featureDetection::LaneConstPtr& lane)
{
    float ed = abs(lane->rightDistance) - 0.8;
    float et = lane->rightTheta;
    float K = 10, Ked = 10;
    float w = (et + ed*Ked) * K;
    //w = -et*0.5;
    w = ed;
    cout<<ed<<endl;
    float v = 0.8;
    v=0;
    geometry_msgs::Twist cmdVel;
    cmdVel.linear.x = v;
    cmdVel.linear.y = 0;
    cmdVel.linear.z = 0;
    cmdVel.angular.x = 0;
    cmdVel.angular.y = 0;
    cmdVel.angular.z = w;
    cmdVelPub.publish(cmdVel);
}

void Localization::g2oUpdate(float& lx, float& ly, float& lt,
               float last_lx, float last_ly, float last_lt,
               float landmarkx, float landmarky,
               float rx, float ry,
               float last_rx, float last_ry)
{

}

int Localization::loadMap()
{
    fstream mapFile;
    mapFile.open("map.txt", ios::in);
    if(!mapFile.good()){
        cout<<"load map failed"<<endl;
        return -1;
    }

    int loadState = 0;//0,start; 1,node; 2,edge; 3,finish
    string line;
    while(getline(mapFile, line) && (loadState<3)){
        istringstream lineStream(line);
        string tmp;
        int i=0;

        if(line=="NODE"){
            loadState = 1;
            cout<<"load NODE"<<endl;
        }
        else if(line=="EDGE"){
            loadState = 2;
            cout<<"load EDGE"<<endl;
        }
        else if(line=="FIN"){
            loadState = 3;
            cout<<"load finish"<<endl;
        }
        else{
            if(loadState==0){
                cout<<"load head.."<<endl;
            }
            else if(loadState==1){//node
                topoNodeDelaunay::FeaturePolygon addNode;
                while(getline(lineStream, tmp, ',')){
                    switch(i){
                        case 0:
                            //id
                            if(i!=stoi(tmp)){
                                //cout<<"map file error (node id)"<<i<<" "<<(int)(tmp[0])-'0'<<endl;
                            //return -2;
                            }
                            cout<<"seq:"<<stoi(tmp)<<" ";
                            break;
                        case 1:
                            //mapx
                            addNode.mapx = stof(tmp);
                            cout<<"x:"<<addNode.mapx<<" ";
                            break;
                        case 2:
                            //mapy
                            addNode.mapy = stof(tmp);
                            cout<<"y:"<<addNode.mapy<<" ";
                            break;
                        case 3:
                            //edgeNum
                            //cout<<(int)tmp[0]<<" ";
                            addNode.edgeNum = (tmp[0]);
                            cout<<"edgeNum:"<<(int)addNode.edgeNum<<" ";
                            break;
                        case 4:
                            //goodEdgeNum
                            addNode.goodEdgeNum = (tmp[0]);
                            cout<<"goodEdgeNum"<<(int)addNode.goodEdgeNum<<" ";
                            break;
                        default:
                            //feature
                            addNode.feature.push_back(stof(tmp));
                            cout<<"F["<<addNode.feature.size()<<"]:"<<addNode.feature.back()<<" ";
                            break;
                    }
                    ++i;
                }
                cout<<endl;
                mapNodes.push_back(addNode);
            }
            else if(loadState==2){//edge
                MAPEDGE addEdge;
                while(getline(lineStream, tmp, ',')){
                    switch(i){
                        case 0:
                            //no1
                            addEdge.no1 = stoi(tmp);
                            cout<<"no1:"<<addEdge.no1<<" ";
                            break;
                        case 1:
                            //no2
                            addEdge.no2 = stoi(tmp);
                            cout<<"no2:"<<addEdge.no2<<" ";
                            break;
                        default:
                            cout<<"map file error (edge)"<<endl;
                            return -2;
                    }
                    ++i;
                }
                cout<<endl;

                float x1 = mapNodes[addEdge.no1].mapx;
                float y1 = mapNodes[addEdge.no1].mapy;
                float y2 = mapNodes[addEdge.no2].mapy;
                float x2 = mapNodes[addEdge.no2].mapx;
                float A = y2-y1;
                float B = x1-x2;
                float C = x2*y1-x1*y2;
                float norm = sqrt(A*A+B*B);
                if(C<0) norm = -norm;
                if(abs(norm)>1e-10){
                    A = A/norm;
                    B = B/norm;
                    C = C/norm;
                }
                else{
                    A = 0;
                    B = 0;
                    C = 0;
                }
                addEdge.A = A;
                addEdge.B = B;
                addEdge.C = C;

                mapEdges.insert(addEdge);
            }
        }
    }

    return loadState;
}

void Localization::drawMap()
{
    visualization_msgs::Marker topoNode;
    topoNode.header.frame_id = "odom";
    topoNode.header.stamp = ros::Time::now();
    topoNode.ns = "topoNode";
    topoNode.id = 0;
    topoNode.type = visualization_msgs::Marker::POINTS;
    topoNode.action = visualization_msgs::Marker::MODIFY;
    topoNode.scale.x = 0.5;
    topoNode.scale.y = 0.5;
    topoNode.color.b = 1.0;
    topoNode.color.a = 1.0;
    geometry_msgs::Point p;
    p.z = 0;
    topoNode.points.reserve(mapNodes.size());
    for(int i=0; i<mapNodes.size(); ++i){
        p.x = mapNodes[i].mapx;
        p.y = mapNodes[i].mapy;
        topoNode.points.push_back(p);
        //cout<<p.x<<" "<<p.y<<endl;
    }
    topoAllNodeShowPub.publish(topoNode);


    visualization_msgs::Marker topoEdge;
    topoEdge.header.frame_id = "odom";
    topoEdge.header.stamp = ros::Time::now();
    topoEdge.ns = "topoEdge";
    topoEdge.id = 0;
    topoEdge.type = visualization_msgs::Marker::LINE_LIST;
    topoEdge.action = visualization_msgs::Marker::MODIFY;
    topoEdge.scale.x = 0.05;
    topoEdge.color.b = 1.0;
    topoEdge.color.a = 1.0;
    topoEdge.points.reserve(mapEdges.size()*2);
    //int i=0;
    for(auto &edge:mapEdges){
        p.x = mapNodes[edge.no1].mapx;
        p.y = mapNodes[edge.no1].mapy;
        topoEdge.points.push_back(p);
        p.x = mapNodes[edge.no2].mapx;
        p.y = mapNodes[edge.no2].mapy;
        topoEdge.points.push_back(p);

    }
    topoAllEdgeShowPub.publish(topoEdge);
    cout<<"drawMap"<<endl;
}
