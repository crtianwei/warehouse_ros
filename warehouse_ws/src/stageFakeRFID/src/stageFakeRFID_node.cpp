//
// Created by crti on 5/11/17.
//

#include "ros/ros.h"
#include "stageFakeRFID.h"

int main(int argc, char** argv) {
    ros::init(argc, argv, "stageFakeRFID");

    stageFakeRFID sfRFID;

//    for(int i=0; i < sfRFID.RFIDs.size(); ++i) {
//        cout<< sfRFID.RFIDs[i].ID <<" "<< sfRFID.RFIDs[i].x <<" "<< sfRFID.RFIDs[i].y <<endl;
//    }

    ros::spin();
    return 0;
}
