//
// Created by crti on 5/11/17.
//
//
//todo: 1 seq <-> 1 RFID now, need to change 1 seq <-> n RFID
#include "stageFakeRFID.h"

stageFakeRFID::stageFakeRFID() :
        private_nh_("~")
{
    RFIDRadius = 0.1;
    RFIDLogFileName = "RFID.txt";

    loadRFIDsFromFile(RFIDLogFileName);

    odometrySub = nh_.subscribe("odom", 1, &stageFakeRFID::odometryCallback, this);

    stageFakeRFIDPub = nh_.advertise<std_msgs::Int32>("RFIDInfo", 0);//x~x, y~y, z~id

}

stageFakeRFID::~stageFakeRFID() {}

void stageFakeRFID::odometryCallback(const nav_msgs::OdometryConstPtr &odometry)
{
    vector<int> nearbyRFID;
    double x = odometry->pose.pose.position.x;
    double y = odometry->pose.pose.position.y;

    for(auto &vMap:pointMap){
        //todo: 1 seq <-> 1 RFID now, need to change 1 seq <-> n RFID
        //current struct: n RFID in 1 seq, have same x&y
        for(auto &vRFID:vMap.second.RFIDs){
            double xError = abs(x - vMap.second.x);
            double yError = abs(y - (14-vMap.second.y));
            double dError = sqrt(xError*xError + yError*yError);
            if(xError<RFIDRadius && yError<RFIDRadius && dError<RFIDRadius){
                nearbyRFID.push_back(vRFID);
            }
        }
    }

    std_msgs::Int32 pubRFID;
    switch(nearbyRFID.size()) {
        case 0:
            cout << "-1 " <<x<<","<<y<< endl;
            pubRFID.data = -1;
            stageFakeRFIDPub.publish(pubRFID);
            break;
        case 1:
            cout << nearbyRFID[0] << endl;
            //publish
            pubRFID.data = nearbyRFID[0];
            stageFakeRFIDPub.publish(pubRFID);
            break;
        default:
            cout<< "multi RIFDs: ";
            for(auto id : nearbyRFID) {
                cout << " " << id;
            }
            int index = rand() % nearbyRFID.size();
            cout<<" choose:"<<nearbyRFID[index];
            pubRFID.data = nearbyRFID[index];
            stageFakeRFIDPub.publish(pubRFID);
            cout << endl;
            break;
    }

}

void stageFakeRFID::loadRFIDsFromFile(string _RFIDLogFileName)
{
    fstream RFIDLogFile;
    RFIDLogFile.open(_RFIDLogFileName, ios::in);
    if(!RFIDLogFile.good()) {
        cout << "topoMapServer: read RFID log file (" << _RFIDLogFileName << " ) failed." <<endl;
        return;
    }

    string line;
    while(getline(RFIDLogFile, line)) {
        TOPO_RFID addRFID;
        istringstream lineStream(line);
        string RFIDInfo;
        int i = 0;
        while(getline(lineStream, RFIDInfo, ',')) {
            switch(i) {
                case 0:
                    //ID
                    addRFID.seq = stoi(RFIDInfo);
                    cout<<"seq:"<<addRFID.seq<<" ";
                    break;
                case 1:
                    //x
                    addRFID.x = stod(RFIDInfo);
                    cout<<"X:"<<addRFID.x<<" ";
                    break;
                case 2:
                    //y
                    addRFID.y = stod(RFIDInfo);
                    cout<<"Y:"<<addRFID.y<<" ";
                    break;
                default:
                    //cout<< "topoMapServer: read RFID info failed. Line: " << line << endl;
                    addRFID.RFIDs.push_back(stoi(RFIDInfo));
                    cout<<"RIFD["<<addRFID.RFIDs.size()<<"]:"<<addRFID.RFIDs.back()<<" ";
                    break;
            }
            ++i;
        }
        cout<<endl;
        //todo: add txt input exception handling

        //add to pointMap
        pointMap[addRFID.seq] = addRFID;
    }
}

