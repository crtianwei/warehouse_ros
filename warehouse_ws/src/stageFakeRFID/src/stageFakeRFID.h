//
// Created by crti on 5/11/17.
//

#ifndef STAGEFAKERFID_STAGEFAKERFID_H
#define STAGEFAKERFID_STAGEFAKERFID_H

#include <iostream>
#include <vector>
#include <map>
#include <fstream>
#include <algorithm>
#include "ros/ros.h"
#include "nav_msgs/Odometry.h"
#include "std_msgs/builtin_int32.h"
#include <tf/tf.h>
using namespace std;

struct TOPO_RFID{
    int seq;
    double x;
    double y;
    vector<int> RFIDs;
};

class stageFakeRFID {
public:
    stageFakeRFID();
    ~stageFakeRFID();

    double RFIDRadius;
    string RFIDLogFileName;

    //RFID points
    //  seq  x,y,RFID[]
    map<int, TOPO_RFID> pointMap;

private:
    //ros
    ros::NodeHandle nh_;
    ros::NodeHandle private_nh_;

    //Subscriber & Publisher
    ros::Subscriber odometrySub;
    void odometryCallback(const nav_msgs::OdometryConstPtr& odometry);

    ros::Publisher stageFakeRFIDPub;

    void loadRFIDsFromFile(string RFIDLogFileName);

};


#endif //STAGEFAKERFID_STAGEFAKERFID_H
