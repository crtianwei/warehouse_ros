/**************************************************************************
* Description: lineExtraction with raw laser data
* Author     : crtianwei
* Create Date: 2017-02-20
**************************************************************************/

/*need to check Memory Leak*/

#ifndef _LINEEXTRACTION_H_
#define _LINEEXTRACTION_H_

#include <iostream>
#include <vector>
#include <stack>
#include <algorithm>
#define _USE_MATH_DEFINES
#include <math.h>
#include <mutex>
#include <chrono>
#include "laserStructs.h"
#include "laserExtractionStructs.h"


using namespace std;

/**
 * @class LineExtraction
 * @brief line segment extraction with raw laser data.
 */
class LineExtraction
{
private:


	vector<Point> laserData;
    float laserStartAngle;
    float laserIncrement;
    float laserMaxRange;

    float clusterDistanceThreshold;
    float spliteDistanceThreshold;

	//line: Ax+By+C=0, xcosa+ysina+r=0. (a,r)
    float mergeCThreshold;//dr
    float mergeABThreshold;//sin(da)
    float mergeDistanceThreshold;//4 endpoints distance

	int leastPointNumInLine;
	float leastLineLength;

public:
	vector<LineSegment> mergeResLineList, clusterResLineList, spliteResLineList;

private:
    mutex lineSegmentMutex;

public:
	Lane lane;
private:
    mutex laneMutex;

    float lastLeftDistance;
    float lastRightDistance;
    float lastFrontDistance;
    float leftDistanceThresholdMax;
    float rightDistanceThresholdMax;
    float frontDistanceThresholdMax;
    float leftDistanceThresholdMin;
    float rightDistanceThresholdMin;
    float frontDistanceThresholdMin;
    float leftDistanceThresholdStep;
    float rightDistanceThresholdStep;
    float frontDistanceThresholdStep;
    float leftDistanceThreshold;
    float rightDistanceThreshold;
    float frontDistanceThreshold;

public:
    float laserOffset;//distance between laser center and robot center, >0 laser is front
	LineExtraction();
	~LineExtraction();

	/**
	 * @brief main update function
	 * @param[in] rawLaserRange raw laser range data
	 * @param[out] resLineList vector stores LineSegments
	 * @output laserData
	 */
	void update(LaserParam& laserParam, const vector<float> &rawLaserRange);

	/**
	 * @brief getLineSegments
	 * @param[out] resLineList vector stores LineSegments
	 * @input vector<LineSegment> mergeResLineList;
	 */
	void getLineSegments(vector<LineSegment> &lineSegments);

	/**
	 * @brief getLane
	 * @return lane
	 */
	Lane getLane();

private:
	/**
	 * @brief cluster points by range difference, and calculate (x,y) to laserData
	 * @param[in] rawLaserRange raw laser range data
	 * @param[out] resLineList vector stores cluters in LineSegments
	 * @output laserData
	 */
	void clusterPoint(vector<float> rawLaserRange, vector<LineSegment> &resLineList);

	/**
	 * @brief splite one LineSegment to several lineSegments, recursive algorithm atom step
	 * @param[in] srcLine input lineSegment
	 * @param[out] resLineList splited lineSegments vector
	 * @input laserData
	 */
	void spliteLine(LineSegment &srcLine, vector<LineSegment> &resLineList);

	/**
	 * @brief splite one LineSegment to several lineSegments, use stack instead of recursive algorithm
	 * @param[in] srcLine input lineSegment
	 * @param[out] resLineList splited lineSegments vector
	 * @input laserData
	 */
	void spliteLineStack(LineSegment &srcLine, vector<LineSegment> &resLineList);

	/**
	* @brief splite each cluster to several lineSegments, part of split-and-merge(SAM) algorithm
	* @param[in] srcLineList clutered vector
	* @param[out] resLineList splited lineSegments vector
	* @input laserData
	*/
	void spliteLineList(vector<LineSegment> srcLineList, vector<LineSegment> &resLineList);

	/**
	 * @brief merge the splited lineSegments which are on the same line
	 * @param[in] srcLineList splited lineSegments
	 * @param[out] resLineList merged lineSegments
	 * @input laserData
	 */
	void mergeLineList(vector<LineSegment> &srcLineList, vector<LineSegment> &resLineList);

	/**
	 * @brief find lane
	 * @param[in] srcLineList merged lineSegments
	 * @param[out] resault lane
	 * //@input laserData
	 */
	void calLane(vector<LineSegment> srcLineList, Lane &resLane);

	/**
	 * @brief calculate the distance between a Point and a Line
	 * @param[in] point
	 * @param[in] line
	 * @return distance (not always >= 0, sign+- means the point on the which side of the line)
	 */
    float pointToLineDistance(Point point, Line line);

	/**
	 * @brief calculate the distance between 2 Points
	 * @param[in] point1
	 * @param[in] point2
	 * @return distance (always >= 0)
	 */
    float pointToPointDistance(Point point1, Point point2);

	/**
	 * @brief calculate the (abs) distance between 2 Lines(which has endpoints)
	 * @param[in] line1
	 * @param[in] endPoint11 endpoint of line1
	 * @param[in] endPoint12 endpoint of line1
	 * @param[in] line2
	 * @param[in] endPoint21 endpoint of line2
	 * @param[in] endPoint22 endpoint of line2
	 * @return distance (always >= 0)
	 */
    float lineToLineDistance(Line line1, Point endPoint11, Point endPoint12, Line line2, Point endPoint21, Point endPoint22);

	/**
	 * @brief calculate a Line passing through the passPoint and vertical to the srcLine
	 * @param[in] srcLine
	 * @param[in] passPoint
	 * @return Line
	 * @note if the srcLine is normalized(A^2+B^2=1), the resLine is normalized.
	 */
	Line verticalLine(Line srcLine, Point passPoint);

	/**
	 * @brief calculate intersection point of the srcLine and the Line which passing through the passPoint and vertical to the srcLine
	 * @param[in] srcLine
	 * @param[in] passPoint
	 * @return vertical point
	 * @note the srcLine should be normalized(A^2+B^2=1).
	 */
	Point verticalPoint(Line srcLine, Point passPoint);

	/**
	 * @brief fit a line by least square algorithm
	 * @param[in] srcLine lineSegment to fit
	 * @param[in] startIndex start point index, defualt -1 means 0
	 * @param[in] endIndex end point index, defualt -1 means N (max)
	 * @return fitted lineSegment
	 * @input laserData
	 */
	Line leastSquareLineFit(LineSegment srcLine, int startIndex = -1, int endIndex = -1);

	/**
	 * @brief fit a line by 2 line endpoints
	 * @param[in] srcLine lineSegment to fit
	 * @param[in] startIndex start point index, defualt -1 means 0
	 * @param[in] endIndex end point index, defualt -1 means N (max)
	 * @return fitted lineSegment
	 * @input laserData
	 */
	Line endPointLineFit(LineSegment srcLine, int startIndex = -1, int endIndex = -1);

	/**
	* @brief normalize line parameter, make A^2+B^2=1, C>0.
	* @param[in] srcLine Line to normalize
	* @return normalized Line
	*/
	Line normalizeLine(Line srcLine);
};

#endif