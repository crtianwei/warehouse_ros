/**************************************************************************
* Description: lineExtraction with raw laser data
* Author     : crtianwei
* Create Date: 2017-02-20
**************************************************************************/

#include "lineExtraction.h"

LineExtraction::LineExtraction()
{
    laserOffset = 0.2;

	clusterDistanceThreshold = 1;
	spliteDistanceThreshold = 0.3;

//	mergeCThreshold = 0.1;
//	mergeABThreshold = sin(5 * M_PI / 180);
//	mergeDistanceThreshold = 0.5;

    mergeCThreshold = 0.1;
    mergeABThreshold = sin(10 * M_PI / 180);
    mergeDistanceThreshold = 1;

	leastPointNumInLine = 10;
	leastLineLength = 0.25;

    lastLeftDistance = 0.0;
    lastRightDistance = 0.0;
    lastFrontDistance = 0.0;
    leftDistanceThresholdMax = 2.0;
    rightDistanceThresholdMax = 2.0;
    frontDistanceThresholdMax = 3.0;
    leftDistanceThresholdMin = 1.0;
    rightDistanceThresholdMin = 1.0;
    frontDistanceThresholdMin = 1.0;
    leftDistanceThresholdStep = 0.5;
    rightDistanceThresholdStep = 0.5;
    frontDistanceThresholdStep = 0.5;
    leftDistanceThreshold = leftDistanceThresholdMax;
    rightDistanceThreshold = rightDistanceThresholdMax;
    frontDistanceThreshold = frontDistanceThresholdMax;
}

LineExtraction::~LineExtraction()
{
	vector<Point>().swap(laserData);
	vector<LineSegment>().swap(mergeResLineList);
}

void LineExtraction::update(LaserParam& laserParam, const vector<float> &rawLaserRange)
{
	auto startTime0 = chrono::steady_clock::now();

	laserStartAngle = laserParam.laserAngleMin;
	laserIncrement = laserParam.laserAngleIncrement;
	laserMaxRange = laserParam.laserRangeMax;
	//laserMaxRange = 3;

	//vector<LineSegment> clusterResLineList, spliteResLineList;

	//auto startTime = chrono::steady_clock::now();
	clusterPoint(rawLaserRange, clusterResLineList);
	//auto clusterEndTime = chrono::steady_clock::now();

	spliteLineList(clusterResLineList, spliteResLineList);
	//auto spliteEndTime = chrono::steady_clock::now();
	//cout << "splite size: " << spliteResLineList.size() << endl;

    mergeLineList(spliteResLineList, mergeResLineList);
	//cout << "merge size: " << mergeResLineList.size() << endl;
	auto mergeEndTime = chrono::steady_clock::now();

	calLane(mergeResLineList, lane);
	//auto laneEndTime = chrono::steady_clock::now();

	//auto duration = chrono::duration_cast<chrono::microseconds>(clusterEndTime - startTime);
	//double t = double(duration.count()) * chrono::microseconds::period::num / chrono::microseconds::period::den;
	//cout << "clusterTime: " << t << " Hz: " << 1 / t << endl;

	//duration = chrono::duration_cast<chrono::microseconds>(spliteEndTime - clusterEndTime);
	//t = double(duration.count()) * chrono::microseconds::period::num / chrono::microseconds::period::den;
	//cout << "spliteTime: " << t << " Hz: " << 1 / t << endl;

	auto duration = chrono::duration_cast<chrono::microseconds>(mergeEndTime - startTime0);
	double t = double(duration.count()) * chrono::microseconds::period::num / chrono::microseconds::period::den;
	//cout << "mergeTime: " << t << " Hz: " << 1 / t << endl;
    //cout<<endl<<t;

	//duration = chrono::duration_cast<chrono::microseconds>(laneEndTime - mergeEndTime);
	//t = double(duration.count()) * chrono::microseconds::period::num / chrono::microseconds::period::den;
	//cout << "laneTime: " << t << " Hz: " << 1 / t << endl;

	//clusterPoint(rawLaserRange, mergeResLineList);
	//spliteLineList(clusterResLineList, mergeResLineList);
	//auto endTime = chrono::steady_clock::now();
	vector<LineSegment>().swap(clusterResLineList);
	vector<LineSegment>().swap(spliteResLineList);

	
	//duration = chrono::duration_cast<chrono::microseconds>(endTime - laneEndTime);
	//t = double(duration.count()) * chrono::microseconds::period::num / chrono::microseconds::period::den;
	//cout << "--Time: " << t << " Hz: " << 1 / t << endl;
}

void LineExtraction::getLineSegments(vector<LineSegment> &lineSegments)
{
	lineSegments.clear();
	lineSegmentMutex.lock();
	lineSegments.insert(lineSegments.end(), mergeResLineList.begin(), mergeResLineList.end());
	lineSegmentMutex.unlock();
}

Lane LineExtraction::getLane()
{
	Lane tmpLane;
	laneMutex.lock();
	tmpLane = lane;
	laneMutex.unlock();
	return tmpLane;
}

void LineExtraction::clusterPoint(vector<float> rawLaserRange, vector<LineSegment> &resLineList)
{
    vector<LineSegment>().swap(resLineList);
	vector<Point>().swap(laserData);

	LineSegment currentLine;
	int laserSize = rawLaserRange.size();

	for (int i = 0; i < laserSize; ++i){
		//cluster by range distance
		if (i == 0){
			//reserve capacity to prevent memory reallocation
			vector<int>().swap(currentLine.pointIndexs);
			currentLine.pointIndexs.reserve(laserSize);
			if (rawLaserRange[i] < laserMaxRange - 0.1) currentLine.pointIndexs.push_back(i);
		}
		else if (abs(rawLaserRange[i] - rawLaserRange[i - 1]) > clusterDistanceThreshold){
			//vector<int>(currentLine.pointIndexs).swap(currentLine.pointIndexs);//minimize the space use of vector
            currentLine.pointIndexs.shrink_to_fit();
			resLineList.push_back(currentLine);

			//reserve capacity to prevent memory reallocation
			vector<int>().swap(currentLine.pointIndexs);
			currentLine.pointIndexs.reserve(laserSize);
			if (rawLaserRange[i] < laserMaxRange - 0.1) currentLine.pointIndexs.push_back(i);
			//if this point is the last, the line has only 1 point, so do not add to the lineList
			//the vector released out of for loop
		}
		else if (i == laserSize - 1){
			currentLine.pointIndexs.push_back(i);
			//vector<int>(currentLine.pointIndexs).swap(currentLine.pointIndexs);//minimize the space use of vector
            currentLine.pointIndexs.shrink_to_fit();
			resLineList.push_back(currentLine);
			//the vector released out of for loop
		}
		else{
			if (rawLaserRange[i] < laserMaxRange - 0.1) currentLine.pointIndexs.push_back(i);
		}

		//change range to (x,y)
        float theta, x, y;
		theta = (i * laserIncrement + laserStartAngle);// / 180 * M_PI;
		x = cos(theta) * rawLaserRange[i] + laserOffset;
		y = sin(theta) * rawLaserRange[i];// + laserOffset;
		laserData.push_back(Point{ x, y });
		//cout << "[" << i << "]" << x << "," << y << endl;
	}
	//release the vector
	vector<int>().swap(currentLine.pointIndexs);
}

int cnt = 0;
void LineExtraction::spliteLine(LineSegment &srcLine, vector<LineSegment> &resLineList)
{
    vector<LineSegment>().swap(resLineList);

    cnt++;
    float maxDistanceToLine = 0;
	int maxDistancePointIndex;
	int srcLinePointNum = srcLine.pointIndexs.size();
	Line srcFitLine = endPointLineFit(srcLine);

	

	for (int i = 0; i < srcLinePointNum; ++i){
        float currentDistanceToLine = abs(pointToLineDistance(laserData[srcLine.pointIndexs[i]], srcFitLine));
		if (currentDistanceToLine > maxDistanceToLine){
			maxDistanceToLine = currentDistanceToLine;
			maxDistancePointIndex = i;
		}
	}

	if (maxDistanceToLine > spliteDistanceThreshold){
		LineSegment srcLine1, srcLine2;
		vector<LineSegment> resLineList1, resLineList2;

		srcLine1.pointIndexs.insert(srcLine1.pointIndexs.begin(), srcLine.pointIndexs.begin(), srcLine.pointIndexs.begin() + maxDistancePointIndex);
		srcLine2.pointIndexs.insert(srcLine2.pointIndexs.begin(), srcLine.pointIndexs.begin() + maxDistancePointIndex, srcLine.pointIndexs.end());
		
		spliteLine(srcLine1, resLineList1);
		spliteLine(srcLine2, resLineList2);

		//auto startTime = chrono::steady_clock::now();

		//vector<LineSegment>().swap(resLineList);
		resLineList.clear();
		//resLineList.reserve(resLineList1.size() + resLineList2.size());
		resLineList.insert(resLineList.end(), resLineList1.begin(), resLineList1.end());
		resLineList.insert(resLineList.end(), resLineList2.begin(), resLineList2.end());

		//vector<LineSegment>().swap(resLineList1);
		//vector<LineSegment>().swap(resLineList2);
		//vector<int>().swap(srcLine1.pointIndexs);
		//vector<int>().swap(srcLine2.pointIndexs);

		//auto endTime = chrono::steady_clock::now();
		//auto duration = chrono::duration_cast<chrono::nanoseconds>(endTime - startTime);
		//double t = double(duration.count()) * chrono::nanoseconds::period::num / chrono::nanoseconds::period::den;
		//cout << "@@@@@@@@@@@: " << t << " Hz: " << 1 / t << endl;
	}
	else{
		//vector<LineSegment>().swap(resLineList);
		resLineList.clear();
		srcLine.lineParam = leastSquareLineFit(srcLine);
		//cout << "LINE " << tmpLine.lineParam.A << " " << tmpLine.lineParam.B << " " << tmpLine.lineParam.C << endl;
		//tmpLine.endPoint1 = laserData[srcLine.pointIndexs[0]];
		//tmpLine.endPoint2 = laserData[srcLine.pointIndexs[srcLinePointNum - 1]];
		srcLine.endPoint1 = verticalPoint(srcLine.lineParam, laserData[srcLine.pointIndexs[0]]);
		srcLine.endPoint2 = verticalPoint(srcLine.lineParam, laserData[srcLine.pointIndexs[srcLinePointNum - 1]]);
		resLineList.push_back(srcLine);
	}
	//vector<LineSegment>(resLineList).swap(resLineList);
    resLineList.shrink_to_fit();
}

void LineExtraction::spliteLineStack(LineSegment &srcLine, vector<LineSegment> &resLineList)
{
    vector<LineSegment>().swap(resLineList);

    resLineList.clear();
	resLineList.reserve(100);

	stack<int> splitePointStack;
	int minIndex = 0;
	int maxIndex = srcLine.pointIndexs.size();
	splitePointStack.push(maxIndex);

	while (!splitePointStack.empty()){
		++cnt;
        float maxDistanceToLine = 0;
		int maxDistancePointIndex;
		Line fitLine = endPointLineFit(srcLine, minIndex, maxIndex);

		for (int i = minIndex; i < maxIndex; ++i){
            float currentDistanceToLine = abs(pointToLineDistance(laserData[srcLine.pointIndexs[i]], fitLine));
			if (currentDistanceToLine > maxDistanceToLine){
				maxDistanceToLine = currentDistanceToLine;
				maxDistancePointIndex = i;
			}
		}

		if (maxDistanceToLine > spliteDistanceThreshold){
			maxIndex = maxDistancePointIndex;
			splitePointStack.push(maxDistancePointIndex);
		}
		else{
			LineSegment tmpLine;
			tmpLine.pointIndexs.insert(tmpLine.pointIndexs.end(), srcLine.pointIndexs.begin() + minIndex, srcLine.pointIndexs.begin() + maxIndex-1);
			//tmpLine.lineParam = leastSquareLineFit(tmpLine);
			tmpLine.lineParam = endPointLineFit(tmpLine);
			tmpLine.endPoint1 = verticalPoint(tmpLine.lineParam, laserData[tmpLine.pointIndexs[0]]);
			tmpLine.endPoint2 = verticalPoint(tmpLine.lineParam, laserData[tmpLine.pointIndexs[tmpLine.pointIndexs.size() - 1]]);
			resLineList.push_back(tmpLine);

			minIndex = splitePointStack.top();
			splitePointStack.pop();
			if (splitePointStack.empty()){
				break;
			}
			else{
				maxIndex = splitePointStack.top();
			}
		}
	}

	resLineList.shrink_to_fit();
}

void LineExtraction::spliteLineList(vector<LineSegment> srcLineList, vector<LineSegment> &resLineList)
{
    vector<LineSegment>().swap(resLineList);

    //vector<LineSegment>().swap(resLineList);
	//resLineList.clear();
	int srcLineListNum = srcLineList.size();
	for (int i = 0; i < srcLineListNum; ++i){
		if (srcLineList[i].pointIndexs.size() >= leastPointNumInLine){
			vector<LineSegment> tmpLineList;
			spliteLine(srcLineList[i], tmpLineList);
			//spliteLineStack(srcLineList[i], tmpLineList);
			resLineList.insert(resLineList.end(), tmpLineList.begin(), tmpLineList.end());
			//vector<LineSegment>().swap(tmpLineList);
		}		
	}
	//vector<LineSegment>(resLineList).swap(resLineList);
    resLineList.shrink_to_fit();
	//cout << "splite times: " << cnt << endl;
}

void LineExtraction::mergeLineList(vector<LineSegment> &srcLineList, vector<LineSegment> &resLineList)
{
    vector<LineSegment>().swap(resLineList);

    vector<LineSegment> sortLineList;
	vector<LineSegment>().swap(sortLineList);
	sortLineList.insert(sortLineList.end(), srcLineList.begin(), srcLineList.end());

	sort(sortLineList.begin(), sortLineList.end(), [](LineSegment lineSegment1, LineSegment lineSegment2){
		return (lineSegment1.lineParam.C < lineSegment2.lineParam.C);
	});
	int sortLineListSize = sortLineList.size();
	vector<int> sortLineListMask(sortLineListSize);
	for (int i = 0; i < sortLineListSize; ++i){
		if (sortLineListMask[i] != 0) continue;
		sortLineListMask[i] = 1;
		//cout << "[a] i: " << i << " " << sortLineList[i].lineParam.A << " " << sortLineList[i].lineParam.B << " " << sortLineList[i].lineParam.C << " size " << sortLineList[i].pointIndexs.size() << endl;
		for (int j = i + 1; j < sortLineListSize; ++j){
			if (sortLineListMask[j] != 0) continue;
			//cout << "[b] j0: " <<j<<" "<< sortLineList[j].lineParam.A << " " << sortLineList[j].lineParam.B << " " << sortLineList[j].lineParam.C << endl;

            //signC to konw wether the 2 line are the same side
            int signC = (sortLineList[j].lineParam.A * sortLineList[i].lineParam.A > 0) ? -1 : 1;
			if (abs(sortLineList[j].lineParam.C + signC*sortLineList[i].lineParam.C) < mergeCThreshold){
                float sin_da = abs(sortLineList[i].lineParam.A*sortLineList[j].lineParam.B - sortLineList[j].lineParam.A*sortLineList[i].lineParam.B);
				if (sin_da < mergeABThreshold){
                    float lineDistance = lineToLineDistance(sortLineList[i].lineParam, sortLineList[i].endPoint1, sortLineList[i].endPoint2,
						                                     sortLineList[j].lineParam, sortLineList[j].endPoint1, sortLineList[j].endPoint2);
					if (lineDistance<mergeDistanceThreshold){
						//merge
						//cout << "i size " << sortLineList[i].pointIndexs.size() << " j size " << sortLineList[j].pointIndexs.size() << endl;
						sortLineList[i].pointIndexs.insert(sortLineList[i].pointIndexs.end(), sortLineList[j].pointIndexs.begin(), sortLineList[j].pointIndexs.end());
						//cout << "i+j size " << sortLineList[i].pointIndexs.size() << endl;
						sortLineList[i].lineParam = leastSquareLineFit(sortLineList[i]);
						vector<Point> endPoints = { sortLineList[i].endPoint1, sortLineList[i].endPoint2, sortLineList[j].endPoint1, sortLineList[j].endPoint2 };
                        float lineA = sortLineList[i].lineParam.A;
                        float lineB = sortLineList[i].lineParam.B;
						sort(endPoints.begin(), endPoints.end(), [lineA, lineB](Point endPoint1, Point endPoint2){
							return ((endPoint1.x*lineB - endPoint1.y*lineA) < (endPoint2.x*lineB - endPoint2.y*lineA));
						});
						sortLineList[i].endPoint1 = verticalPoint(sortLineList[i].lineParam, endPoints[0]);
						sortLineList[i].endPoint2 = verticalPoint(sortLineList[i].lineParam, endPoints[3]);
						//sortLineList[i].endPoint1 = endPoints[0];
						//sortLineList[i].endPoint2 = endPoints[3];
						vector<Point>().swap(endPoints);

						sortLineListMask[j] = -1;
						//cout << "merge " << j << " to " << i << endl;
						//cout << "[m] i+j: "<< sortLineList[i].lineParam.A << " " << sortLineList[i].lineParam.B << " " << sortLineList[i].lineParam.C << endl;
					}
				}
			}
		}
	}

	lineSegmentMutex.lock();
	vector<LineSegment>().swap(resLineList);
	for (int i = 0; i < sortLineListSize; ++i){
		if (sortLineListMask[i] == 1){
			sortLineList[i].endPoint1 = verticalPoint(sortLineList[i].lineParam, sortLineList[i].endPoint1);
			sortLineList[i].endPoint2 = verticalPoint(sortLineList[i].lineParam, sortLineList[i].endPoint2);
			if (pointToPointDistance(sortLineList[i].endPoint1, sortLineList[i].endPoint2) > leastLineLength){
				resLineList.push_back(sortLineList[i]);
			}
		}
	}
	//vector<LineSegment>(resLineList).swap(resLineList);
    resLineList.shrink_to_fit();
	lineSegmentMutex.unlock();

	vector<LineSegment>().swap(sortLineList);        
	vector<int>().swap(sortLineListMask);
}

void LineExtraction::calLane(vector<LineSegment> srcLineList, Lane &resLane)
{
	resLane = {};
    float minLineDistance = 0.3;
    float maxLineDistance = 3.0;
    float maxPointDistance = 6.0;
    float thetaToIncrease = 10.0;


    float lastLeftTheta = 0.0;
    float lastRightTheta = 0.0;
    float lastFrontTheta = 0.0;

    float lastLeftDistanceTmp = lastLeftDistance;
    float lastRightDistanceTmp = lastRightDistance;


	sort(srcLineList.begin(), srcLineList.end(), [lastLeftDistanceTmp, lastRightDistanceTmp](LineSegment lineSegment1, LineSegment lineSegment2){
		if (lineSegment1.lineParam.A > 0 && lineSegment2.lineParam.A < 0){
			return true;
		}
		else if (lineSegment1.lineParam.A < 0 && lineSegment2.lineParam.A > 0){
			return false;
		}
		else if (lineSegment1.lineParam.A > 0 && lineSegment2.lineParam.A > 0){
			return (abs(lastLeftDistanceTmp -  lineSegment1.lineParam.C) < abs(lastLeftDistanceTmp - lineSegment2.lineParam.C));
		}
		else if (lineSegment1.lineParam.A < 0 && lineSegment2.lineParam.A < 0){
			return (abs(lastRightDistanceTmp - lineSegment1.lineParam.C) < abs(lastRightDistanceTmp - lineSegment2.lineParam.C));
		}
		else{
			return false;
		}
	});


	int srcSize = srcLineList.size();
    float leftLength = 0.0;
    float rightLength = 0.0;

    //find the longest line
    float maxLength = 0;
    int maxLengthIndex = -1;
    for (int i = 0; i < srcSize; ++i){
        float l = pointToPointDistance(srcLineList[i].endPoint1, srcLineList[i].endPoint2);
        if(l>maxLength){
            maxLength = l;
            maxLengthIndex = i;
        }
    }
    float maxLengthTheta = atan(srcLineList[maxLengthIndex].lineParam.B / srcLineList[maxLengthIndex].lineParam.A) * 180 / M_PI;
    if(abs(maxLengthTheta) < 45){//dont konw < or > means!!!!!!!!!!!!!
        //in the main lane, longest is shelf line, lane lines are short
        lastLeftTheta = (maxLengthTheta > 0) ? (maxLengthTheta - 90) : (maxLengthTheta + 90);
        lastRightTheta = lastLeftTheta;
		lastFrontTheta = maxLengthTheta;
    }
    else{
        lastLeftTheta = maxLengthTheta;
        lastRightTheta = lastLeftTheta;
		lastFrontTheta = (maxLengthTheta > 0) ? (maxLengthTheta - 90) : (maxLengthTheta + 90);
    }



    vector<Point> leftEndPoints, rightEndPoints, frontEndPoints;
    LineSegment leftLine, rightLine, frontLine;
    float leftTheta = 0, rightTheta = 0, frontTheta = 0;

	for (int i = 0; i < srcSize; ++i){
        float theta = atan(srcLineList[i].lineParam.B / srcLineList[i].lineParam.A);
		//cout << theta * 180 / M_PI << endl;
		if (srcLineList[i].lineParam.A < 0){//left
            //find all the left lines belong to lane
			if (abs(theta * 180 / M_PI - lastLeftTheta) < 20
                && abs(lastLeftDistance - srcLineList[i].lineParam.C) < leftDistanceThreshold
                && abs(srcLineList[i].lineParam.C) < leftDistanceThresholdMax){
                leftEndPoints.push_back(srcLineList[i].endPoint1);
                leftEndPoints.push_back(srcLineList[i].endPoint2);
                leftLine.pointIndexs.insert(leftLine.pointIndexs.end(), srcLineList[i].pointIndexs.begin(), srcLineList[i].pointIndexs.end());
			}
        }
		else{//right
            //find all the right lines belong to lane
			if (abs(theta * 180 / M_PI - lastRightTheta) < 20
                && abs(lastRightDistance - srcLineList[i].lineParam.C) < rightDistanceThreshold
                && abs(srcLineList[i].lineParam.C) < rightDistanceThresholdMax){
                rightEndPoints.push_back(srcLineList[i].endPoint1);
                rightEndPoints.push_back(srcLineList[i].endPoint2);
                rightLine.pointIndexs.insert(rightLine.pointIndexs.end(), srcLineList[i].pointIndexs.begin(), srcLineList[i].pointIndexs.end());
			}
		}

        //front
        if (abs(theta * 180 / M_PI - lastFrontTheta) < 20
            && abs(lastFrontDistance - srcLineList[i].lineParam.C) < frontDistanceThreshold
            && abs(srcLineList[i].lineParam.C) < frontDistanceThresholdMax){
            frontEndPoints.push_back(srcLineList[i].endPoint1);
            frontEndPoints.push_back(srcLineList[i].endPoint2);
            frontLine.pointIndexs.insert(frontLine.pointIndexs.end(), srcLineList[i].pointIndexs.begin(), srcLineList[i].pointIndexs.end());
        }
	}

    //todo: add distance judgment
    if(leftLine.pointIndexs.size() > 0){
        leftLine.lineParam = leastSquareLineFit(leftLine);
        float leftLineA = leftLine.lineParam.A;
        float leftLineB = leftLine.lineParam.B;
        sort(leftEndPoints.begin(), leftEndPoints.end(), [leftLineA, leftLineB](Point endPoint1, Point endPoint2){
            return ((endPoint1.x*leftLineB - endPoint1.y*leftLineA) < (endPoint2.x*leftLineB - endPoint2.y*leftLineA));
        });
        leftLine.endPoint1 = verticalPoint(leftLine.lineParam, leftEndPoints.front());
        leftLine.endPoint2 = verticalPoint(leftLine.lineParam, leftEndPoints.back());
        vector<Point>().swap(leftEndPoints);
        leftTheta = atan(leftLine.lineParam.B / leftLine.lineParam.A) * 180 / M_PI;

        lastLeftDistance = leftLine.lineParam.C;
        leftDistanceThreshold -= leftDistanceThresholdStep;
        if(leftDistanceThreshold < leftDistanceThresholdMin){
            leftDistanceThreshold = leftDistanceThresholdMin;
        }
    }
    else{
        lastLeftDistance = 0;
        leftDistanceThreshold += leftDistanceThresholdStep;
        if(leftDistanceThreshold > leftDistanceThresholdMax){
            leftDistanceThreshold = leftDistanceThresholdMax;
        }
    }
    if(rightLine.pointIndexs.size() > 0){
        rightLine.lineParam = leastSquareLineFit(rightLine);
        float rightLineA = rightLine.lineParam.A;
        float rightLineB = rightLine.lineParam.B;
        sort(rightEndPoints.begin(), rightEndPoints.end(), [rightLineA, rightLineB](Point endPoint1, Point endPoint2){
            return ((endPoint1.x*rightLineB - endPoint1.y*rightLineA) < (endPoint2.x*rightLineB - endPoint2.y*rightLineA));
        });
        rightLine.endPoint1 = verticalPoint(rightLine.lineParam, rightEndPoints.front());
        rightLine.endPoint2 = verticalPoint(rightLine.lineParam, rightEndPoints.back());
        vector<Point>().swap(rightEndPoints);
        rightTheta = atan(rightLine.lineParam.B / rightLine.lineParam.A) * 180 / M_PI;

        lastRightDistance = rightLine.lineParam.C;
        rightDistanceThreshold -= rightDistanceThresholdStep;
        if(rightDistanceThreshold < rightDistanceThresholdMin){
            rightDistanceThreshold = rightDistanceThresholdMin;
        }
    }
    else{
        lastRightDistance = 0;
        rightDistanceThreshold += rightDistanceThresholdStep;
        if(rightDistanceThreshold > rightDistanceThresholdMax){
            rightDistanceThreshold = rightDistanceThresholdMax;
        }
    }
    if(frontLine.pointIndexs.size() > 0){
        frontLine.lineParam = leastSquareLineFit(frontLine);
        float frontLineA = frontLine.lineParam.A;
        float frontLineB = frontLine.lineParam.B;
        sort(frontEndPoints.begin(), frontEndPoints.end(), [frontLineA, frontLineB](Point endPoint1, Point endPoint2){
            return ((endPoint1.x*frontLineB - endPoint1.y*frontLineA) < (endPoint2.x*frontLineB - endPoint2.y*frontLineA));
        });
        frontLine.endPoint1 = verticalPoint(frontLine.lineParam, frontEndPoints.front());
        frontLine.endPoint2 = verticalPoint(frontLine.lineParam, frontEndPoints.back());
        vector<Point>().swap(frontEndPoints);
        frontTheta = atan(frontLine.lineParam.B / frontLine.lineParam.A) * 180 / M_PI;

        lastFrontDistance = frontLine.lineParam.C;
        frontDistanceThreshold -= frontDistanceThresholdStep;
        if(frontDistanceThreshold < frontDistanceThresholdMin){
            frontDistanceThreshold = frontDistanceThresholdMin;
        }
    }
    else{
        lastFrontDistance = 0;
        frontDistanceThreshold += frontDistanceThresholdStep;
        if(frontDistanceThreshold > frontDistanceThresholdMax){
            frontDistanceThreshold = frontDistanceThresholdMax;
        }
    }

    laneMutex.lock();
    resLane.leftLine = leftLine.lineParam;
    resLane.thetaToLeftLine = leftTheta;
    resLane.distanceToLeftLine = leftLine.lineParam.C;
    resLane.leftLineForPaint = LineForPaint{ leftLine.endPoint1, leftLine.endPoint2 };

    resLane.rightLine = rightLine.lineParam;
    resLane.thetaToRightLine = rightTheta;
    resLane.distanceToRightLine = rightLine.lineParam.C;
    resLane.rightLineForPaint = LineForPaint{ rightLine.endPoint1, rightLine.endPoint2 };

    resLane.frontLine = frontLine.lineParam;
    resLane.thetaToFrontLine = frontTheta;
    resLane.distanceToFrontLine = frontLine.lineParam.C;
    resLane.frontLineForPaint = LineForPaint{ frontLine.endPoint1, frontLine.endPoint2 };
    laneMutex.unlock();


}

float LineExtraction::pointToLineDistance(Point point, Line line)
{
	//need line.A and line.B != 0, and AB normalized A^2+B^2 =1
	//return (abs(line.A*point.x + line.B*point.y + line.C) / sqrt(line.A*line.A + line.B*line.B));
	return line.A*point.x + line.B*point.y + line.C;
}

float LineExtraction::pointToPointDistance(Point point1, Point point2)
{
	return sqrt((point1.x - point2.x)*(point1.x - point2.x) + (point1.y - point2.y)*(point1.y - point2.y));
}

float LineExtraction::lineToLineDistance(Line line1, Point endPoint11, Point endPoint12, Line line2, Point endPoint21, Point endPoint22)
{
	vector<vector<Point>> endPoints = { { endPoint11, endPoint12 }, { endPoint21, endPoint22 } };
	vector<Line> lines = { line1, line2 };

	//check whether the lines are intersected//*WRONG*//
	//for (int i = 0; i < 2; ++i){
	//	if (pointToLineDistance(endPoints[i][0], lines[1 - i]) * pointToLineDistance(endPoints[i][1], lines[1 - i]) < 1e-10){
	//		return 0;
	//	}
	//}

	//not intersect, cal the distance
    float distance = 9876543210;
	//check whether endpoint is between the another line
	for (int i = 0; i < 2; ++i){
		Line verticalLine1 = verticalLine(lines[1 - i], endPoints[1 - i][0]);
		Line verticalLine2 = verticalLine(lines[1 - i], endPoints[1 - i][1]);
		for (int j = 0; j < 2; ++j){
			Point point = endPoints[i][j];
			if (pointToLineDistance(point, verticalLine1) * pointToLineDistance(point, verticalLine2) < 0){
                float tmpDistance = abs(pointToLineDistance(point, lines[1 - i]));
				if (tmpDistance < distance){
					distance = tmpDistance;
				}
			}
		}
	}
	//calculate distance between 4 endPoints, get the min distance
	for (int i = 0; i < 2; ++i){
		for (int j = 0; j < 2; ++j){
            float tmpDistance = pointToPointDistance(endPoints[0][i], endPoints[1][j]);
			if (tmpDistance < distance){
				distance = tmpDistance;
			}
		}
	}

	return distance;
}

Line LineExtraction::verticalLine(Line srcLine, Point passPoint)
{
    float LineA = srcLine.B;
    float LineB = -srcLine.A;
    float LineC = srcLine.A * passPoint.y - srcLine.B * passPoint.x;
	return Line{ LineA, LineB, LineC };
}

Point LineExtraction::verticalPoint(Line srcLine, Point passPoint)
{
	if (pointToLineDistance(passPoint, srcLine) < 0.05){
		return passPoint;
	}
	else{
        float A = srcLine.A;
        float B = srcLine.B;
        float C = srcLine.C;
        float X = passPoint.x;
        float Y = passPoint.y;
		return Point{ B*B*X - A*B*Y - A*C, -A*B*X + A*A*Y - B*C };
	}
}

Line LineExtraction::leastSquareLineFit(LineSegment srcLine, int startIndex, int endIndex)
{
    float LineA, LineB, LineC;
    float sumX = 0, sumY = 0, sumXY = 0, sumX2 = 0, sumY2 = 0;
	int N = srcLine.pointIndexs.size();//N should > 1

	int min, max;
	if (startIndex != -1 && endIndex != -1){
		min = startIndex;
		max = endIndex;
	}
	else{
		min = 0;
		max = N;
	}

	for (int i = min; i < max; ++i){
		int pointIndex = srcLine.pointIndexs[i];
        float x = laserData[pointIndex].x;
        float y = laserData[pointIndex].y;
		sumX += x;
		sumY += y;
		sumXY += x*y;
		sumX2 += x*x;
		sumY2 += y*y;
	}
	//LineA = N * sumXY - sumX * sumY;
	//LineB = sumX * sumX - N * sumX2;
	//LineC = sumX2 * sumY - sumX * sumXY;
	LineA = sumX*sumY2 - sumY*sumXY;
	LineB = sumY*sumX2 - sumX*sumXY;
	LineC = (2 * sumX*sumY*sumXY - sumX*sumX*sumY2 - sumY*sumY*sumX2) / (max - min);
	//cout << "sumX: " << sumX << " sumY: " << sumY << " sumX2: " << sumX2 << " sumXY: " << sumXY << endl;
	//cout << "N,A,B,C:" << N << " " << LineA << " " << LineB << " " << LineC << endl;

	return normalizeLine(Line{ LineA, LineB, LineC });
	//return Line{ LineA, LineB, LineC };
}

Line LineExtraction::endPointLineFit(LineSegment srcLine, int startIndex, int endIndex)
{
    float LineA, LineB, LineC;

	int N = srcLine.pointIndexs.size();//N should > 1
	int endPoint1Index, endPoint2Index;
	if (startIndex != -1 && endIndex != -1){
		endPoint1Index = srcLine.pointIndexs[startIndex];
		endPoint2Index = srcLine.pointIndexs[endIndex-1];
	}
	else{
		endPoint1Index = srcLine.pointIndexs[0];
		endPoint2Index = srcLine.pointIndexs[N - 1];
	}

    float x1 = laserData[endPoint1Index].x;
    float y1 = laserData[endPoint1Index].y;
    float x2 = laserData[endPoint2Index].x;
    float y2 = laserData[endPoint2Index].y;

	LineA = y2 - y1;
	LineB = x1 - x2;
	LineC = x2 * y1 - x1 * y2;

	return normalizeLine(Line{ LineA, LineB, LineC });
}

Line LineExtraction::normalizeLine(Line srcLine)
{
    float LineA = srcLine.A;
    float LineB = srcLine.B;
    float LineC = srcLine.C;
    float LineAB = sqrt(LineA*LineA + LineB*LineB);
	if (LineC < 0) LineAB = -LineAB;
	if (abs(LineAB) > 1e-10){ //LineAB != 0
		LineA = LineA / LineAB;
		LineB = LineB / LineAB;
		LineC = LineC / LineAB;
	}
	else{
		LineA = 0;
		LineB = 0;
		LineC = 0;
	}
	return Line{ LineA, LineB, LineC };
}