/**************************************************************************
* Description: structs of Laser.h
* Author     : crtianwei
* Create Date: 2017-02-28
**************************************************************************/
#ifndef _LASERSTRUCTS_H_
#define _LASERSTRUCTS_H_

struct LaserParam
{
	double laserAngleMin;
	double laserAngleMax;
	double laserAngleIncrement;
	double laserRangeMin;
	double laserRangeMax;
	unsigned int laserDataNum;
};

#endif