/**************************************************************************
* Description: lineExtraction with raw laser data
* Author     : crtianwei
* Create Date: 2017-02-20
**************************************************************************/

/*need to check Memory Leak*/

#ifndef _LINEEXTRACTIONSTRUCTS_H_
#define _LINEEXTRACTIONSTRUCTS_H_

#include <vector>
using namespace std;

/**
 * @struct Point
 * @brief present a point (x,y)
 */
struct Point
{
    float x;
    float y;
};

/**
 * @struct Line
 * @brief present a line Ax+By+C=0
 */
struct Line
{
    float A;
    float B;
    float C;
};

/**
 * @struct LineForPaint
 * @brief present a line by 2 endpints
 */
struct LineForPaint
{
	Point endPoint1;
	Point endPoint2;
};

/**
 * @struct LineSegment
 * @brief line segments
 */
struct LineSegment
{
	//line param
	Line lineParam;
	//one of the line endpoints
	Point endPoint1;
	//one of the line endpoints
	Point endPoint2;
	//point(belong to this lineSegment) indexs of laserData
	vector<int> pointIndexs;

	LineSegment() :lineParam(Line{ 0, 0, 0 }), endPoint1(Point{ 0, 0 }), endPoint2(Point{ 0, 0 }){}
};

/**
 *
 *
 */
struct Lane
{
	Line leftLine;
	float distanceToLeftLine;
    float thetaToLeftLine;
	LineForPaint leftLineForPaint;

	Line rightLine;
    float distanceToRightLine;
    float thetaToRightLine;
	LineForPaint rightLineForPaint;

    Line frontLine;
    float distanceToFrontLine;
    float thetaToFrontLine;
    LineForPaint frontLineForPaint;
	
};

#endif