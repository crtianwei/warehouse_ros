//
// Created by crti on 4/26/17.
//

#ifndef FEATUREDETECTION_FEATUREDETECTION_H
#define FEATUREDETECTION_FEATUREDETECTION_H

#include "ros/ros.h"
#include "sensor_msgs/LaserScan.h"
#include "nav_msgs/Odometry.h"
#include "visualization_msgs/Marker.h"
#include "geometry_msgs/Point.h"
#include "../msg/Lane.h"
#include "../msg/LineSegment.h"
#include "../msg/LineSegmentList.h"

#include "lineExtraction.h"

class FeatureDetection
{
public:
    FeatureDetection();
    ~FeatureDetection(){};

private:

    LineExtraction lineExtraction;
    LaserParam laserParam;
    Lane lane;

    //ros
    ros::NodeHandle nh_;
    ros::NodeHandle private_nh_;

    //Subscriber & Publisher

    ros::Subscriber laserScanSub;
    void laserScanCallback(const sensor_msgs::LaserScanConstPtr& laserScan);

    ros::Subscriber odometrySub;
    void odometryCallback(const nav_msgs::OdometryConstPtr& odometry);

    ros::Subscriber initFeatureParamSub;
    //void initFeatureParamCallback(const);

    ros::Publisher featurePub;
    ros::Publisher localPosePub;

    ros::Publisher lineSegmentsPub;
    ros::Publisher lineSegmentsShowPub;
    //parameters


};




#endif //FEATUREDETECTION_FEATUREDETECTION_H
