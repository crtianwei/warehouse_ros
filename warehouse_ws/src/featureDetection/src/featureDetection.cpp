//
// Created by crti on 4/26/17.
//

#include "featureDetection.h"

//todo: error when no laser data
FeatureDetection::FeatureDetection() :
        private_nh_("~")
{
    //private_nh_.param(

    laserScanSub = nh_.subscribe("scan", 1, &FeatureDetection::laserScanCallback, this);
    //odometrySub = nh_.subscribe("odom", 1, &FeatureDetection::odometryCallback, this);

    featurePub = nh_.advertise<visualization_msgs::Marker>("featureLineS", 0);
    localPosePub = nh_.advertise<featureDetection::Lane>("localPose",0);

    lineSegmentsPub = nh_.advertise<featureDetection::LineSegmentList>("lineSegments",0);
    lineSegmentsShowPub = nh_.advertise<visualization_msgs::Marker>("lineSegmentsShow", 0);


}



void FeatureDetection::laserScanCallback(const sensor_msgs::LaserScanConstPtr& laserScan)
{
    laserParam.laserAngleIncrement = laserScan->angle_increment;
    laserParam.laserAngleMax = laserScan->angle_max;
    laserParam.laserAngleMin = laserScan->angle_min;
    laserParam.laserRangeMax = laserScan->range_max;
    laserParam.laserRangeMin = laserScan->range_min;
    laserParam.laserDataNum = laserScan->ranges.size();
    //cout<<laserParam.laserAngleIncrement<<" "<<laserParam.laserAngleMax<<" "<<laserParam.laserAngleMin<<" "<<laserParam.laserRangeMax<<" "<<laserParam.laserRangeMin<<" "<<laserParam.laserDataNum<<endl;

    /*if(laserScan->ranges.size()>800){//real laser 270deg, use 180deg
        laserParam.laserAngleMax = 90.0*M_PI/180;
        laserParam.laserAngleMin = -90.0*M_PI/180;
        vector<float> partLaserScan;
        partLaserScan.reserve(721);
        for(int i=180; i<=900; ++i){
            partLaserScan.push_back(laserScan->ranges[i]);
        }
        laserParam.laserDataNum = partLaserScan.size();
        //cout<<laserParam.laserAngleIncrement<<" "<<laserParam.laserAngleMax<<" "<<laserParam.laserAngleMin<<" "<<laserParam.laserRangeMax<<" "<<laserParam.laserRangeMin<<" "<<laserParam.laserDataNum<<endl;
        lineExtraction.update(laserParam, partLaserScan);
    }
    else{*/
        lineExtraction.update(laserParam, laserScan->ranges);
    //}


    lane = lineExtraction.getLane();
    //publish visualization_msgs/Marker Line List (LINE_LIST=5)
    visualization_msgs::Marker featureLineS;
    featureLineS.header.frame_id = "base_link";
    //featureLineS.header.stamp = ros::Time::now();
    featureLineS.header.stamp = laserScan->header.stamp;
    featureLineS.ns = "featureLineNameSpace";
    featureLineS.id = 0;
    featureLineS.type = visualization_msgs::Marker::LINE_LIST;
    featureLineS.action = visualization_msgs::Marker::MODIFY;
    featureLineS.scale.x = 0.1;
    featureLineS.color.g = 1.0;
    featureLineS.color.a = 1.0;

    geometry_msgs::Point p;
    p.z = 0;
    p.x = lane.leftLineForPaint.endPoint1.x;
    p.y = lane.leftLineForPaint.endPoint1.y;
    //cout<<"["<<p.x<<","<<p.y<<"] ";
    featureLineS.points.push_back(p);
    p.x = lane.leftLineForPaint.endPoint2.x;
    p.y = lane.leftLineForPaint.endPoint2.y;
    //cout<<"["<<p.x<<","<<p.y<<"] ";
    featureLineS.points.push_back(p);
    p.x = lane.rightLineForPaint.endPoint1.x;
    p.y = lane.rightLineForPaint.endPoint1.y;
    //cout<<"["<<p.x<<","<<p.y<<"] ";
    featureLineS.points.push_back(p);
    p.x = lane.rightLineForPaint.endPoint2.x;
    p.y = lane.rightLineForPaint.endPoint2.y;
    //cout<<"["<<p.x<<","<<p.y<<"] "<<endl;
    featureLineS.points.push_back(p);
    p.x = lane.frontLineForPaint.endPoint1.x;
    p.y = lane.frontLineForPaint.endPoint1.y;
    //cout<<"["<<p.x<<","<<p.y<<"] "<<endl;
    featureLineS.points.push_back(p);
    p.x = lane.frontLineForPaint.endPoint2.x;
    p.y = lane.frontLineForPaint.endPoint2.y;
    //cout<<"["<<p.x<<","<<p.y<<"] "<<endl;
    featureLineS.points.push_back(p);

    featurePub.publish(featureLineS);

    featureDetection::Lane L;
    L.header.stamp = featureLineS.header.stamp;
    L.leftDistance = lane.distanceToLeftLine;
    L.leftTheta = lane.thetaToLeftLine;
    L.rightDistance = lane.distanceToRightLine;
    L.rightTheta = lane.thetaToRightLine;
    L.frontDistance = lane.distanceToFrontLine;
    L.frontTheta = lane.thetaToFrontLine;

    localPosePub.publish(L);


    visualization_msgs::Marker lineSegmentsShow;
    lineSegmentsShow.header.frame_id = "base_link";
    lineSegmentsShow.header.stamp = featureLineS.header.stamp;
    lineSegmentsShow.ns = "lineSegmentsShowNameSpace";
    lineSegmentsShow.id = 0;
    lineSegmentsShow.type = visualization_msgs::Marker::LINE_LIST;
    lineSegmentsShow.action = visualization_msgs::Marker::MODIFY;
    lineSegmentsShow.scale.x = 0.1;
    lineSegmentsShow.color.g = 0.5;
    lineSegmentsShow.color.b = 1.0;
    lineSegmentsShow.color.a = 0.6;

    //geometry_msgs::Point p;
    featureDetection::LineSegmentList LSL;
    LSL.header.stamp = featureLineS.header.stamp;

    for(int i=0; i<lineExtraction.mergeResLineList.size(); ++i){
        featureDetection::LineSegment LS;
        LS.A = lineExtraction.mergeResLineList[i].lineParam.A;
        LS.B = lineExtraction.mergeResLineList[i].lineParam.B;
        LS.C = lineExtraction.mergeResLineList[i].lineParam.C;
        LS.point1_x = lineExtraction.mergeResLineList[i].endPoint1.x;
        LS.point1_y = lineExtraction.mergeResLineList[i].endPoint1.y;
        LS.point2_x = lineExtraction.mergeResLineList[i].endPoint2.x;
        LS.point2_y = lineExtraction.mergeResLineList[i].endPoint2.y;
        LS.pointIndex.reserve(lineExtraction.mergeResLineList[i].pointIndexs.size());
        for(int j=0; j<lineExtraction.mergeResLineList[i].pointIndexs.size(); ++j){
            LS.pointIndex[j] = lineExtraction.mergeResLineList[i].pointIndexs[j];
        }
        LSL.lineSegments.push_back(LS);

        p.x = LS.point1_x;
        p.y = LS.point1_y;
        lineSegmentsShow.points.push_back(p);
        p.x = LS.point2_x;
        p.y = LS.point2_y;
        lineSegmentsShow.points.push_back(p);
        //cout<<LS.point1_x<<" "<<LS.point1_y<<" "<<LS.point2_x<<" "<<LS.point2_y<<endl;
    }
    //cout<<"___________________________"<<endl;

    lineSegmentsPub.publish(LSL);
    lineSegmentsShowPub.publish(lineSegmentsShow);

}

void FeatureDetection::odometryCallback(const nav_msgs::OdometryConstPtr& odometry)
{
cout<<odometry->pose.pose.position.x<<", "<<odometry->pose.pose.position.y<<", "<<endl;
}
