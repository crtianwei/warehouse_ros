//
// Created by crti on 17-12-19.
//

#ifndef TOPOMAPBUILD_TOPOMAPBUILD_H
#define TOPOMAPBUILD_TOPOMAPBUILD_H


#include "ros/ros.h"
#include "sensor_msgs/LaserScan.h"
#include "nav_msgs/Odometry.h"
#include "visualization_msgs/Marker.h"
#include "geometry_msgs/Point.h"
#include "std_msgs/String.h"

#include "../msg/FeaturePolygon.h"

#include <math.h>
#include <chrono>
#include <iostream>
#include <fstream>

#include <tf/tf.h>

#include "g2o/core/sparse_optimizer.h"
#include "g2o/core/block_solver.h"
#include "g2o/core/factory.h"
#include "g2o/core/optimization_algorithm_factory.h"
#include "g2o/core/optimization_algorithm_gauss_newton.h"
#include "g2o/core/optimization_algorithm_levenberg.h"
/*#include "core/sparse_optimizer.h"
#include "core/block_solver.h"
#include "core/factory.h"
#include "core/optimization_algorithm_factory.h"
#include "core/optimization_algorithm_gauss_newton.h"*/

#include "g2o/solvers/csparse/linear_solver_csparse.h"

#include "g2o/types/slam2d/vertex_se2.h"
#include "g2o/types/slam2d/vertex_point_xy.h"
#include "g2o/types/slam2d/edge_se2.h"
#include "g2o/types/slam2d/edge_se2_pointxy.h"

G2O_USE_TYPE_GROUP(slam2d);

using namespace std;
using namespace g2o;

struct MAPEDGE{
    int no1;//firts node index
    int no2;//second node index, must no1<no2
    friend bool operator <(MAPEDGE const& a, MAPEDGE const& b){
        return (a.no1==b.no1) ? (a.no2<b.no2) : (a.no1<b.no1);
    }
};

class TopoMapBuild {
public:
    TopoMapBuild();
    ~TopoMapBuild(){};

private:
    //ros
    ros::NodeHandle nh_;
    ros::NodeHandle private_nh_;

    ros::Subscriber nodeSub;
    void nodeCallback(const topoNodeDelaunay::FeaturePolygonConstPtr& node);

    ros::Subscriber saveMapSub;
    void saveMapCallback(const std_msgs::StringConstPtr& fileName);

    ros::Publisher topoAllNodeShowPub;
    ros::Publisher topoAllEdgeShowPub;

    int findNode(topoNodeDelaunay::FeaturePolygon& node);//-1 if not find, if find return index in mapNodes, -1000-i same place whit i node but different edge
    int addEdge(int index1, int index2);
    void updateNode(topoNodeDelaunay::FeaturePolygon& node, int index);
    void forceUpdateNode(topoNodeDelaunay::FeaturePolygon& node, int index);
    void g2oLoop();//return current robot position

    void drawMap();
    void saveMap(string &mapFileName);


    vector<topoNodeDelaunay::FeaturePolygon> mapNodes;
    set<MAPEDGE> mapEdges;
    //topoNodeDelaunay::FeaturePolygon lastNode;
    int lastNodeIndex, currentNodeIndex;


    float poseDistanceTh, edgeTh, angleTh;
    float startFindDis;
    float xNoise, yNoise, tNoise;//odom
    float landmarkXNoise, landmarkYNoise;//landmark
};


#endif //TOPOMAPBUILD_TOPOMAPBUILD_H
