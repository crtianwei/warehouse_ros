//
// Created by crti on 17-12-19.
//

#include "topoMapBuild.h"

TopoMapBuild::TopoMapBuild() :
        private_nh_("~")
{
    lastNodeIndex = 0;
    //currentNodeIndex = 0;
    poseDistanceTh = 2;
    edgeTh = 0.5;
    angleTh = 20;
    startFindDis = 1.5;

    xNoise = 0.1;
    yNoise = 0.1;
    tNoise = 0.1;
    landmarkXNoise = 0.1;
    landmarkYNoise = 0.1;

    nodeSub = nh_.subscribe("currentNode",1,&TopoMapBuild::nodeCallback,this);
    saveMapSub = nh_.subscribe("saveMap",1,&TopoMapBuild::saveMapCallback,this);

    topoAllNodeShowPub = nh_.advertise<visualization_msgs::Marker>("topoAllNode", 0);
    topoAllEdgeShowPub = nh_.advertise<visualization_msgs::Marker>("topoAllEdge", 0);
    cout<<"START"<<endl;
}




void TopoMapBuild::nodeCallback(const topoNodeDelaunay::FeaturePolygonConstPtr& node)
{
    //zugoujin cai jiance
    if(node->edgeNum<=0 || node->goodEdgeNum<=0){
        return;
    }
    float minDis = node->robotx*node->robotx+node->roboty*node->roboty;
    for(int i=0; i<node->pointx.size(); ++i){
        float tmpDis = node->pointx[i]*node->pointx[i] + node->pointy[i]*node->pointy[i];
        if(tmpDis<minDis){
            minDis = tmpDis;
        }
    }
    if(minDis > startFindDis*startFindDis){
        return;
    }

    topoNodeDelaunay::FeaturePolygon N;
    N.edgeNum = node->edgeNum;
    N.goodEdgeNum = node->goodEdgeNum;
    N.robotx = node->robotx;//x in robot coor
    N.roboty = node->roboty;
    N.mapx = node->mapx;//x in map coor
    N.mapy = node->mapy;
    N.odomx = node->odomx;
    N.odomy = node->odomy;
    N.odomt = node->odomt;
    for(int i=0; i<node->feature.size(); ++i){
        N.feature.push_back(node->feature[i]);
    }

    if(mapNodes.size()==0){//first node
        mapNodes.push_back(N);
        lastNodeIndex = 0;
        //currentNodeIndex = 0;
        cout<<"first node: ("<<N.mapx<<","<<N.mapy<<" "<<(int)N.edgeNum<<") "<<mapNodes.size()<<endl;
    }
    else{
        int sameNodeIndex = findNode(N);
        if(sameNodeIndex==-1){//new node
            mapNodes.push_back(N);
            addEdge(mapNodes.size()-1, lastNodeIndex);
            lastNodeIndex = mapNodes.size()-1;
            cout<<"new node: ("<<N.mapx<<","<<N.mapy<<" "<<(int)N.edgeNum<<") "<<mapNodes.size()<<" "<<sameNodeIndex<<endl;
        }
        else if(sameNodeIndex>=0){//old node
            if(sameNodeIndex==lastNodeIndex){//current node
                updateNode(N,sameNodeIndex);//update node position
                cout<<"current node: ("<<N.mapx<<","<<N.mapy<<" "<<(int)N.edgeNum<<") "<<mapNodes.size()<<" "<<sameNodeIndex<<endl;
            }
            else{//loop
                addEdge(sameNodeIndex, lastNodeIndex);
                g2oLoop();
                lastNodeIndex = sameNodeIndex;
                cout<<"loop node: ("<<N.mapx<<","<<N.mapy<<" "<<(int)N.edgeNum<<") "<<mapNodes.size()<<" "<<sameNodeIndex<<endl;
            }
        }
        else{//near position but different edge num
            if(N.edgeNum>mapNodes[sameNodeIndex].edgeNum && (-1000-sameNodeIndex)==mapNodes.size()){//current better than old, when enter landmark
                forceUpdateNode(N,sameNodeIndex);
                g2oLoop();
                cout<<"better node: ("<<N.mapx<<","<<N.mapy<<" "<<(int)N.edgeNum<<") "<<mapNodes.size()<<" "<<sameNodeIndex<<endl;
            }
            else{//current worse, when leave landmark, do nothing
                cout<<"worse node: ("<<N.mapx<<","<<N.mapy<<" "<<(int)N.edgeNum<<") "<<mapNodes.size()<<" "<<sameNodeIndex<<endl;
            }

        }

    }

    drawMap();
}

int TopoMapBuild::findNode(topoNodeDelaunay::FeaturePolygon& node)
{
    int nearIndex = -1;
    float minDis = poseDistanceTh;
    bool isSame = true;
    for(int i=0; i< mapNodes.size(); ++i){
        isSame = true;
        topoNodeDelaunay::FeaturePolygon N = mapNodes[i];
        float dis = sqrt((node.mapx-N.mapx)*(node.mapx-N.mapx) + (node.mapy-N.mapy)*(node.mapy-N.mapy));
        cout<<dis;
        if(dis<minDis){//check distance in map coor
            cout<<"!";
            minDis = dis;
            nearIndex = i;
            if(node.edgeNum == N.edgeNum){//check edgeNum
                if(node.goodEdgeNum == N.goodEdgeNum){
                    for(int j=0; j<node.feature.size(); j+=2){//roll the node, 123456 to 561234
                        for(int k=0; k<node.feature.size(); ++k){
                            int s = (j+k)%node.feature.size();
                            if(k%2==0){//edge
                                if(abs(node.feature[s]-N.feature[k])>edgeTh){
                                    isSame = false;
                                }
                            }
                            else{//angle
                                if(abs(node.feature[s]-N.feature[k])>angleTh){
                                    isSame = false;
                                }
                            }
                        }
                    }
                }

            }
        }
        else{
            isSame = false;
        }
        if(isSame==true){break;}
    }

    if(isSame){//find same node
        return nearIndex;
    }
    else if(nearIndex<0){//all node far
        return nearIndex;
    }
    else{//find near node but not same, return -1000-i
        return (-1000-nearIndex);
    }
}

int TopoMapBuild::addEdge(int index1, int index2)
{
    if(index1==index2){
        cout<<"edge with same node"<<endl;
    }
    else{
        MAPEDGE e;
        e.no1 = min(index1, index2);
        e.no2 = max(index1, index2);
        mapEdges.insert(e);
    }
}

void TopoMapBuild::updateNode(topoNodeDelaunay::FeaturePolygon& node, int index)
{
    typedef g2o::BlockSolver< g2o::BlockSolverTraits<-1,-1> > Block;
    Block::LinearSolverType * linearSolver = new g2o::LinearSolverCSparse<Block::PoseMatrixType>();
    auto* blockSolver = new Block(linearSolver);
    OptimizationAlgorithmGaussNewton* optimizationAlgorithm ;
    optimizationAlgorithm= new OptimizationAlgorithmGaussNewton(blockSolver);
    g2o::SparseOptimizer optimizer;
    optimizer.setAlgorithm(optimizationAlgorithm);
    optimizer.setVerbose(false);

    Eigen::Matrix3d covO;
    covO.fill(0.0);
    covO(0,0) = xNoise*xNoise;
    covO(1,1) = yNoise*yNoise;
    covO(2,2) = tNoise*tNoise;
    Eigen::Matrix3d infoO = covO.inverse();

    g2o::VertexSE2* robot1 = new g2o::VertexSE2;
    g2o::SE2 pose1(mapNodes[index].odomx, mapNodes[index].odomy, mapNodes[index].odomt);
    robot1->setId(0);
    robot1->setEstimate(pose1);
    robot1->setFixed(true);
    optimizer.addVertex(robot1);

    g2o::VertexSE2* robot2 = new g2o::VertexSE2;
    g2o::SE2 pose2(node.odomx, node.odomy, node.odomt);
    robot2->setId(1);
    robot2->setEstimate(pose2);
    optimizer.addVertex(robot2);

    g2o::EdgeSE2* odometry = new g2o::EdgeSE2;
    g2o::SE2 edge12 = pose1.inverse()*pose2;
    odometry->vertices()[0] = optimizer.vertex(0);
    odometry->vertices()[1] = optimizer.vertex(1);
    odometry->setInformation(infoO);
    odometry->setMeasurement(edge12);
    optimizer.addEdge(odometry);

    Eigen::Matrix2d covL;
    covL.fill(0.0);
    covL(0,0) = landmarkXNoise*landmarkXNoise;
    covL(1,1) = landmarkYNoise*landmarkYNoise;
    Eigen::Matrix2d infoL = covL.inverse();

    g2o::VertexPointXY* landmark = new g2o::VertexPointXY;
    Eigen::Vector2d landmarkPose(mapNodes[index].mapx, mapNodes[index].mapy);
    landmark->setId(2);
    landmark->setEstimate(landmarkPose);
    optimizer.addVertex(landmark);

    g2o::EdgeSE2PointXY* landmarkObservation1 = new g2o::EdgeSE2PointXY;
    //Eigen::Vector2d edge1 = pose1.inverse()*landmarkPose;
    landmarkObservation1->vertices()[0] = optimizer.vertex(0);
    landmarkObservation1->vertices()[1] = optimizer.vertex(2);
    landmarkObservation1->setInformation(infoL);
    landmarkObservation1->setMeasurement(Eigen::Vector2d(mapNodes[index].robotx,mapNodes[index].roboty));
    optimizer.addEdge(landmarkObservation1);

    g2o::EdgeSE2PointXY* landmarkObservation2 = new g2o::EdgeSE2PointXY;
    //Eigen::Vector2d edge2 = pose2.inverse()*landmarkPose;
    landmarkObservation2->vertices()[0] = optimizer.vertex(1);
    landmarkObservation2->vertices()[1] = optimizer.vertex(2);
    landmarkObservation2->setInformation(infoL);
    landmarkObservation2->setMeasurement(Eigen::Vector2d(node.robotx,node.roboty));
    optimizer.addEdge(landmarkObservation2);

    optimizer.setVerbose(true);
    optimizer.initializeOptimization();
    optimizer.optimize(10);


    double r_pose1[3],r_pose2[3],l_pose[3];
    optimizer.vertex(0)->getEstimateData(r_pose1);
    optimizer.vertex(1)->getEstimateData(r_pose2);
    optimizer.vertex(2)->getEstimateData(l_pose);
    cout<<" "<<mapNodes[index].odomx<<" "<<mapNodes[index].odomy
        <<" "<<r_pose1[0]<<" "<<r_pose1[1]<<endl
        <<" "<<node.odomx<<" "<<node.odomy
        <<" "<<r_pose2[0]<<" "<<r_pose2[1]<<endl
        <<" "<<mapNodes[index].mapx<<" "<<mapNodes[index].mapy
        <<" "<<l_pose[0]<<" "<<l_pose[1]<<endl
        <<endl;
    //mapNodes[index].odomx =

    optimizer.clear();
    //g2o::Factory::destroy();
    //g2o::OptimizationAlgorithmFactory::destroy();
    //g2o::HyperGraphActionLibrary::destroy();
}

void TopoMapBuild::forceUpdateNode(topoNodeDelaunay::FeaturePolygon& node, int index)
{

}

void TopoMapBuild::g2oLoop()
{
    /*BlockSolverX::LinearSolverType * linearSolver = new LinearSolverCSparse<BlockSolverX::PoseMatrixType>();
    BlockSolverX* blockSolver = new BlockSolverX(linearSolver);
    OptimizationAlgorithmGaussNewton* optimizationAlgorithm = new OptimizationAlgorithmGaussNewton(blockSolver);
    g2o::SparseOptimizer optimizer;
    optimizer.setAlgorithm(optimizationAlgorithm);

    Eigen::Matrix3d covO;
    covO.fill(0.0);
    covO(0,0) = xNoise*xNoise;
    covO(1,1) = yNoise*yNoise;
    covO(2,2) = tNoise*tNoise;
    Eigen::Matrix3d infoO = covO.inverse();

    for(int i=0; i<mapNodes.size(); ++i){
        VertexSE2* robot = new VertexSE2;
        SE2 pose(mapNodes[i].odomx, mapNodes[i].odomy, mapNodes[i].odomt);
        robot->setId(i);
        robot->setEstimate(pose);
        robot->setFixed(true);
        optimizer.addVertex(robot);
    }
    for(auto &e:mapEdges){
        EdgeSE2* odometry = new EdgeSE2;
        int from = e.no1;
        int to = e.no2;
        SE2 pose1(mapNodes[from].odomx, mapNodes[from].odomy, mapNodes[from].odomt);
        SE2 pose2(mapNodes[to].odomx, mapNodes[to].odomy, mapNodes[to].odomt);
        SE2 edge = pose1.inverse()*pose2;
        odometry->vertices()[0] = optimizer.vertex(from);
        odometry->vertices()[1] = optimizer.vertex(to);
        odometry->setInformation(infoO);
        odometry->setMeasurement(edge);
        optimizer.addEdge(odometry);
    }

    optimizer.setVerbose(true);
    optimizer.initializeOptimization();
    optimizer.optimize(10);

    optimizer.clear();
    Factory::destroy();
    OptimizationAlgorithmFactory::destroy();
    HyperGraphActionLibrary::destroy();
*/
}

void TopoMapBuild::drawMap()
{
    visualization_msgs::Marker topoNode;
    topoNode.header.frame_id = "odom";
    topoNode.header.stamp = ros::Time::now();
    topoNode.ns = "topoNode";
    topoNode.id = 0;
    topoNode.type = visualization_msgs::Marker::POINTS;
    topoNode.action = visualization_msgs::Marker::MODIFY;
    topoNode.scale.x = 0.5;
    topoNode.scale.y = 0.5;
    topoNode.color.b = 1.0;
    topoNode.color.a = 1.0;
    geometry_msgs::Point p;
    p.z = 0;
    topoNode.points.reserve(mapNodes.size());
    for(int i=0; i<mapNodes.size(); ++i){
        p.x = mapNodes[i].mapx;
        p.y = mapNodes[i].mapy;
        topoNode.points.push_back(p);
    }
    topoAllNodeShowPub.publish(topoNode);


    visualization_msgs::Marker topoEdge;
    topoEdge.header.frame_id = "odom";
    topoEdge.header.stamp = ros::Time::now();
    topoEdge.ns = "topoEdge";
    topoEdge.id = 0;
    topoEdge.type = visualization_msgs::Marker::LINE_LIST;
    topoEdge.action = visualization_msgs::Marker::MODIFY;
    topoEdge.scale.x = 0.05;
    topoEdge.color.b = 1.0;
    topoEdge.color.a = 1.0;
    topoEdge.points.reserve(mapEdges.size()*2);
    //int i=0;
    for(auto &edge:mapEdges){
        p.x = mapNodes[edge.no1].mapx;
        p.y = mapNodes[edge.no1].mapy;
        topoEdge.points.push_back(p);
        p.x = mapNodes[edge.no2].mapx;
        p.y = mapNodes[edge.no2].mapy;
        topoEdge.points.push_back(p);

    }
    topoAllEdgeShowPub.publish(topoEdge);
}

void TopoMapBuild::saveMap(string &mapFileName)
{
    fstream mapFile;
    mapFile.open(mapFileName, ios::out);
    mapFile << "NODE" << endl;
    for(int i=0; i<mapNodes.size(); ++i){
        topoNodeDelaunay::FeaturePolygon N = mapNodes[i];
        mapFile << i << "," << N.mapx << "," << N.mapy << ","
                << (int)N.edgeNum << "," << (int)N.goodEdgeNum;
        for(int j=0; j<N.feature.size(); ++j){
            mapFile << "," << N.feature[j];
        }
        mapFile << endl;
    }

    mapFile << "EDGE" << endl;
    for(auto &edge:mapEdges){
        mapFile << edge.no1 << "," << edge.no2 << endl;
    }

    mapFile << "FIN" << endl;
    mapFile.close();
}

void TopoMapBuild::saveMapCallback(const std_msgs::StringConstPtr& p)
{
    string s = p->data;
    saveMap(s);
}
