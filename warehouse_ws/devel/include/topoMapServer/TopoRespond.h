// Generated by gencpp from file topoMapServer/TopoRespond.msg
// DO NOT EDIT!


#ifndef TOPOMAPSERVER_MESSAGE_TOPORESPOND_H
#define TOPOMAPSERVER_MESSAGE_TOPORESPOND_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>

#include <std_msgs/Header.h>

namespace topoMapServer
{
template <class ContainerAllocator>
struct TopoRespond_
{
  typedef TopoRespond_<ContainerAllocator> Type;

  TopoRespond_()
    : header()
    , goalReached(0)
    , reachedGoalSeq(0)
    , errorNum(0)  {
    }
  TopoRespond_(const ContainerAllocator& _alloc)
    : header(_alloc)
    , goalReached(0)
    , reachedGoalSeq(0)
    , errorNum(0)  {
  (void)_alloc;
    }



   typedef  ::std_msgs::Header_<ContainerAllocator>  _header_type;
  _header_type header;

   typedef int32_t _goalReached_type;
  _goalReached_type goalReached;

   typedef int32_t _reachedGoalSeq_type;
  _reachedGoalSeq_type reachedGoalSeq;

   typedef int32_t _errorNum_type;
  _errorNum_type errorNum;




  typedef boost::shared_ptr< ::topoMapServer::TopoRespond_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::topoMapServer::TopoRespond_<ContainerAllocator> const> ConstPtr;

}; // struct TopoRespond_

typedef ::topoMapServer::TopoRespond_<std::allocator<void> > TopoRespond;

typedef boost::shared_ptr< ::topoMapServer::TopoRespond > TopoRespondPtr;
typedef boost::shared_ptr< ::topoMapServer::TopoRespond const> TopoRespondConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::topoMapServer::TopoRespond_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::topoMapServer::TopoRespond_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace topoMapServer

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': True}
// {'topoMapServer': ['/home/crti/warehouse_ros/warehouse_ws/src/topoMapServer/msg'], 'std_msgs': ['/opt/ros/lunar/share/std_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::topoMapServer::TopoRespond_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::topoMapServer::TopoRespond_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::topoMapServer::TopoRespond_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::topoMapServer::TopoRespond_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::topoMapServer::TopoRespond_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::topoMapServer::TopoRespond_<ContainerAllocator> const>
  : TrueType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::topoMapServer::TopoRespond_<ContainerAllocator> >
{
  static const char* value()
  {
    return "018e2107edb458b395b981e66546f526";
  }

  static const char* value(const ::topoMapServer::TopoRespond_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x018e2107edb458b3ULL;
  static const uint64_t static_value2 = 0x95b981e66546f526ULL;
};

template<class ContainerAllocator>
struct DataType< ::topoMapServer::TopoRespond_<ContainerAllocator> >
{
  static const char* value()
  {
    return "topoMapServer/TopoRespond";
  }

  static const char* value(const ::topoMapServer::TopoRespond_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::topoMapServer::TopoRespond_<ContainerAllocator> >
{
  static const char* value()
  {
    return "std_msgs/Header header\n\
int32 goalReached\n\
int32 reachedGoalSeq\n\
int32 errorNum\n\
================================================================================\n\
MSG: std_msgs/Header\n\
# Standard metadata for higher-level stamped data types.\n\
# This is generally used to communicate timestamped data \n\
# in a particular coordinate frame.\n\
# \n\
# sequence ID: consecutively increasing ID \n\
uint32 seq\n\
#Two-integer timestamp that is expressed as:\n\
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')\n\
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')\n\
# time-handling sugar is provided by the client library\n\
time stamp\n\
#Frame this data is associated with\n\
# 0: no frame\n\
# 1: global frame\n\
string frame_id\n\
";
  }

  static const char* value(const ::topoMapServer::TopoRespond_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::topoMapServer::TopoRespond_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.header);
      stream.next(m.goalReached);
      stream.next(m.reachedGoalSeq);
      stream.next(m.errorNum);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct TopoRespond_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::topoMapServer::TopoRespond_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::topoMapServer::TopoRespond_<ContainerAllocator>& v)
  {
    s << indent << "header: ";
    s << std::endl;
    Printer< ::std_msgs::Header_<ContainerAllocator> >::stream(s, indent + "  ", v.header);
    s << indent << "goalReached: ";
    Printer<int32_t>::stream(s, indent + "  ", v.goalReached);
    s << indent << "reachedGoalSeq: ";
    Printer<int32_t>::stream(s, indent + "  ", v.reachedGoalSeq);
    s << indent << "errorNum: ";
    Printer<int32_t>::stream(s, indent + "  ", v.errorNum);
  }
};

} // namespace message_operations
} // namespace ros

#endif // TOPOMAPSERVER_MESSAGE_TOPORESPOND_H
