// Generated by gencpp from file topoNodeDelaunay/FeaturePolygon.msg
// DO NOT EDIT!


#ifndef TOPONODEDELAUNAY_MESSAGE_FEATUREPOLYGON_H
#define TOPONODEDELAUNAY_MESSAGE_FEATUREPOLYGON_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace topoNodeDelaunay
{
template <class ContainerAllocator>
struct FeaturePolygon_
{
  typedef FeaturePolygon_<ContainerAllocator> Type;

  FeaturePolygon_()
    : edgeNum(0)
    , goodEdgeNum(0)
    , mapx(0.0)
    , mapy(0.0)
    , robotx(0.0)
    , roboty(0.0)
    , odomx(0.0)
    , odomy(0.0)
    , odomt(0.0)
    , feature()
    , pointx()
    , pointy()  {
    }
  FeaturePolygon_(const ContainerAllocator& _alloc)
    : edgeNum(0)
    , goodEdgeNum(0)
    , mapx(0.0)
    , mapy(0.0)
    , robotx(0.0)
    , roboty(0.0)
    , odomx(0.0)
    , odomy(0.0)
    , odomt(0.0)
    , feature(_alloc)
    , pointx(_alloc)
    , pointy(_alloc)  {
  (void)_alloc;
    }



   typedef uint8_t _edgeNum_type;
  _edgeNum_type edgeNum;

   typedef uint8_t _goodEdgeNum_type;
  _goodEdgeNum_type goodEdgeNum;

   typedef float _mapx_type;
  _mapx_type mapx;

   typedef float _mapy_type;
  _mapy_type mapy;

   typedef float _robotx_type;
  _robotx_type robotx;

   typedef float _roboty_type;
  _roboty_type roboty;

   typedef float _odomx_type;
  _odomx_type odomx;

   typedef float _odomy_type;
  _odomy_type odomy;

   typedef float _odomt_type;
  _odomt_type odomt;

   typedef std::vector<float, typename ContainerAllocator::template rebind<float>::other >  _feature_type;
  _feature_type feature;

   typedef std::vector<float, typename ContainerAllocator::template rebind<float>::other >  _pointx_type;
  _pointx_type pointx;

   typedef std::vector<float, typename ContainerAllocator::template rebind<float>::other >  _pointy_type;
  _pointy_type pointy;




  typedef boost::shared_ptr< ::topoNodeDelaunay::FeaturePolygon_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::topoNodeDelaunay::FeaturePolygon_<ContainerAllocator> const> ConstPtr;

}; // struct FeaturePolygon_

typedef ::topoNodeDelaunay::FeaturePolygon_<std::allocator<void> > FeaturePolygon;

typedef boost::shared_ptr< ::topoNodeDelaunay::FeaturePolygon > FeaturePolygonPtr;
typedef boost::shared_ptr< ::topoNodeDelaunay::FeaturePolygon const> FeaturePolygonConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::topoNodeDelaunay::FeaturePolygon_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::topoNodeDelaunay::FeaturePolygon_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace topoNodeDelaunay

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': False}
// {'topoNodeDelaunay': ['/home/crti/warehouse_ros/warehouse_ws/src/topoNodeDelaunay/msg'], 'std_msgs': ['/opt/ros/lunar/share/std_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::topoNodeDelaunay::FeaturePolygon_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::topoNodeDelaunay::FeaturePolygon_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::topoNodeDelaunay::FeaturePolygon_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::topoNodeDelaunay::FeaturePolygon_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::topoNodeDelaunay::FeaturePolygon_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::topoNodeDelaunay::FeaturePolygon_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::topoNodeDelaunay::FeaturePolygon_<ContainerAllocator> >
{
  static const char* value()
  {
    return "00732ac22662243c281801f4bb564f65";
  }

  static const char* value(const ::topoNodeDelaunay::FeaturePolygon_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x00732ac22662243cULL;
  static const uint64_t static_value2 = 0x281801f4bb564f65ULL;
};

template<class ContainerAllocator>
struct DataType< ::topoNodeDelaunay::FeaturePolygon_<ContainerAllocator> >
{
  static const char* value()
  {
    return "topoNodeDelaunay/FeaturePolygon";
  }

  static const char* value(const ::topoNodeDelaunay::FeaturePolygon_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::topoNodeDelaunay::FeaturePolygon_<ContainerAllocator> >
{
  static const char* value()
  {
    return "uint8 edgeNum\n\
uint8 goodEdgeNum\n\
float32 mapx\n\
float32 mapy\n\
float32 robotx\n\
float32 roboty\n\
float32 odomx\n\
float32 odomy\n\
float32 odomt\n\
float32[] feature\n\
float32[] pointx\n\
float32[] pointy\n\
";
  }

  static const char* value(const ::topoNodeDelaunay::FeaturePolygon_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::topoNodeDelaunay::FeaturePolygon_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.edgeNum);
      stream.next(m.goodEdgeNum);
      stream.next(m.mapx);
      stream.next(m.mapy);
      stream.next(m.robotx);
      stream.next(m.roboty);
      stream.next(m.odomx);
      stream.next(m.odomy);
      stream.next(m.odomt);
      stream.next(m.feature);
      stream.next(m.pointx);
      stream.next(m.pointy);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct FeaturePolygon_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::topoNodeDelaunay::FeaturePolygon_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::topoNodeDelaunay::FeaturePolygon_<ContainerAllocator>& v)
  {
    s << indent << "edgeNum: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.edgeNum);
    s << indent << "goodEdgeNum: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.goodEdgeNum);
    s << indent << "mapx: ";
    Printer<float>::stream(s, indent + "  ", v.mapx);
    s << indent << "mapy: ";
    Printer<float>::stream(s, indent + "  ", v.mapy);
    s << indent << "robotx: ";
    Printer<float>::stream(s, indent + "  ", v.robotx);
    s << indent << "roboty: ";
    Printer<float>::stream(s, indent + "  ", v.roboty);
    s << indent << "odomx: ";
    Printer<float>::stream(s, indent + "  ", v.odomx);
    s << indent << "odomy: ";
    Printer<float>::stream(s, indent + "  ", v.odomy);
    s << indent << "odomt: ";
    Printer<float>::stream(s, indent + "  ", v.odomt);
    s << indent << "feature[]" << std::endl;
    for (size_t i = 0; i < v.feature.size(); ++i)
    {
      s << indent << "  feature[" << i << "]: ";
      Printer<float>::stream(s, indent + "  ", v.feature[i]);
    }
    s << indent << "pointx[]" << std::endl;
    for (size_t i = 0; i < v.pointx.size(); ++i)
    {
      s << indent << "  pointx[" << i << "]: ";
      Printer<float>::stream(s, indent + "  ", v.pointx[i]);
    }
    s << indent << "pointy[]" << std::endl;
    for (size_t i = 0; i < v.pointy.size(); ++i)
    {
      s << indent << "  pointy[" << i << "]: ";
      Printer<float>::stream(s, indent + "  ", v.pointy[i]);
    }
  }
};

} // namespace message_operations
} // namespace ros

#endif // TOPONODEDELAUNAY_MESSAGE_FEATUREPOLYGON_H
