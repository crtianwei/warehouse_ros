// Auto-generated. Do not edit!

// (in-package topoNodeDelaunay.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class FeaturePolygon {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.edgeNum = null;
      this.goodEdgeNum = null;
      this.mapx = null;
      this.mapy = null;
      this.robotx = null;
      this.roboty = null;
      this.odomx = null;
      this.odomy = null;
      this.odomt = null;
      this.feature = null;
      this.pointx = null;
      this.pointy = null;
    }
    else {
      if (initObj.hasOwnProperty('edgeNum')) {
        this.edgeNum = initObj.edgeNum
      }
      else {
        this.edgeNum = 0;
      }
      if (initObj.hasOwnProperty('goodEdgeNum')) {
        this.goodEdgeNum = initObj.goodEdgeNum
      }
      else {
        this.goodEdgeNum = 0;
      }
      if (initObj.hasOwnProperty('mapx')) {
        this.mapx = initObj.mapx
      }
      else {
        this.mapx = 0.0;
      }
      if (initObj.hasOwnProperty('mapy')) {
        this.mapy = initObj.mapy
      }
      else {
        this.mapy = 0.0;
      }
      if (initObj.hasOwnProperty('robotx')) {
        this.robotx = initObj.robotx
      }
      else {
        this.robotx = 0.0;
      }
      if (initObj.hasOwnProperty('roboty')) {
        this.roboty = initObj.roboty
      }
      else {
        this.roboty = 0.0;
      }
      if (initObj.hasOwnProperty('odomx')) {
        this.odomx = initObj.odomx
      }
      else {
        this.odomx = 0.0;
      }
      if (initObj.hasOwnProperty('odomy')) {
        this.odomy = initObj.odomy
      }
      else {
        this.odomy = 0.0;
      }
      if (initObj.hasOwnProperty('odomt')) {
        this.odomt = initObj.odomt
      }
      else {
        this.odomt = 0.0;
      }
      if (initObj.hasOwnProperty('feature')) {
        this.feature = initObj.feature
      }
      else {
        this.feature = [];
      }
      if (initObj.hasOwnProperty('pointx')) {
        this.pointx = initObj.pointx
      }
      else {
        this.pointx = [];
      }
      if (initObj.hasOwnProperty('pointy')) {
        this.pointy = initObj.pointy
      }
      else {
        this.pointy = [];
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type FeaturePolygon
    // Serialize message field [edgeNum]
    bufferOffset = _serializer.uint8(obj.edgeNum, buffer, bufferOffset);
    // Serialize message field [goodEdgeNum]
    bufferOffset = _serializer.uint8(obj.goodEdgeNum, buffer, bufferOffset);
    // Serialize message field [mapx]
    bufferOffset = _serializer.float32(obj.mapx, buffer, bufferOffset);
    // Serialize message field [mapy]
    bufferOffset = _serializer.float32(obj.mapy, buffer, bufferOffset);
    // Serialize message field [robotx]
    bufferOffset = _serializer.float32(obj.robotx, buffer, bufferOffset);
    // Serialize message field [roboty]
    bufferOffset = _serializer.float32(obj.roboty, buffer, bufferOffset);
    // Serialize message field [odomx]
    bufferOffset = _serializer.float32(obj.odomx, buffer, bufferOffset);
    // Serialize message field [odomy]
    bufferOffset = _serializer.float32(obj.odomy, buffer, bufferOffset);
    // Serialize message field [odomt]
    bufferOffset = _serializer.float32(obj.odomt, buffer, bufferOffset);
    // Serialize message field [feature]
    bufferOffset = _arraySerializer.float32(obj.feature, buffer, bufferOffset, null);
    // Serialize message field [pointx]
    bufferOffset = _arraySerializer.float32(obj.pointx, buffer, bufferOffset, null);
    // Serialize message field [pointy]
    bufferOffset = _arraySerializer.float32(obj.pointy, buffer, bufferOffset, null);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type FeaturePolygon
    let len;
    let data = new FeaturePolygon(null);
    // Deserialize message field [edgeNum]
    data.edgeNum = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [goodEdgeNum]
    data.goodEdgeNum = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [mapx]
    data.mapx = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [mapy]
    data.mapy = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [robotx]
    data.robotx = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [roboty]
    data.roboty = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [odomx]
    data.odomx = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [odomy]
    data.odomy = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [odomt]
    data.odomt = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [feature]
    data.feature = _arrayDeserializer.float32(buffer, bufferOffset, null)
    // Deserialize message field [pointx]
    data.pointx = _arrayDeserializer.float32(buffer, bufferOffset, null)
    // Deserialize message field [pointy]
    data.pointy = _arrayDeserializer.float32(buffer, bufferOffset, null)
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += 4 * object.feature.length;
    length += 4 * object.pointx.length;
    length += 4 * object.pointy.length;
    return length + 42;
  }

  static datatype() {
    // Returns string type for a message object
    return 'topoNodeDelaunay/FeaturePolygon';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '00732ac22662243c281801f4bb564f65';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    uint8 edgeNum
    uint8 goodEdgeNum
    float32 mapx
    float32 mapy
    float32 robotx
    float32 roboty
    float32 odomx
    float32 odomy
    float32 odomt
    float32[] feature
    float32[] pointx
    float32[] pointy
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new FeaturePolygon(null);
    if (msg.edgeNum !== undefined) {
      resolved.edgeNum = msg.edgeNum;
    }
    else {
      resolved.edgeNum = 0
    }

    if (msg.goodEdgeNum !== undefined) {
      resolved.goodEdgeNum = msg.goodEdgeNum;
    }
    else {
      resolved.goodEdgeNum = 0
    }

    if (msg.mapx !== undefined) {
      resolved.mapx = msg.mapx;
    }
    else {
      resolved.mapx = 0.0
    }

    if (msg.mapy !== undefined) {
      resolved.mapy = msg.mapy;
    }
    else {
      resolved.mapy = 0.0
    }

    if (msg.robotx !== undefined) {
      resolved.robotx = msg.robotx;
    }
    else {
      resolved.robotx = 0.0
    }

    if (msg.roboty !== undefined) {
      resolved.roboty = msg.roboty;
    }
    else {
      resolved.roboty = 0.0
    }

    if (msg.odomx !== undefined) {
      resolved.odomx = msg.odomx;
    }
    else {
      resolved.odomx = 0.0
    }

    if (msg.odomy !== undefined) {
      resolved.odomy = msg.odomy;
    }
    else {
      resolved.odomy = 0.0
    }

    if (msg.odomt !== undefined) {
      resolved.odomt = msg.odomt;
    }
    else {
      resolved.odomt = 0.0
    }

    if (msg.feature !== undefined) {
      resolved.feature = msg.feature;
    }
    else {
      resolved.feature = []
    }

    if (msg.pointx !== undefined) {
      resolved.pointx = msg.pointx;
    }
    else {
      resolved.pointx = []
    }

    if (msg.pointy !== undefined) {
      resolved.pointy = msg.pointy;
    }
    else {
      resolved.pointy = []
    }

    return resolved;
    }
};

module.exports = FeaturePolygon;
