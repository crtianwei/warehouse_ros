// Auto-generated. Do not edit!

// (in-package topoMapServer.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class TopoTask {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.lastGoal = null;
      this.currentGoal = null;
      this.nextGoal = null;
      this.nextNextGoal = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('lastGoal')) {
        this.lastGoal = initObj.lastGoal
      }
      else {
        this.lastGoal = 0;
      }
      if (initObj.hasOwnProperty('currentGoal')) {
        this.currentGoal = initObj.currentGoal
      }
      else {
        this.currentGoal = 0;
      }
      if (initObj.hasOwnProperty('nextGoal')) {
        this.nextGoal = initObj.nextGoal
      }
      else {
        this.nextGoal = 0;
      }
      if (initObj.hasOwnProperty('nextNextGoal')) {
        this.nextNextGoal = initObj.nextNextGoal
      }
      else {
        this.nextNextGoal = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type TopoTask
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [lastGoal]
    bufferOffset = _serializer.int32(obj.lastGoal, buffer, bufferOffset);
    // Serialize message field [currentGoal]
    bufferOffset = _serializer.int32(obj.currentGoal, buffer, bufferOffset);
    // Serialize message field [nextGoal]
    bufferOffset = _serializer.int32(obj.nextGoal, buffer, bufferOffset);
    // Serialize message field [nextNextGoal]
    bufferOffset = _serializer.int32(obj.nextNextGoal, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type TopoTask
    let len;
    let data = new TopoTask(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [lastGoal]
    data.lastGoal = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [currentGoal]
    data.currentGoal = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [nextGoal]
    data.nextGoal = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [nextNextGoal]
    data.nextNextGoal = _deserializer.int32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    return length + 16;
  }

  static datatype() {
    // Returns string type for a message object
    return 'topoMapServer/TopoTask';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'ffb97a02a4885599661760b20720afb1';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    std_msgs/Header header
    int32 lastGoal
    int32 currentGoal
    int32 nextGoal
    int32 nextNextGoal
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    # 0: no frame
    # 1: global frame
    string frame_id
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new TopoTask(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.lastGoal !== undefined) {
      resolved.lastGoal = msg.lastGoal;
    }
    else {
      resolved.lastGoal = 0
    }

    if (msg.currentGoal !== undefined) {
      resolved.currentGoal = msg.currentGoal;
    }
    else {
      resolved.currentGoal = 0
    }

    if (msg.nextGoal !== undefined) {
      resolved.nextGoal = msg.nextGoal;
    }
    else {
      resolved.nextGoal = 0
    }

    if (msg.nextNextGoal !== undefined) {
      resolved.nextNextGoal = msg.nextNextGoal;
    }
    else {
      resolved.nextNextGoal = 0
    }

    return resolved;
    }
};

module.exports = TopoTask;
