// Auto-generated. Do not edit!

// (in-package topoMapServer.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class TopoRespond {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.goalReached = null;
      this.reachedGoalSeq = null;
      this.errorNum = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('goalReached')) {
        this.goalReached = initObj.goalReached
      }
      else {
        this.goalReached = 0;
      }
      if (initObj.hasOwnProperty('reachedGoalSeq')) {
        this.reachedGoalSeq = initObj.reachedGoalSeq
      }
      else {
        this.reachedGoalSeq = 0;
      }
      if (initObj.hasOwnProperty('errorNum')) {
        this.errorNum = initObj.errorNum
      }
      else {
        this.errorNum = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type TopoRespond
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [goalReached]
    bufferOffset = _serializer.int32(obj.goalReached, buffer, bufferOffset);
    // Serialize message field [reachedGoalSeq]
    bufferOffset = _serializer.int32(obj.reachedGoalSeq, buffer, bufferOffset);
    // Serialize message field [errorNum]
    bufferOffset = _serializer.int32(obj.errorNum, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type TopoRespond
    let len;
    let data = new TopoRespond(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [goalReached]
    data.goalReached = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [reachedGoalSeq]
    data.reachedGoalSeq = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [errorNum]
    data.errorNum = _deserializer.int32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    return length + 12;
  }

  static datatype() {
    // Returns string type for a message object
    return 'topoMapServer/TopoRespond';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '018e2107edb458b395b981e66546f526';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    std_msgs/Header header
    int32 goalReached
    int32 reachedGoalSeq
    int32 errorNum
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    # 0: no frame
    # 1: global frame
    string frame_id
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new TopoRespond(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.goalReached !== undefined) {
      resolved.goalReached = msg.goalReached;
    }
    else {
      resolved.goalReached = 0
    }

    if (msg.reachedGoalSeq !== undefined) {
      resolved.reachedGoalSeq = msg.reachedGoalSeq;
    }
    else {
      resolved.reachedGoalSeq = 0
    }

    if (msg.errorNum !== undefined) {
      resolved.errorNum = msg.errorNum;
    }
    else {
      resolved.errorNum = 0
    }

    return resolved;
    }
};

module.exports = TopoRespond;
