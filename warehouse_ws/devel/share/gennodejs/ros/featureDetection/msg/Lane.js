// Auto-generated. Do not edit!

// (in-package featureDetection.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class Lane {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.leftDistance = null;
      this.leftTheta = null;
      this.rightDistance = null;
      this.rightTheta = null;
      this.frontDistance = null;
      this.frontTheta = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('leftDistance')) {
        this.leftDistance = initObj.leftDistance
      }
      else {
        this.leftDistance = 0.0;
      }
      if (initObj.hasOwnProperty('leftTheta')) {
        this.leftTheta = initObj.leftTheta
      }
      else {
        this.leftTheta = 0.0;
      }
      if (initObj.hasOwnProperty('rightDistance')) {
        this.rightDistance = initObj.rightDistance
      }
      else {
        this.rightDistance = 0.0;
      }
      if (initObj.hasOwnProperty('rightTheta')) {
        this.rightTheta = initObj.rightTheta
      }
      else {
        this.rightTheta = 0.0;
      }
      if (initObj.hasOwnProperty('frontDistance')) {
        this.frontDistance = initObj.frontDistance
      }
      else {
        this.frontDistance = 0.0;
      }
      if (initObj.hasOwnProperty('frontTheta')) {
        this.frontTheta = initObj.frontTheta
      }
      else {
        this.frontTheta = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type Lane
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [leftDistance]
    bufferOffset = _serializer.float32(obj.leftDistance, buffer, bufferOffset);
    // Serialize message field [leftTheta]
    bufferOffset = _serializer.float32(obj.leftTheta, buffer, bufferOffset);
    // Serialize message field [rightDistance]
    bufferOffset = _serializer.float32(obj.rightDistance, buffer, bufferOffset);
    // Serialize message field [rightTheta]
    bufferOffset = _serializer.float32(obj.rightTheta, buffer, bufferOffset);
    // Serialize message field [frontDistance]
    bufferOffset = _serializer.float32(obj.frontDistance, buffer, bufferOffset);
    // Serialize message field [frontTheta]
    bufferOffset = _serializer.float32(obj.frontTheta, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type Lane
    let len;
    let data = new Lane(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [leftDistance]
    data.leftDistance = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [leftTheta]
    data.leftTheta = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [rightDistance]
    data.rightDistance = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [rightTheta]
    data.rightTheta = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [frontDistance]
    data.frontDistance = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [frontTheta]
    data.frontTheta = _deserializer.float32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    return length + 24;
  }

  static datatype() {
    // Returns string type for a message object
    return 'featureDetection/Lane';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'fad9ce0686203b1ef1587aafa5f013d3';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    std_msgs/Header header
    float32 leftDistance
    float32 leftTheta
    float32 rightDistance
    float32 rightTheta
    float32 frontDistance
    float32 frontTheta
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    # 0: no frame
    # 1: global frame
    string frame_id
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new Lane(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.leftDistance !== undefined) {
      resolved.leftDistance = msg.leftDistance;
    }
    else {
      resolved.leftDistance = 0.0
    }

    if (msg.leftTheta !== undefined) {
      resolved.leftTheta = msg.leftTheta;
    }
    else {
      resolved.leftTheta = 0.0
    }

    if (msg.rightDistance !== undefined) {
      resolved.rightDistance = msg.rightDistance;
    }
    else {
      resolved.rightDistance = 0.0
    }

    if (msg.rightTheta !== undefined) {
      resolved.rightTheta = msg.rightTheta;
    }
    else {
      resolved.rightTheta = 0.0
    }

    if (msg.frontDistance !== undefined) {
      resolved.frontDistance = msg.frontDistance;
    }
    else {
      resolved.frontDistance = 0.0
    }

    if (msg.frontTheta !== undefined) {
      resolved.frontTheta = msg.frontTheta;
    }
    else {
      resolved.frontTheta = 0.0
    }

    return resolved;
    }
};

module.exports = Lane;
