;; Auto-generated. Do not edit!


(when (boundp 'topoNodeDelaunay::FeaturePolygon)
  (if (not (find-package "TOPONODEDELAUNAY"))
    (make-package "TOPONODEDELAUNAY"))
  (shadow 'FeaturePolygon (find-package "TOPONODEDELAUNAY")))
(unless (find-package "TOPONODEDELAUNAY::FEATUREPOLYGON")
  (make-package "TOPONODEDELAUNAY::FEATUREPOLYGON"))

(in-package "ROS")
;;//! \htmlinclude FeaturePolygon.msg.html


(defclass topoNodeDelaunay::FeaturePolygon
  :super ros::object
  :slots (_edgeNum _goodEdgeNum _mapx _mapy _robotx _roboty _odomx _odomy _odomt _feature _pointx _pointy ))

(defmethod topoNodeDelaunay::FeaturePolygon
  (:init
   (&key
    ((:edgeNum __edgeNum) 0)
    ((:goodEdgeNum __goodEdgeNum) 0)
    ((:mapx __mapx) 0.0)
    ((:mapy __mapy) 0.0)
    ((:robotx __robotx) 0.0)
    ((:roboty __roboty) 0.0)
    ((:odomx __odomx) 0.0)
    ((:odomy __odomy) 0.0)
    ((:odomt __odomt) 0.0)
    ((:feature __feature) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:pointx __pointx) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:pointy __pointy) (make-array 0 :initial-element 0.0 :element-type :float))
    )
   (send-super :init)
   (setq _edgeNum (round __edgeNum))
   (setq _goodEdgeNum (round __goodEdgeNum))
   (setq _mapx (float __mapx))
   (setq _mapy (float __mapy))
   (setq _robotx (float __robotx))
   (setq _roboty (float __roboty))
   (setq _odomx (float __odomx))
   (setq _odomy (float __odomy))
   (setq _odomt (float __odomt))
   (setq _feature __feature)
   (setq _pointx __pointx)
   (setq _pointy __pointy)
   self)
  (:edgeNum
   (&optional __edgeNum)
   (if __edgeNum (setq _edgeNum __edgeNum)) _edgeNum)
  (:goodEdgeNum
   (&optional __goodEdgeNum)
   (if __goodEdgeNum (setq _goodEdgeNum __goodEdgeNum)) _goodEdgeNum)
  (:mapx
   (&optional __mapx)
   (if __mapx (setq _mapx __mapx)) _mapx)
  (:mapy
   (&optional __mapy)
   (if __mapy (setq _mapy __mapy)) _mapy)
  (:robotx
   (&optional __robotx)
   (if __robotx (setq _robotx __robotx)) _robotx)
  (:roboty
   (&optional __roboty)
   (if __roboty (setq _roboty __roboty)) _roboty)
  (:odomx
   (&optional __odomx)
   (if __odomx (setq _odomx __odomx)) _odomx)
  (:odomy
   (&optional __odomy)
   (if __odomy (setq _odomy __odomy)) _odomy)
  (:odomt
   (&optional __odomt)
   (if __odomt (setq _odomt __odomt)) _odomt)
  (:feature
   (&optional __feature)
   (if __feature (setq _feature __feature)) _feature)
  (:pointx
   (&optional __pointx)
   (if __pointx (setq _pointx __pointx)) _pointx)
  (:pointy
   (&optional __pointy)
   (if __pointy (setq _pointy __pointy)) _pointy)
  (:serialization-length
   ()
   (+
    ;; uint8 _edgeNum
    1
    ;; uint8 _goodEdgeNum
    1
    ;; float32 _mapx
    4
    ;; float32 _mapy
    4
    ;; float32 _robotx
    4
    ;; float32 _roboty
    4
    ;; float32 _odomx
    4
    ;; float32 _odomy
    4
    ;; float32 _odomt
    4
    ;; float32[] _feature
    (* 4    (length _feature)) 4
    ;; float32[] _pointx
    (* 4    (length _pointx)) 4
    ;; float32[] _pointy
    (* 4    (length _pointy)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; uint8 _edgeNum
       (write-byte _edgeNum s)
     ;; uint8 _goodEdgeNum
       (write-byte _goodEdgeNum s)
     ;; float32 _mapx
       (sys::poke _mapx (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _mapy
       (sys::poke _mapy (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _robotx
       (sys::poke _robotx (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _roboty
       (sys::poke _roboty (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _odomx
       (sys::poke _odomx (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _odomy
       (sys::poke _odomy (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _odomt
       (sys::poke _odomt (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32[] _feature
     (write-long (length _feature) s)
     (dotimes (i (length _feature))
       (sys::poke (elt _feature i) (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
       )
     ;; float32[] _pointx
     (write-long (length _pointx) s)
     (dotimes (i (length _pointx))
       (sys::poke (elt _pointx i) (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
       )
     ;; float32[] _pointy
     (write-long (length _pointy) s)
     (dotimes (i (length _pointy))
       (sys::poke (elt _pointy i) (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; uint8 _edgeNum
     (setq _edgeNum (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _goodEdgeNum
     (setq _goodEdgeNum (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; float32 _mapx
     (setq _mapx (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _mapy
     (setq _mapy (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _robotx
     (setq _robotx (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _roboty
     (setq _roboty (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _odomx
     (setq _odomx (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _odomy
     (setq _odomy (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _odomt
     (setq _odomt (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32[] _feature
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _feature (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _feature i) (sys::peek buf ptr- :float)) (incf ptr- 4)
     ))
   ;; float32[] _pointx
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _pointx (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _pointx i) (sys::peek buf ptr- :float)) (incf ptr- 4)
     ))
   ;; float32[] _pointy
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _pointy (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _pointy i) (sys::peek buf ptr- :float)) (incf ptr- 4)
     ))
   ;;
   self)
  )

(setf (get topoNodeDelaunay::FeaturePolygon :md5sum-) "00732ac22662243c281801f4bb564f65")
(setf (get topoNodeDelaunay::FeaturePolygon :datatype-) "topoNodeDelaunay/FeaturePolygon")
(setf (get topoNodeDelaunay::FeaturePolygon :definition-)
      "uint8 edgeNum
uint8 goodEdgeNum
float32 mapx
float32 mapy
float32 robotx
float32 roboty
float32 odomx
float32 odomy
float32 odomt
float32[] feature
float32[] pointx
float32[] pointy

")



(provide :topoNodeDelaunay/FeaturePolygon "00732ac22662243c281801f4bb564f65")


