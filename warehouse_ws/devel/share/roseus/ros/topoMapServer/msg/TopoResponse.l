;; Auto-generated. Do not edit!


(when (boundp 'topoMapServer::TopoResponse)
  (if (not (find-package "TOPOMAPSERVER"))
    (make-package "TOPOMAPSERVER"))
  (shadow 'TopoResponse (find-package "TOPOMAPSERVER")))
(unless (find-package "TOPOMAPSERVER::TOPORESPONSE")
  (make-package "TOPOMAPSERVER::TOPORESPONSE"))

(in-package "ROS")
;;//! \htmlinclude TopoResponse.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass topoMapServer::TopoResponse
  :super ros::object
  :slots (_header _goalReached _reachedGoalSeq _errorNum ))

(defmethod topoMapServer::TopoResponse
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:goalReached __goalReached) 0)
    ((:reachedGoalSeq __reachedGoalSeq) 0)
    ((:errorNum __errorNum) 0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _goalReached (round __goalReached))
   (setq _reachedGoalSeq (round __reachedGoalSeq))
   (setq _errorNum (round __errorNum))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:goalReached
   (&optional __goalReached)
   (if __goalReached (setq _goalReached __goalReached)) _goalReached)
  (:reachedGoalSeq
   (&optional __reachedGoalSeq)
   (if __reachedGoalSeq (setq _reachedGoalSeq __reachedGoalSeq)) _reachedGoalSeq)
  (:errorNum
   (&optional __errorNum)
   (if __errorNum (setq _errorNum __errorNum)) _errorNum)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; int32 _goalReached
    4
    ;; int32 _reachedGoalSeq
    4
    ;; int32 _errorNum
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; int32 _goalReached
       (write-long _goalReached s)
     ;; int32 _reachedGoalSeq
       (write-long _reachedGoalSeq s)
     ;; int32 _errorNum
       (write-long _errorNum s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; int32 _goalReached
     (setq _goalReached (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _reachedGoalSeq
     (setq _reachedGoalSeq (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _errorNum
     (setq _errorNum (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get topoMapServer::TopoResponse :md5sum-) "018e2107edb458b395b981e66546f526")
(setf (get topoMapServer::TopoResponse :datatype-) "topoMapServer/TopoResponse")
(setf (get topoMapServer::TopoResponse :definition-)
      "std_msgs/Header header
int32 goalReached
int32 reachedGoalSeq
int32 errorNum
================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

")



(provide :topoMapServer/TopoResponse "018e2107edb458b395b981e66546f526")


