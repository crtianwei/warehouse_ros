;; Auto-generated. Do not edit!


(when (boundp 'topoMapServer::TopoTask)
  (if (not (find-package "TOPOMAPSERVER"))
    (make-package "TOPOMAPSERVER"))
  (shadow 'TopoTask (find-package "TOPOMAPSERVER")))
(unless (find-package "TOPOMAPSERVER::TOPOTASK")
  (make-package "TOPOMAPSERVER::TOPOTASK"))

(in-package "ROS")
;;//! \htmlinclude TopoTask.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass topoMapServer::TopoTask
  :super ros::object
  :slots (_header _lastGoal _currentGoal _nextGoal _nextNextGoal ))

(defmethod topoMapServer::TopoTask
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:lastGoal __lastGoal) 0)
    ((:currentGoal __currentGoal) 0)
    ((:nextGoal __nextGoal) 0)
    ((:nextNextGoal __nextNextGoal) 0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _lastGoal (round __lastGoal))
   (setq _currentGoal (round __currentGoal))
   (setq _nextGoal (round __nextGoal))
   (setq _nextNextGoal (round __nextNextGoal))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:lastGoal
   (&optional __lastGoal)
   (if __lastGoal (setq _lastGoal __lastGoal)) _lastGoal)
  (:currentGoal
   (&optional __currentGoal)
   (if __currentGoal (setq _currentGoal __currentGoal)) _currentGoal)
  (:nextGoal
   (&optional __nextGoal)
   (if __nextGoal (setq _nextGoal __nextGoal)) _nextGoal)
  (:nextNextGoal
   (&optional __nextNextGoal)
   (if __nextNextGoal (setq _nextNextGoal __nextNextGoal)) _nextNextGoal)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; int32 _lastGoal
    4
    ;; int32 _currentGoal
    4
    ;; int32 _nextGoal
    4
    ;; int32 _nextNextGoal
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; int32 _lastGoal
       (write-long _lastGoal s)
     ;; int32 _currentGoal
       (write-long _currentGoal s)
     ;; int32 _nextGoal
       (write-long _nextGoal s)
     ;; int32 _nextNextGoal
       (write-long _nextNextGoal s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; int32 _lastGoal
     (setq _lastGoal (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _currentGoal
     (setq _currentGoal (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _nextGoal
     (setq _nextGoal (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _nextNextGoal
     (setq _nextNextGoal (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get topoMapServer::TopoTask :md5sum-) "ffb97a02a4885599661760b20720afb1")
(setf (get topoMapServer::TopoTask :datatype-) "topoMapServer/TopoTask")
(setf (get topoMapServer::TopoTask :definition-)
      "std_msgs/Header header
int32 lastGoal
int32 currentGoal
int32 nextGoal
int32 nextNextGoal
================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

")



(provide :topoMapServer/TopoTask "ffb97a02a4885599661760b20720afb1")


