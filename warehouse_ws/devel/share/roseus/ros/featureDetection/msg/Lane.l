;; Auto-generated. Do not edit!


(when (boundp 'featureDetection::Lane)
  (if (not (find-package "FEATUREDETECTION"))
    (make-package "FEATUREDETECTION"))
  (shadow 'Lane (find-package "FEATUREDETECTION")))
(unless (find-package "FEATUREDETECTION::LANE")
  (make-package "FEATUREDETECTION::LANE"))

(in-package "ROS")
;;//! \htmlinclude Lane.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass featureDetection::Lane
  :super ros::object
  :slots (_header _leftDistance _leftTheta _rightDistance _rightTheta _frontDistance _frontTheta ))

(defmethod featureDetection::Lane
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:leftDistance __leftDistance) 0.0)
    ((:leftTheta __leftTheta) 0.0)
    ((:rightDistance __rightDistance) 0.0)
    ((:rightTheta __rightTheta) 0.0)
    ((:frontDistance __frontDistance) 0.0)
    ((:frontTheta __frontTheta) 0.0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _leftDistance (float __leftDistance))
   (setq _leftTheta (float __leftTheta))
   (setq _rightDistance (float __rightDistance))
   (setq _rightTheta (float __rightTheta))
   (setq _frontDistance (float __frontDistance))
   (setq _frontTheta (float __frontTheta))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:leftDistance
   (&optional __leftDistance)
   (if __leftDistance (setq _leftDistance __leftDistance)) _leftDistance)
  (:leftTheta
   (&optional __leftTheta)
   (if __leftTheta (setq _leftTheta __leftTheta)) _leftTheta)
  (:rightDistance
   (&optional __rightDistance)
   (if __rightDistance (setq _rightDistance __rightDistance)) _rightDistance)
  (:rightTheta
   (&optional __rightTheta)
   (if __rightTheta (setq _rightTheta __rightTheta)) _rightTheta)
  (:frontDistance
   (&optional __frontDistance)
   (if __frontDistance (setq _frontDistance __frontDistance)) _frontDistance)
  (:frontTheta
   (&optional __frontTheta)
   (if __frontTheta (setq _frontTheta __frontTheta)) _frontTheta)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; float32 _leftDistance
    4
    ;; float32 _leftTheta
    4
    ;; float32 _rightDistance
    4
    ;; float32 _rightTheta
    4
    ;; float32 _frontDistance
    4
    ;; float32 _frontTheta
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; float32 _leftDistance
       (sys::poke _leftDistance (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _leftTheta
       (sys::poke _leftTheta (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _rightDistance
       (sys::poke _rightDistance (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _rightTheta
       (sys::poke _rightTheta (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _frontDistance
       (sys::poke _frontDistance (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _frontTheta
       (sys::poke _frontTheta (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; float32 _leftDistance
     (setq _leftDistance (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _leftTheta
     (setq _leftTheta (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _rightDistance
     (setq _rightDistance (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _rightTheta
     (setq _rightTheta (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _frontDistance
     (setq _frontDistance (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _frontTheta
     (setq _frontTheta (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get featureDetection::Lane :md5sum-) "fad9ce0686203b1ef1587aafa5f013d3")
(setf (get featureDetection::Lane :datatype-) "featureDetection/Lane")
(setf (get featureDetection::Lane :definition-)
      "std_msgs/Header header
float32 leftDistance
float32 leftTheta
float32 rightDistance
float32 rightTheta
float32 frontDistance
float32 frontTheta
================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

")



(provide :featureDetection/Lane "fad9ce0686203b1ef1587aafa5f013d3")


