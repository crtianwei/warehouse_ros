(cl:in-package featureDetection-msg)
(cl:export '(HEADER-VAL
          HEADER
          LEFTDISTANCE-VAL
          LEFTDISTANCE
          LEFTTHETA-VAL
          LEFTTHETA
          RIGHTDISTANCE-VAL
          RIGHTDISTANCE
          RIGHTTHETA-VAL
          RIGHTTHETA
          FRONTDISTANCE-VAL
          FRONTDISTANCE
          FRONTTHETA-VAL
          FRONTTHETA
))