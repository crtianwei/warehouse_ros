; Auto-generated. Do not edit!


(cl:in-package topoNodeDelaunay-msg)


;//! \htmlinclude FeaturePolygon.msg.html

(cl:defclass <FeaturePolygon> (roslisp-msg-protocol:ros-message)
  ((edgeNum
    :reader edgeNum
    :initarg :edgeNum
    :type cl:fixnum
    :initform 0)
   (goodEdgeNum
    :reader goodEdgeNum
    :initarg :goodEdgeNum
    :type cl:fixnum
    :initform 0)
   (mapx
    :reader mapx
    :initarg :mapx
    :type cl:float
    :initform 0.0)
   (mapy
    :reader mapy
    :initarg :mapy
    :type cl:float
    :initform 0.0)
   (robotx
    :reader robotx
    :initarg :robotx
    :type cl:float
    :initform 0.0)
   (roboty
    :reader roboty
    :initarg :roboty
    :type cl:float
    :initform 0.0)
   (odomx
    :reader odomx
    :initarg :odomx
    :type cl:float
    :initform 0.0)
   (odomy
    :reader odomy
    :initarg :odomy
    :type cl:float
    :initform 0.0)
   (odomt
    :reader odomt
    :initarg :odomt
    :type cl:float
    :initform 0.0)
   (feature
    :reader feature
    :initarg :feature
    :type (cl:vector cl:float)
   :initform (cl:make-array 0 :element-type 'cl:float :initial-element 0.0))
   (pointx
    :reader pointx
    :initarg :pointx
    :type (cl:vector cl:float)
   :initform (cl:make-array 0 :element-type 'cl:float :initial-element 0.0))
   (pointy
    :reader pointy
    :initarg :pointy
    :type (cl:vector cl:float)
   :initform (cl:make-array 0 :element-type 'cl:float :initial-element 0.0)))
)

(cl:defclass FeaturePolygon (<FeaturePolygon>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <FeaturePolygon>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'FeaturePolygon)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name topoNodeDelaunay-msg:<FeaturePolygon> is deprecated: use topoNodeDelaunay-msg:FeaturePolygon instead.")))

(cl:ensure-generic-function 'edgeNum-val :lambda-list '(m))
(cl:defmethod edgeNum-val ((m <FeaturePolygon>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader topoNodeDelaunay-msg:edgeNum-val is deprecated.  Use topoNodeDelaunay-msg:edgeNum instead.")
  (edgeNum m))

(cl:ensure-generic-function 'goodEdgeNum-val :lambda-list '(m))
(cl:defmethod goodEdgeNum-val ((m <FeaturePolygon>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader topoNodeDelaunay-msg:goodEdgeNum-val is deprecated.  Use topoNodeDelaunay-msg:goodEdgeNum instead.")
  (goodEdgeNum m))

(cl:ensure-generic-function 'mapx-val :lambda-list '(m))
(cl:defmethod mapx-val ((m <FeaturePolygon>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader topoNodeDelaunay-msg:mapx-val is deprecated.  Use topoNodeDelaunay-msg:mapx instead.")
  (mapx m))

(cl:ensure-generic-function 'mapy-val :lambda-list '(m))
(cl:defmethod mapy-val ((m <FeaturePolygon>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader topoNodeDelaunay-msg:mapy-val is deprecated.  Use topoNodeDelaunay-msg:mapy instead.")
  (mapy m))

(cl:ensure-generic-function 'robotx-val :lambda-list '(m))
(cl:defmethod robotx-val ((m <FeaturePolygon>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader topoNodeDelaunay-msg:robotx-val is deprecated.  Use topoNodeDelaunay-msg:robotx instead.")
  (robotx m))

(cl:ensure-generic-function 'roboty-val :lambda-list '(m))
(cl:defmethod roboty-val ((m <FeaturePolygon>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader topoNodeDelaunay-msg:roboty-val is deprecated.  Use topoNodeDelaunay-msg:roboty instead.")
  (roboty m))

(cl:ensure-generic-function 'odomx-val :lambda-list '(m))
(cl:defmethod odomx-val ((m <FeaturePolygon>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader topoNodeDelaunay-msg:odomx-val is deprecated.  Use topoNodeDelaunay-msg:odomx instead.")
  (odomx m))

(cl:ensure-generic-function 'odomy-val :lambda-list '(m))
(cl:defmethod odomy-val ((m <FeaturePolygon>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader topoNodeDelaunay-msg:odomy-val is deprecated.  Use topoNodeDelaunay-msg:odomy instead.")
  (odomy m))

(cl:ensure-generic-function 'odomt-val :lambda-list '(m))
(cl:defmethod odomt-val ((m <FeaturePolygon>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader topoNodeDelaunay-msg:odomt-val is deprecated.  Use topoNodeDelaunay-msg:odomt instead.")
  (odomt m))

(cl:ensure-generic-function 'feature-val :lambda-list '(m))
(cl:defmethod feature-val ((m <FeaturePolygon>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader topoNodeDelaunay-msg:feature-val is deprecated.  Use topoNodeDelaunay-msg:feature instead.")
  (feature m))

(cl:ensure-generic-function 'pointx-val :lambda-list '(m))
(cl:defmethod pointx-val ((m <FeaturePolygon>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader topoNodeDelaunay-msg:pointx-val is deprecated.  Use topoNodeDelaunay-msg:pointx instead.")
  (pointx m))

(cl:ensure-generic-function 'pointy-val :lambda-list '(m))
(cl:defmethod pointy-val ((m <FeaturePolygon>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader topoNodeDelaunay-msg:pointy-val is deprecated.  Use topoNodeDelaunay-msg:pointy instead.")
  (pointy m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <FeaturePolygon>) ostream)
  "Serializes a message object of type '<FeaturePolygon>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'edgeNum)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'goodEdgeNum)) ostream)
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'mapx))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'mapy))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'robotx))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'roboty))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'odomx))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'odomy))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'odomt))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'feature))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-single-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)))
   (cl:slot-value msg 'feature))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'pointx))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-single-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)))
   (cl:slot-value msg 'pointx))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'pointy))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-single-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)))
   (cl:slot-value msg 'pointy))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <FeaturePolygon>) istream)
  "Deserializes a message object of type '<FeaturePolygon>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'edgeNum)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'goodEdgeNum)) (cl:read-byte istream))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'mapx) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'mapy) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'robotx) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'roboty) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'odomx) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'odomy) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'odomt) (roslisp-utils:decode-single-float-bits bits)))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'feature) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'feature)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-single-float-bits bits))))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'pointx) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'pointx)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-single-float-bits bits))))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'pointy) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'pointy)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-single-float-bits bits))))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<FeaturePolygon>)))
  "Returns string type for a message object of type '<FeaturePolygon>"
  "topoNodeDelaunay/FeaturePolygon")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'FeaturePolygon)))
  "Returns string type for a message object of type 'FeaturePolygon"
  "topoNodeDelaunay/FeaturePolygon")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<FeaturePolygon>)))
  "Returns md5sum for a message object of type '<FeaturePolygon>"
  "00732ac22662243c281801f4bb564f65")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'FeaturePolygon)))
  "Returns md5sum for a message object of type 'FeaturePolygon"
  "00732ac22662243c281801f4bb564f65")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<FeaturePolygon>)))
  "Returns full string definition for message of type '<FeaturePolygon>"
  (cl:format cl:nil "uint8 edgeNum~%uint8 goodEdgeNum~%float32 mapx~%float32 mapy~%float32 robotx~%float32 roboty~%float32 odomx~%float32 odomy~%float32 odomt~%float32[] feature~%float32[] pointx~%float32[] pointy~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'FeaturePolygon)))
  "Returns full string definition for message of type 'FeaturePolygon"
  (cl:format cl:nil "uint8 edgeNum~%uint8 goodEdgeNum~%float32 mapx~%float32 mapy~%float32 robotx~%float32 roboty~%float32 odomx~%float32 odomy~%float32 odomt~%float32[] feature~%float32[] pointx~%float32[] pointy~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <FeaturePolygon>))
  (cl:+ 0
     1
     1
     4
     4
     4
     4
     4
     4
     4
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'feature) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 4)))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'pointx) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 4)))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'pointy) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 4)))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <FeaturePolygon>))
  "Converts a ROS message object to a list"
  (cl:list 'FeaturePolygon
    (cl:cons ':edgeNum (edgeNum msg))
    (cl:cons ':goodEdgeNum (goodEdgeNum msg))
    (cl:cons ':mapx (mapx msg))
    (cl:cons ':mapy (mapy msg))
    (cl:cons ':robotx (robotx msg))
    (cl:cons ':roboty (roboty msg))
    (cl:cons ':odomx (odomx msg))
    (cl:cons ':odomy (odomy msg))
    (cl:cons ':odomt (odomt msg))
    (cl:cons ':feature (feature msg))
    (cl:cons ':pointx (pointx msg))
    (cl:cons ':pointy (pointy msg))
))
