(cl:in-package topoNodeDelaunay-msg)
(cl:export '(EDGENUM-VAL
          EDGENUM
          GOODEDGENUM-VAL
          GOODEDGENUM
          MAPX-VAL
          MAPX
          MAPY-VAL
          MAPY
          ROBOTX-VAL
          ROBOTX
          ROBOTY-VAL
          ROBOTY
          ODOMX-VAL
          ODOMX
          ODOMY-VAL
          ODOMY
          ODOMT-VAL
          ODOMT
          FEATURE-VAL
          FEATURE
          POINTX-VAL
          POINTX
          POINTY-VAL
          POINTY
))