; Auto-generated. Do not edit!


(cl:in-package topoMapServer-msg)


;//! \htmlinclude TopoTask.msg.html

(cl:defclass <TopoTask> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (lastGoal
    :reader lastGoal
    :initarg :lastGoal
    :type cl:integer
    :initform 0)
   (currentGoal
    :reader currentGoal
    :initarg :currentGoal
    :type cl:integer
    :initform 0)
   (nextGoal
    :reader nextGoal
    :initarg :nextGoal
    :type cl:integer
    :initform 0)
   (nextNextGoal
    :reader nextNextGoal
    :initarg :nextNextGoal
    :type cl:integer
    :initform 0))
)

(cl:defclass TopoTask (<TopoTask>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <TopoTask>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'TopoTask)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name topoMapServer-msg:<TopoTask> is deprecated: use topoMapServer-msg:TopoTask instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <TopoTask>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader topoMapServer-msg:header-val is deprecated.  Use topoMapServer-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'lastGoal-val :lambda-list '(m))
(cl:defmethod lastGoal-val ((m <TopoTask>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader topoMapServer-msg:lastGoal-val is deprecated.  Use topoMapServer-msg:lastGoal instead.")
  (lastGoal m))

(cl:ensure-generic-function 'currentGoal-val :lambda-list '(m))
(cl:defmethod currentGoal-val ((m <TopoTask>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader topoMapServer-msg:currentGoal-val is deprecated.  Use topoMapServer-msg:currentGoal instead.")
  (currentGoal m))

(cl:ensure-generic-function 'nextGoal-val :lambda-list '(m))
(cl:defmethod nextGoal-val ((m <TopoTask>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader topoMapServer-msg:nextGoal-val is deprecated.  Use topoMapServer-msg:nextGoal instead.")
  (nextGoal m))

(cl:ensure-generic-function 'nextNextGoal-val :lambda-list '(m))
(cl:defmethod nextNextGoal-val ((m <TopoTask>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader topoMapServer-msg:nextNextGoal-val is deprecated.  Use topoMapServer-msg:nextNextGoal instead.")
  (nextNextGoal m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <TopoTask>) ostream)
  "Serializes a message object of type '<TopoTask>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let* ((signed (cl:slot-value msg 'lastGoal)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'currentGoal)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'nextGoal)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'nextNextGoal)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <TopoTask>) istream)
  "Deserializes a message object of type '<TopoTask>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'lastGoal) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'currentGoal) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'nextGoal) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'nextNextGoal) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<TopoTask>)))
  "Returns string type for a message object of type '<TopoTask>"
  "topoMapServer/TopoTask")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'TopoTask)))
  "Returns string type for a message object of type 'TopoTask"
  "topoMapServer/TopoTask")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<TopoTask>)))
  "Returns md5sum for a message object of type '<TopoTask>"
  "ffb97a02a4885599661760b20720afb1")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'TopoTask)))
  "Returns md5sum for a message object of type 'TopoTask"
  "ffb97a02a4885599661760b20720afb1")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<TopoTask>)))
  "Returns full string definition for message of type '<TopoTask>"
  (cl:format cl:nil "std_msgs/Header header~%int32 lastGoal~%int32 currentGoal~%int32 nextGoal~%int32 nextNextGoal~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'TopoTask)))
  "Returns full string definition for message of type 'TopoTask"
  (cl:format cl:nil "std_msgs/Header header~%int32 lastGoal~%int32 currentGoal~%int32 nextGoal~%int32 nextNextGoal~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <TopoTask>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <TopoTask>))
  "Converts a ROS message object to a list"
  (cl:list 'TopoTask
    (cl:cons ':header (header msg))
    (cl:cons ':lastGoal (lastGoal msg))
    (cl:cons ':currentGoal (currentGoal msg))
    (cl:cons ':nextGoal (nextGoal msg))
    (cl:cons ':nextNextGoal (nextNextGoal msg))
))
