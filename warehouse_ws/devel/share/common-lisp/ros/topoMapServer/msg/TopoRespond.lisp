; Auto-generated. Do not edit!


(cl:in-package topoMapServer-msg)


;//! \htmlinclude TopoRespond.msg.html

(cl:defclass <TopoRespond> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (goalReached
    :reader goalReached
    :initarg :goalReached
    :type cl:integer
    :initform 0)
   (reachedGoalSeq
    :reader reachedGoalSeq
    :initarg :reachedGoalSeq
    :type cl:integer
    :initform 0)
   (errorNum
    :reader errorNum
    :initarg :errorNum
    :type cl:integer
    :initform 0))
)

(cl:defclass TopoRespond (<TopoRespond>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <TopoRespond>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'TopoRespond)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name topoMapServer-msg:<TopoRespond> is deprecated: use topoMapServer-msg:TopoRespond instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <TopoRespond>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader topoMapServer-msg:header-val is deprecated.  Use topoMapServer-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'goalReached-val :lambda-list '(m))
(cl:defmethod goalReached-val ((m <TopoRespond>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader topoMapServer-msg:goalReached-val is deprecated.  Use topoMapServer-msg:goalReached instead.")
  (goalReached m))

(cl:ensure-generic-function 'reachedGoalSeq-val :lambda-list '(m))
(cl:defmethod reachedGoalSeq-val ((m <TopoRespond>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader topoMapServer-msg:reachedGoalSeq-val is deprecated.  Use topoMapServer-msg:reachedGoalSeq instead.")
  (reachedGoalSeq m))

(cl:ensure-generic-function 'errorNum-val :lambda-list '(m))
(cl:defmethod errorNum-val ((m <TopoRespond>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader topoMapServer-msg:errorNum-val is deprecated.  Use topoMapServer-msg:errorNum instead.")
  (errorNum m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <TopoRespond>) ostream)
  "Serializes a message object of type '<TopoRespond>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let* ((signed (cl:slot-value msg 'goalReached)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'reachedGoalSeq)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'errorNum)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <TopoRespond>) istream)
  "Deserializes a message object of type '<TopoRespond>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'goalReached) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'reachedGoalSeq) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'errorNum) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<TopoRespond>)))
  "Returns string type for a message object of type '<TopoRespond>"
  "topoMapServer/TopoRespond")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'TopoRespond)))
  "Returns string type for a message object of type 'TopoRespond"
  "topoMapServer/TopoRespond")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<TopoRespond>)))
  "Returns md5sum for a message object of type '<TopoRespond>"
  "018e2107edb458b395b981e66546f526")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'TopoRespond)))
  "Returns md5sum for a message object of type 'TopoRespond"
  "018e2107edb458b395b981e66546f526")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<TopoRespond>)))
  "Returns full string definition for message of type '<TopoRespond>"
  (cl:format cl:nil "std_msgs/Header header~%int32 goalReached~%int32 reachedGoalSeq~%int32 errorNum~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'TopoRespond)))
  "Returns full string definition for message of type 'TopoRespond"
  (cl:format cl:nil "std_msgs/Header header~%int32 goalReached~%int32 reachedGoalSeq~%int32 errorNum~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <TopoRespond>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <TopoRespond>))
  "Converts a ROS message object to a list"
  (cl:list 'TopoRespond
    (cl:cons ':header (header msg))
    (cl:cons ':goalReached (goalReached msg))
    (cl:cons ':reachedGoalSeq (reachedGoalSeq msg))
    (cl:cons ':errorNum (errorNum msg))
))
