(cl:in-package topoMapServer-msg)
(cl:export '(HEADER-VAL
          HEADER
          LASTGOAL-VAL
          LASTGOAL
          CURRENTGOAL-VAL
          CURRENTGOAL
          NEXTGOAL-VAL
          NEXTGOAL
          NEXTNEXTGOAL-VAL
          NEXTNEXTGOAL
))