(cl:in-package topoMapServer-msg)
(cl:export '(HEADER-VAL
          HEADER
          GOALREACHED-VAL
          GOALREACHED
          REACHEDGOALSEQ-VAL
          REACHEDGOALSEQ
          ERRORNUM-VAL
          ERRORNUM
))