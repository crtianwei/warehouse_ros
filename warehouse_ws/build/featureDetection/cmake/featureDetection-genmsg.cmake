# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "featureDetection: 3 messages, 0 services")

set(MSG_I_FLAGS "-IfeatureDetection:/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg;-Istd_msgs:/opt/ros/lunar/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(featureDetection_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegment.msg" NAME_WE)
add_custom_target(_featureDetection_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "featureDetection" "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegment.msg" ""
)

get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/Lane.msg" NAME_WE)
add_custom_target(_featureDetection_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "featureDetection" "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/Lane.msg" "std_msgs/Header"
)

get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegmentList.msg" NAME_WE)
add_custom_target(_featureDetection_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "featureDetection" "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegmentList.msg" "featureDetection/LineSegment:std_msgs/Header"
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(featureDetection
  "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/Lane.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/lunar/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/featureDetection
)
_generate_msg_cpp(featureDetection
  "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegmentList.msg"
  "${MSG_I_FLAGS}"
  "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegment.msg;/opt/ros/lunar/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/featureDetection
)
_generate_msg_cpp(featureDetection
  "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegment.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/featureDetection
)

### Generating Services

### Generating Module File
_generate_module_cpp(featureDetection
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/featureDetection
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(featureDetection_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(featureDetection_generate_messages featureDetection_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegment.msg" NAME_WE)
add_dependencies(featureDetection_generate_messages_cpp _featureDetection_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/Lane.msg" NAME_WE)
add_dependencies(featureDetection_generate_messages_cpp _featureDetection_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegmentList.msg" NAME_WE)
add_dependencies(featureDetection_generate_messages_cpp _featureDetection_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(featureDetection_gencpp)
add_dependencies(featureDetection_gencpp featureDetection_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS featureDetection_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(featureDetection
  "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/Lane.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/lunar/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/featureDetection
)
_generate_msg_eus(featureDetection
  "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegmentList.msg"
  "${MSG_I_FLAGS}"
  "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegment.msg;/opt/ros/lunar/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/featureDetection
)
_generate_msg_eus(featureDetection
  "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegment.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/featureDetection
)

### Generating Services

### Generating Module File
_generate_module_eus(featureDetection
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/featureDetection
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(featureDetection_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(featureDetection_generate_messages featureDetection_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegment.msg" NAME_WE)
add_dependencies(featureDetection_generate_messages_eus _featureDetection_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/Lane.msg" NAME_WE)
add_dependencies(featureDetection_generate_messages_eus _featureDetection_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegmentList.msg" NAME_WE)
add_dependencies(featureDetection_generate_messages_eus _featureDetection_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(featureDetection_geneus)
add_dependencies(featureDetection_geneus featureDetection_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS featureDetection_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(featureDetection
  "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/Lane.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/lunar/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/featureDetection
)
_generate_msg_lisp(featureDetection
  "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegmentList.msg"
  "${MSG_I_FLAGS}"
  "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegment.msg;/opt/ros/lunar/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/featureDetection
)
_generate_msg_lisp(featureDetection
  "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegment.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/featureDetection
)

### Generating Services

### Generating Module File
_generate_module_lisp(featureDetection
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/featureDetection
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(featureDetection_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(featureDetection_generate_messages featureDetection_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegment.msg" NAME_WE)
add_dependencies(featureDetection_generate_messages_lisp _featureDetection_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/Lane.msg" NAME_WE)
add_dependencies(featureDetection_generate_messages_lisp _featureDetection_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegmentList.msg" NAME_WE)
add_dependencies(featureDetection_generate_messages_lisp _featureDetection_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(featureDetection_genlisp)
add_dependencies(featureDetection_genlisp featureDetection_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS featureDetection_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(featureDetection
  "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/Lane.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/lunar/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/featureDetection
)
_generate_msg_nodejs(featureDetection
  "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegmentList.msg"
  "${MSG_I_FLAGS}"
  "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegment.msg;/opt/ros/lunar/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/featureDetection
)
_generate_msg_nodejs(featureDetection
  "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegment.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/featureDetection
)

### Generating Services

### Generating Module File
_generate_module_nodejs(featureDetection
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/featureDetection
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(featureDetection_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(featureDetection_generate_messages featureDetection_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegment.msg" NAME_WE)
add_dependencies(featureDetection_generate_messages_nodejs _featureDetection_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/Lane.msg" NAME_WE)
add_dependencies(featureDetection_generate_messages_nodejs _featureDetection_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegmentList.msg" NAME_WE)
add_dependencies(featureDetection_generate_messages_nodejs _featureDetection_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(featureDetection_gennodejs)
add_dependencies(featureDetection_gennodejs featureDetection_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS featureDetection_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(featureDetection
  "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/Lane.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/lunar/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/featureDetection
)
_generate_msg_py(featureDetection
  "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegmentList.msg"
  "${MSG_I_FLAGS}"
  "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegment.msg;/opt/ros/lunar/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/featureDetection
)
_generate_msg_py(featureDetection
  "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegment.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/featureDetection
)

### Generating Services

### Generating Module File
_generate_module_py(featureDetection
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/featureDetection
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(featureDetection_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(featureDetection_generate_messages featureDetection_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegment.msg" NAME_WE)
add_dependencies(featureDetection_generate_messages_py _featureDetection_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/Lane.msg" NAME_WE)
add_dependencies(featureDetection_generate_messages_py _featureDetection_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/featureDetection/msg/LineSegmentList.msg" NAME_WE)
add_dependencies(featureDetection_generate_messages_py _featureDetection_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(featureDetection_genpy)
add_dependencies(featureDetection_genpy featureDetection_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS featureDetection_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/featureDetection)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/featureDetection
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(featureDetection_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/featureDetection)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/featureDetection
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(featureDetection_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/featureDetection)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/featureDetection
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(featureDetection_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/featureDetection)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/featureDetection
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(featureDetection_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/featureDetection)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/featureDetection\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/featureDetection
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(featureDetection_generate_messages_py std_msgs_generate_messages_py)
endif()
