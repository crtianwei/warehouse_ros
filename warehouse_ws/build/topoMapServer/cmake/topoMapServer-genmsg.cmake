# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "topoMapServer: 2 messages, 0 services")

set(MSG_I_FLAGS "-ItopoMapServer:/home/crti/warehouse_ros/warehouse_ws/src/topoMapServer/msg;-Istd_msgs:/opt/ros/lunar/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(topoMapServer_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/topoMapServer/msg/TopoRespond.msg" NAME_WE)
add_custom_target(_topoMapServer_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "topoMapServer" "/home/crti/warehouse_ros/warehouse_ws/src/topoMapServer/msg/TopoRespond.msg" "std_msgs/Header"
)

get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/topoMapServer/msg/TopoTask.msg" NAME_WE)
add_custom_target(_topoMapServer_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "topoMapServer" "/home/crti/warehouse_ros/warehouse_ws/src/topoMapServer/msg/TopoTask.msg" "std_msgs/Header"
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(topoMapServer
  "/home/crti/warehouse_ros/warehouse_ws/src/topoMapServer/msg/TopoRespond.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/lunar/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/topoMapServer
)
_generate_msg_cpp(topoMapServer
  "/home/crti/warehouse_ros/warehouse_ws/src/topoMapServer/msg/TopoTask.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/lunar/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/topoMapServer
)

### Generating Services

### Generating Module File
_generate_module_cpp(topoMapServer
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/topoMapServer
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(topoMapServer_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(topoMapServer_generate_messages topoMapServer_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/topoMapServer/msg/TopoRespond.msg" NAME_WE)
add_dependencies(topoMapServer_generate_messages_cpp _topoMapServer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/topoMapServer/msg/TopoTask.msg" NAME_WE)
add_dependencies(topoMapServer_generate_messages_cpp _topoMapServer_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(topoMapServer_gencpp)
add_dependencies(topoMapServer_gencpp topoMapServer_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS topoMapServer_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(topoMapServer
  "/home/crti/warehouse_ros/warehouse_ws/src/topoMapServer/msg/TopoRespond.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/lunar/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/topoMapServer
)
_generate_msg_eus(topoMapServer
  "/home/crti/warehouse_ros/warehouse_ws/src/topoMapServer/msg/TopoTask.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/lunar/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/topoMapServer
)

### Generating Services

### Generating Module File
_generate_module_eus(topoMapServer
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/topoMapServer
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(topoMapServer_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(topoMapServer_generate_messages topoMapServer_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/topoMapServer/msg/TopoRespond.msg" NAME_WE)
add_dependencies(topoMapServer_generate_messages_eus _topoMapServer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/topoMapServer/msg/TopoTask.msg" NAME_WE)
add_dependencies(topoMapServer_generate_messages_eus _topoMapServer_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(topoMapServer_geneus)
add_dependencies(topoMapServer_geneus topoMapServer_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS topoMapServer_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(topoMapServer
  "/home/crti/warehouse_ros/warehouse_ws/src/topoMapServer/msg/TopoRespond.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/lunar/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/topoMapServer
)
_generate_msg_lisp(topoMapServer
  "/home/crti/warehouse_ros/warehouse_ws/src/topoMapServer/msg/TopoTask.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/lunar/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/topoMapServer
)

### Generating Services

### Generating Module File
_generate_module_lisp(topoMapServer
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/topoMapServer
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(topoMapServer_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(topoMapServer_generate_messages topoMapServer_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/topoMapServer/msg/TopoRespond.msg" NAME_WE)
add_dependencies(topoMapServer_generate_messages_lisp _topoMapServer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/topoMapServer/msg/TopoTask.msg" NAME_WE)
add_dependencies(topoMapServer_generate_messages_lisp _topoMapServer_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(topoMapServer_genlisp)
add_dependencies(topoMapServer_genlisp topoMapServer_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS topoMapServer_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(topoMapServer
  "/home/crti/warehouse_ros/warehouse_ws/src/topoMapServer/msg/TopoRespond.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/lunar/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/topoMapServer
)
_generate_msg_nodejs(topoMapServer
  "/home/crti/warehouse_ros/warehouse_ws/src/topoMapServer/msg/TopoTask.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/lunar/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/topoMapServer
)

### Generating Services

### Generating Module File
_generate_module_nodejs(topoMapServer
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/topoMapServer
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(topoMapServer_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(topoMapServer_generate_messages topoMapServer_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/topoMapServer/msg/TopoRespond.msg" NAME_WE)
add_dependencies(topoMapServer_generate_messages_nodejs _topoMapServer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/topoMapServer/msg/TopoTask.msg" NAME_WE)
add_dependencies(topoMapServer_generate_messages_nodejs _topoMapServer_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(topoMapServer_gennodejs)
add_dependencies(topoMapServer_gennodejs topoMapServer_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS topoMapServer_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(topoMapServer
  "/home/crti/warehouse_ros/warehouse_ws/src/topoMapServer/msg/TopoRespond.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/lunar/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/topoMapServer
)
_generate_msg_py(topoMapServer
  "/home/crti/warehouse_ros/warehouse_ws/src/topoMapServer/msg/TopoTask.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/lunar/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/topoMapServer
)

### Generating Services

### Generating Module File
_generate_module_py(topoMapServer
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/topoMapServer
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(topoMapServer_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(topoMapServer_generate_messages topoMapServer_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/topoMapServer/msg/TopoRespond.msg" NAME_WE)
add_dependencies(topoMapServer_generate_messages_py _topoMapServer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/crti/warehouse_ros/warehouse_ws/src/topoMapServer/msg/TopoTask.msg" NAME_WE)
add_dependencies(topoMapServer_generate_messages_py _topoMapServer_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(topoMapServer_genpy)
add_dependencies(topoMapServer_genpy topoMapServer_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS topoMapServer_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/topoMapServer)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/topoMapServer
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(topoMapServer_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/topoMapServer)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/topoMapServer
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(topoMapServer_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/topoMapServer)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/topoMapServer
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(topoMapServer_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/topoMapServer)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/topoMapServer
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(topoMapServer_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/topoMapServer)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/topoMapServer\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/topoMapServer
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(topoMapServer_generate_messages_py std_msgs_generate_messages_py)
endif()
