# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/crti/warehouse_ros/warehouse_ws/src/3rd/scan_tools-indigo/laser_scan_sparsifier/include".split(';') if "/home/crti/warehouse_ros/warehouse_ws/src/3rd/scan_tools-indigo/laser_scan_sparsifier/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;nodelet;sensor_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-llaser_scan_sparsifier".split(';') if "-llaser_scan_sparsifier" != "" else []
PROJECT_NAME = "laser_scan_sparsifier"
PROJECT_SPACE_DIR = "/home/crti/warehouse_ros/warehouse_ws/devel"
PROJECT_VERSION = "0.3.2"
