# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/crti/warehouse_ros/warehouse_ws/src/3rd/scan_tools-indigo/laser_scan_splitter/include".split(';') if "/home/crti/warehouse_ros/warehouse_ws/src/3rd/scan_tools-indigo/laser_scan_splitter/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;nodelet;sensor_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-llaser_scan_splitter".split(';') if "-llaser_scan_splitter" != "" else []
PROJECT_NAME = "laser_scan_splitter"
PROJECT_SPACE_DIR = "/home/crti/warehouse_ros/warehouse_ws/devel"
PROJECT_VERSION = "0.3.2"
