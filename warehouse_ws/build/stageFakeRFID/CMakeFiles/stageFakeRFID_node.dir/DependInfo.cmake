# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/crti/warehouse_ros/warehouse_ws/src/stageFakeRFID/src/stageFakeRFID.cpp" "/home/crti/warehouse_ros/warehouse_ws/build/stageFakeRFID/CMakeFiles/stageFakeRFID_node.dir/src/stageFakeRFID.cpp.o"
  "/home/crti/warehouse_ros/warehouse_ws/src/stageFakeRFID/src/stageFakeRFID_node.cpp" "/home/crti/warehouse_ros/warehouse_ws/build/stageFakeRFID/CMakeFiles/stageFakeRFID_node.dir/src/stageFakeRFID_node.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"stageFakeRFID\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/crti/warehouse_ros/warehouse_ws/src/stageFakeRFID/include"
  "/opt/ros/lunar/include"
  "/opt/ros/lunar/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
