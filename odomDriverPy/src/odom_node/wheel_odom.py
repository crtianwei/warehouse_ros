#!/usr/bin/env python2

import sys, serial, struct, time, threading
import math
import rospy
from geometry_msgs.msg import Twist, TransformStamped, Quaternion
from nav_msgs.msg import Odometry
from std_msgs.msg import String
import tf, tf2_ros

BAUD_RATES = (  
    300,600,1200,2400,4800,9600,14400,19200,
    28800,38400,57600,115200)
	
global mvx, mvz, watchDog
mvx=0.0
mvz=0.0
watchDog=0

class MyOdometry(object):  
    x = 0.0;y = 0.0;theta = 0.0; flag_1 = 0.0; flag_2 = 0.0; v_cmd = 0.0; w_cmd = 0.0;   
    START ='\xEE\xAA'
    END ='\x00\xBB'

    def __init__(self, tty, baudrate):
        # serial
        self.ser = serial.Serial(tty, baudrate=baudrate, timeout=2)
        self.buffer = ''
        #self.ser.open()
        self.ser.write('\xEE\xAA\x01\x00\x00\x01\x00\x00\x00\x00\x00\xBB')
        self.lock = threading.RLock()

    def read(self):
        #print self.ser.inWaiting() != 0
        #find tou and weiba 
        while self.buffer.find(self.START)<0 or ( self.buffer[self.buffer.find(self.START)+len(self.START):] ).find(self.END) < 0:
            self.buffer += self.ser.read(1)
               
        self.buffer = self.buffer.split(self.START,2)[1]
        str = self.buffer[0:10]
        #print len(str)
        self.buffer = self.buffer[10:]
        
        if(len(str)<10):
            return

        with self.lock:
            #crti:2016-06-22
            #self.x, self.y, self.theta, self.v_cur,self.w_cur =  struct.unpack('>3h2b', str[0:8])
            self.x, self.y, self.theta, self.flag_1 ,self.flag_2 =  struct.unpack('>3h2b', str[0:8])
            #print 5
            self.x = self.x/100.0
            self.y = self.y/100.0
            self.theta = self.theta / 180.0 * 3.141592653
            while self.theta > 3.1415926: 
                self.theta -= 2*3.1415926
            while self.theta < -3.1415926:
                self.theta += 2*3.1415926

    def write (self):
        with self.lock:
            if self.cmd_v > 0:
                vsign = 0x01
                vabs = self.cmd_v * 1000.00
            else:
                vsign = 0x02
                vabs = -self.cmd_v * 1000.00
            if self.cmd_w > 0:
                wsign = 0x01
                wabs =self.cmd_w * 180 /3.141592653
            else:
                wsign = 0x02
                wabs = - self.cmd_w * 180 /3.141592653
            #print  vsign, vabs, wsign, wabs
            print('stm32(%.2f,%.2f,%.2f,%.2f)'%(vsign,vabs,wsign,wabs))
            data = struct.pack('>3BhBh4B', 0xee, 0xaa, vsign, vabs, wsign, wabs, 0x00, 0x00, 0x00, 0xBB)

            self.ser.write(data);
            
    def set_velocity (self, v, w):
        with self.lock:
            self.cmd_v = v
            self.cmd_w = w
    
    def close(self):
        self.ser.flush()
        self.ser.close()
    
    def update(self, v, w):
        self.set_velocity(v,w)
        self.write()
        self.read()
        
    def odometry(self):
        return self.x, self.y, self.theta

    def set_pose_zero(self):
        self.ser.write('\xEE\xAA\x01\x00\x00\x01\x00\x00\xFE\xDC\x00\xBB')
        print 'set_pose_zero'

    def open_projector(self):
        self.ser.write('\xEE\xAA\x01\x00\x00\x01\x00\x00\xFE\xA2\x00\xBB')
        print 'open_projector'

    def close_projector(self):
        self.ser.write('\xEE\xAA\x01\x00\x00\x01\x00\x00\xFE\xA1\x00\xBB')
        print 'close_projector'
             
import time, array, threading, copy, math

def create_thread(task):
    if callable(task):
        thread = threading.Thread(target = task)
        thread.setDaemon(True)
        thread.start()
        return thread
    else:
        raise 'task must be callable'

class Driver:
    cmd_lock = threading.RLock()
    cmd_v = 0; cmd_w = 0; cmd_manual = False

    odom_lock = threading.RLock()
    x = 0; y = 0; theta = 0; 

    set_zero_flag = True
    open_projector_flag = True
    close_projector_flag = True
   

    def __init__(self, tty, baudrate, joy_index = 0):
        self.is_running = True       
        try:
           from joystick import *
           self.joy = Joystick( joy_index ) 
           self.joy_thread = create_thread(self._joystick)
        except:
           print 'no joystick found'
           
        self.odom = MyOdometry(tty, baudrate)    


    def __del__(self):
        self.is_running = False
        self.joy_thread.join(1000)


    def update(self, v, w):
        with self.cmd_lock:
            if not self.cmd_manual:
                self.cmd_v = v; self.cmd_w = w
            cmd_v = copy.copy(self.cmd_v)
            cmd_w = copy.copy(self.cmd_w)
        self.odom.update( cmd_v, cmd_w)
        with self.odom_lock:
            self.x, self.y, self.theta = self.odom.odometry()
    def _joystick(self):
        while self.is_running:
            self.joy.update()
            with self.cmd_lock:
                if self.joy.button(5):
                    self.cmd_manual = True
                elif self.joy.button(4):
                    self.cmd_manual = False
                    self.cmd_v = 0
                    self.cmd_w = 0
                if self.cmd_manual:
                    #print "hello"
                    self.cmd_v = -self.joy.axis(1)*0.5
                    self.cmd_w = -self.joy.axis(0)*0.7
                    if self.joy.button(12) :
                        self.odom.set_pose_zero()
driver = Driver('/dev/ttyUSB0',19200,0)
def update(v,w,projector):
    driver.update(v,w)
    x, y, t = driver.odom.odometry()   
    if projector == 0:
        driver.odom.close_projector()
    elif projector == 1:
        driver.odom.open_projector()
    #else:
        #print 'open_or_close_projector, input not 0 or 1 :(', projector, ')'
    return x, y, t
   

def callback(data):
    global mvx, mvz, watchDog
    mvx=data.linear.x     #*3.0/8.0
    mvz=data.angular.z * 200  #*9.0/10.0/3.1415926
    watchDog=0
    #print('recv v(%.2f,%.2f)'%(mvx,mvz))

def myspin():
    rospy.spin()
    
def call():		
    global mvx, mvz, watchDog
    rospy.Subscriber('/cmd_vel',Twist,callback)
    pub = rospy.Publisher('/odom', Odometry, queue_size=10)
    rospy.init_node('call', anonymous=True)
    broadcaster=tf2_ros.TransformBroadcaster()
    rate = rospy.Rate(20) # 20hz
    x=0.0
    y=0.0
    t=0.0
    last_x = 0.0
    last_y = 0.0
    last_t = 0.0
    first_time = True
    #create_thread(myspin)

    while not rospy.is_shutdown():
        x, y, t = update(mvx,mvz,-1)
        if first_time:
            print('first time ...')
            last_x = x
            last_y = y
            last_t = t
            first_time = False
            continue
        print('pos=(%.2f,%.2f,%.2f)'%(x, y, t))
        print('cmd=(%.2f,%.3f)'%(driver.cmd_v, driver.cmd_w))
        watchDog += 1
        if(watchDog>10):
            mvx=0
            mvz=0
        current_time=rospy.Time.now()
        #pub odom
        odom=Odometry()
        odom.header.stamp=current_time
        odom.header.frame_id="odom"
        odom.child_frame_id="base_footprint"
	#position-
        odom.pose.pose.position.x=x
        odom.pose.pose.position.y=y
        odom.pose.pose.position.z=0.0
        q=tf.transformations.quaternion_from_euler(0,0,t)
        odom.pose.pose.orientation.x=q[0]
        odom.pose.pose.orientation.y=q[1]
        odom.pose.pose.orientation.z=q[2]
        odom.pose.pose.orientation.w=q[3]     
        odom.pose.covariance[0] = 0.5
        odom.pose.covariance[7] = 0.5
        odom.pose.covariance[14] = 0.000001
        odom.pose.covariance[21] = 0.000001
        odom.pose.covariance[28] = 0.000001
        odom.pose.covariance[35] = 0.5

         #vel
        odom.twist.twist.linear.x=0.0 #(x-last_x)/(0.05*math.cos(t))
        odom.twist.twist.linear.y=0.0
        odom.twist.twist.linear.z=0.0
        odom.twist.twist.angular.x=0.0
        odom.twist.twist.angular.y=0.0
        odom.twist.twist.angular.z=0.0 #(t-last_t)/(0.05)
        #print('odom cmd=(%.2f,%.3f)'%(odom.twist.twist.linear.x, odom.twist.twist.angular.z))
        #pub tf
        trans=TransformStamped()
        trans.header.stamp=current_time
        trans.header.frame_id="odom"
        trans.child_frame_id="base_link"
        trans.transform.translation.x=x
        trans.transform.translation.y=y
        trans.transform.translation.z=0.0
        q=tf.transformations.quaternion_from_euler(0,0,t)
        trans.transform.rotation.x=q[0]
        trans.transform.rotation.y=q[1]
        trans.transform.rotation.z=q[2]
        trans.transform.rotation.w=q[3]
        #pub
        pub.publish(odom)
        broadcaster.sendTransform(trans)

        last_x = x
        last_y = y
        last_t = t
        #rospy.spin()
        #rate.sleep()

if __name__ == '__main__':
    try:
        call()
        #create_thread(call)
        #rospy.spin()
    except rospy.ROSInterruptException:
        pass
